<?php
/* ----------------------------------------------------------------------
 * themes/default/views/bundles/ca_objects_default_html.php :
 * ----------------------------------------------------------------------
 * CollectiveAccess
 * Open-source collections management software
 * ----------------------------------------------------------------------
 *
 * Software by Whirl-i-Gig (http://www.whirl-i-gig.com)
 * Copyright 2013-2018 Whirl-i-Gig
 *
 * For more information visit http://www.CollectiveAccess.org
 *
 * This program is free software; you may redistribute it and/or modify it under
 * the terms of the provided license as published by Whirl-i-Gig
 *
 * CollectiveAccess is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTIES whatsoever, including any implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This source code is free and modifiable under the terms of
 * GNU General Public License. (http://www.gnu.org/copyleft/gpl.html). See
 * the "license.txt" file for details, or visit the CollectiveAccess web site at
 * http://www.CollectiveAccess.org
 *
 * ----------------------------------------------------------------------
 */

$t_object =             $this->getVar("item");
$va_comments =             $this->getVar("comments");
$va_tags =                 $this->getVar("tags_array");
$vn_comments_enabled =     $this->getVar("commentsEnabled");
$vn_share_enabled =     $this->getVar("shareEnabled");
$vn_pdf_enabled =         $this->getVar("pdfEnabled");
$vn_id =                $t_object->get('ca_objects.object_id');
$storyModeUrl =         __CA_THEME_DIR__."/views/StoryMode/";
//    print_r($t_object);

?>
<?php

?>
<!-- use <l> tags to display localized labels: <l lang="en">^ca_entities.preferred_labels.displayname</l> -->
<div class="row">
    <div class='col-xs-12 navTop'><!--- only shown at small screen size -->
        {{{previousLink}}}{{{resultsLink}}}{{{nextLink}}}
    </div><!-- end detailTop -->
    <div class='navLeftRight col-xs-1 col-sm-1 col-md-1 col-lg-1'>
        <div class="detailNavBgLeft">
            {{{previousLink}}}{{{resultsLink}}}
        </div><!-- end detailNavBgLeft -->
    </div><!-- end col -->
    <div class='col-xs-12 col-sm-10 col-md-10 col-lg-10 '>
        <div class='col-sm-12 col-md-12 col-lg-10 col-lg-offset-1'>

            {{{<ifdef code="ca_collections">
							
							<!-- <H6>Path:	<unit relativeTo="ca_collections.hierarchy.parent_id" delimiter="  &nbsp;>>"><l>^ca_collections.preferred_labels.name </l></unit> -->
							<!-- <unit relativeTo="ca_collections" delimiter=" >> "><l>^ca_collections.hierarchy.preferred_labels.name</l></unit> -->
						<!-- </H6> -->
            
			<H5>
                <unit relativeTo="ca_collections" delimiter="<br />"><unit relativeTo="ca_collections.hierarchy.parent_id" restrictToTypes="approaches" delimiter=" ➔ ">
				<l>&#9641; ^ca_collections.preferred_labels.name</l></unit>
				<unit relativeTo="ca_collections.hierarchy.parent_id" restrictToTypes="collection_type_subcollection" delimiter=" ; "><l> ➔ &#9714; ^ca_collections.preferred_labels.name</l></unit></unit>
			</H5>
			</ifdef>}}}
            <H5>
			<!-- {{{<unit relativeTo="ca_collections" restrictToTypes="approaches" delimiter="">
				<l>&#9641; ^ca_collections.preferred_labels.name</l></unit>
				<unit relativeTo="ca_collections" restrictToTypes="collection_type_subcollection" delimiter=""><l> ➔ &#9714; ^ca_collections.preferred_labels.name</l></unit>
				<ifcount min="1" code="ca_collections"> ➔ </ifcount>}}} -->
			{{{<ifcount min="1" code="ca_objects.parent"><unit relativeTo="ca_objects.parent" delimiter=" "><l>&#9671; ^ca_objects.preferred_labels</l> ➔ </unit></ifcount>}}}
			<big style="vertical-align:-100%;">&#9672;<b> {{{ca_objects.preferred_labels.name}}}</b></big></H5>
            <small><hr /></small>
        </div> 

        <div class="container">
            <div class="row">
                <!-- presentation col -->
                <div class='col-sm-6 col-md-6 col-lg-5 col-lg-offset-1'> 
                    {{{representationViewer}}}
                    {{{<ifcount code="ca_object_representations"  max="0" >    <ifdef code="ca_objects.long_description">
                        <div class='unit longText'><h6>Text-Content</h6>
                        <ifdef code="ca_objects.long_description.long_description_text">
                        
                            <span class="trimText1">^ca_objects.long_description.long_description_text</span>
                        </div>
                    </ifdef>
                    <?php /*
                    Reference: caNavLink / controller Parameters:
                    1:expects: $this->request, _t("Linklabel"), giving out the <a>Linklabel</a>
                    2:css class of the <a></a>*/?><style>.redclass{background-color:red;}</style><?/*
                    3:pre-trunk of path: usecase? would result in index.php/pre-trunk/trunk/rest
                    4:trunk of path, resulting in: /index.php/trunk/
                
                    5:rest of path, resulting in: /index.php/trunk/rest, can be "index"
                    */?>
                    <ifdef code="ca_objects.long_description.long_description_pseudolink">
                        <span>See also: <?php print ($this->request->getController() == "StoryMode") ? 'class="active"' : ''; ?><?php print caNavLink($this->request, _t("&#9651;&#9696;&#9675;&#9697;&#9671; ^ca_objects.long_description.long_description_pseudolink.preferred_labels"), "", "", "StoryMode", "Index?single=^ca_objects.long_description.long_description_pseudolink.idno"); ?></span>
                </div>        
                    </ifdef>
                    
                    </ifdef></ifcount>}}}
            
                    <div id="detailAnnotations"></div>
                        
                        <?php print caObjectRepresentationThumbnails($this->request, $this->getVar("representation_id"), $t_object, array("returnAs" => "bsCols", "linkTo" => "carousel", "bsColClasses" => "smallpadding col-sm-3 col-md-3 col-xs-4", "primaryOnly" => $this->getVar('representationViewerPrimaryOnly') ? 1 : 0)); ?>
                                   
                        <?php
                                        // Comment and Share Tools
                        if ($vn_comments_enabled | $vn_share_enabled | $vn_pdf_enabled) {

                            print '<div id="detailTools">';
                            if ($vn_comments_enabled) {
                                ?>                
                        <div class="detailTool"><a href='#' onclick='jQuery("#detailComments").slideToggle(); return false;'><span class="glyphicon glyphicon-comment"></span>Comments and Tags (<?php print sizeof($va_comments) + sizeof($va_tags); ?>)</a></div><!-- end detailTool -->
                        <div id='detailComments'><?php print $this->getVar("itemComments");?></div><!-- end itemComments -->
                            <?php
                            }
                            if ($vn_share_enabled) {
                                print '<div class="detailTool"><span class="glyphicon glyphicon-share-alt"></span>'.$this->getVar("shareLink").'</div><!-- end detailTool -->';
                            }
                            if ($vn_pdf_enabled) {
                                print "<div class='detailTool'><span class='glyphicon glyphicon-file'></span>".caDetailLink($this->request, "Download as PDF", "faDownload", "ca_objects", $vn_id, array('view' => 'pdf', 'export_format' => '_pdf_ca_objects_summary'))."</div>";
                            }
                            print '</div><!-- end detailTools -->';
                        }

?>


                        
                        {{{<ifdef code="ca_objects.dimensions"><br /><h6>Dimensions:</h6><unit relativeTo="ca_objects">
                        <ifdef code="ca_objects.dimensions.dimensions_length">	^ca_objects.dimensions.dimensions_length Length x </ifdef>

                        <ifdef code="ca_objects.dimensions.dimensions_width">^ca_objects.dimensions.dimensions_width Width x </ifdef>
                        <ifdef code="ca_objects.dimensions.dimensions_height">^ca_objects.dimensions.dimensions_height Height x </ifdef>
                        <ifdef code="ca_objects.dimensions.dimensions_thickness">^ca_objects.dimensions.dimensions_thickness Thickness </ifdef> </unit></ifdef><br />
                        <ifdef code="ca_objects.dimensions.dimensions_weight"> Weight: ^ca_objects.dimensions.dimensions_weight</ifdef> </unit></ifdef>}}}
						{{{<ifdef code="ca_objects.formatNotes">
                            <div class='unit'><h6>Format Notes</h6><br />
                                <span class="trimText2">^ca_objects.formatNotes</span>

                            </div>
                        </ifdef>}}}

                </div><!-- end col -->
                    
                <div class='col-sm-6 col-md-6 col-lg-5'>
                   
                        <H6>{{{<unit>Type: ^ca_objects.type_id</unit>}}}</H6><br />
                    
                        
                        <!--This would be a nice unicode symbol for Focus-type collections &#11034;  -->
                        <!--display idno -->
                        {{{<ifdef code="ca_objects.idno"><h6>Identifier:</h6> ^ca_objects.idno</ifdef>}}} 
                        <!--check and display ISBN, ISSN, DOI-  -->

                        {{{<ifdef code="ca_objects.isbn">
                            <h6> | ISBN: </h6><span class="inline">
                            <a href="https://search.worldcat.org/search?q=^ca_objects.isbn" class="link" target="_BLANK">^ca_objects.isbn</a>
                            <span class="tooltip">Search in worldcat.org</span>
                            </span>
                        </ifdef>}}}
                        {{{<ifdef code="ca_objects.issn"><h6> | issn: </h6>
							<unit relativeTo="ca_objects.issn" delimiter=", ">
                            <span class="inline">
                            <a href="https://search.worldcat.org/search?q=n2%3A^ca_objects.issn" class="link" target="_BLANK">^ca_objects.issn</a>
                            <span class="tooltip">Search publication in worldcat.org</span>
                            </span></unit>
                        </ifdef>}}}
                        {{{<ifdef code="ca_objects.doi">
                            <h6> | doi: </h6><span class="inline">
                            <a href="https://doi.org/^ca_objects.doi" class="link" target="_BLANK">^ca_objects.doi</a>
                            <span class="tooltip"> Search article via doi.org</span>
                            </span>
                        </ifdef>}}}



                        {{{<ifdef code="ca_objects.containerID"><H6>Box/series:</H6>^ca_objects.containerID<br/></ifdef>}}}                
                        
                        {{{<ifdef code="ca_objects.description">
                            <div class='unit'><h6>Description</h6><br />
                                <span class="trimText2">^ca_objects.description</span>

                            </div>
                        </ifdef>}}}

                        {{{<ifdef code="ca_objects.description_source">
                            <div class='unit'><h8>Source of Description:</h8>
                                <span class="trimText2">^ca_objects.description_source</span>

                            </div>
                        </ifdef>}}}
						
                        {{{<ifcount code="ca_object_representations" min="1"><ifdef code="ca_objects.long_description">
                            <div class='unit'><h6>Longer Description or Text-Content</h6><br />
                            <ifdef code="ca_objects.long_description.long_description_text">
                            
                                <span class="trimText2">^ca_objects.long_description.long_description_text</span>
                            </div>
                        </ifdef>
                        <?php /*
                        Reference: caNavLink / controller Parameters:
                        1:expects: $this->request, _t("Linklabel"), giving out the <a>Linklabel</a>
                        2:css class of the <a></a>*/?><style>.redclass{background-color:red;}</style><?/*
                        3:pre-trunk of path: usecase? would result in index.php/pre-trunk/trunk/rest
                        4:trunk of path, resulting in: /index.php/trunk/
                    
                        5:rest of path, resulting in: /index.php/trunk/rest, can be "index"
                        */?>
                        <ifdef code="ca_objects.long_description.long_description_pseudolink">
                            <span>See also: <?php print ($this->request->getController() == "StoryMode") ? 'class="active"' : ''; ?><?php print caNavLink($this->request, _t("&#9651;&#9696;&#9675;&#9697;&#9671; ^ca_objects.long_description.long_description_pseudolink.preferred_labels"), "", "", "StoryMode", "Index?single=^ca_objects.long_description.long_description_pseudolink.idno"); ?></span>

                        </ifdef>
                        
                        </ifdef></ifcount>}}}

                        <!-- {{{<ifcount code="ca_occurrences" min="1" max="1" ><H6>Referenced in Fabulation</H6></ifcount>}}}
                        <!-- TODO query for the related occurences, then pull their related collections -->
                        <hr></hr>
                        {{{<ifdef code="ca_occurrences">
                        <!-- <ifcount code="ca_occurrences" min="1" max="1" ><H6 style="color:white">Referenced in Fabulation:</H6><br /></ifcount> -->
                            <ifcount code="ca_occurrences" min="1" >
                            <unit relativeTo="ca_occurrences" delimiter="<br/>"><ifdef code="ca_collections" restrictToTypes="stories"><div class="ca-color"><H6 style="color:white">Referenced in Fabulations:</H6><br />
                                <unit relativeTo="ca_collections" >
                                <?php print ($this->request->getController() == "StoryMode") ? 'class="active" style="color:white"' : ''; ?><?php print caNavLink($this->request, _t("&#9651;&#9696;&#9675;&#9697;&#9671; ^ca_collections.preferred_labels"), "", "", "StoryMode", "Index?single=^ca_collections.idno&focus=$vn_id"); ?> 
                                </unit></div></ifdef>                      
                            </unit> </ifcount>
                        </ifdef>}}}
                        <!-- This is redundant -->
                        <!-- {{{<ifcount code="ca_collections" restrictToTypes="approaches" min="1" max="1"><br /><H6>Approach:</H6></ifcount>}}}
                        {{{<ifcount code="ca_collections" restrictToTypes="approaches" min="2"><br /><H6>Approaches:</H6><br /></ifcount>}}}
                        {{{<unit relativeTo="ca_collections" delimiter="<br/>" restrictToTypes="approaches"><unit relativeTo="ca_collections"><l>^ca_collections.preferred_labels</l></unit> </unit>}}} -->
                            <!-- (^relationship_typename) -->
                        {{{<ifcount code="ca_objects_x_objects"  min="1" max="1"><br /><H6>Related Materials:</H6>

                        <unit relativeTo="ca_objects_x_objects" delimiter="" ><unit relativeTo="ca_objects"><l>^ca_objects.preferred_labels</l></unit> <!-- (^relationship_typename) --></unit>
                        </ifcount>}}}

                        {{{<ifcount code="ca_objects_x_objects"  min="2"><br /><H6>Related Materials:</H6>
                        <unit relativeTo="ca_objects_x_objects" delimiter="</li>"><unit relativeTo="ca_objects"><li><l>^ca_objects.preferred_labels</l></unit> <!-- (^relationship_typename) --></unit>
                        </ifcount>}}}

                            
                        {{{<ifdef code="ca_objects.dateSet.setDisplayValue"><H6>Date:</H6>^ca_objects.dateSet.setDisplayValue<br/></ifdef>}}}
                    <hr></hr>

                    <div class="row">
                            <div class="col-sm-6">     
								<!-- Dates not working yet -->
                                <!-- {{{<ifcount code="ca_objects.date.dates_values" min="1"><H6>Related dates</H6><br /</ifcount>}}}
                                {{{<unit relativeTo="ca_objects.date" delimiter="<br/>">^ca_objects.date.dates_value  ^ca_objects.date.dc_dates_types </unit><br />}}}    -->
								
                                <!-- {{{<ifcount code="ca_entities" min="1" max="1"><H6>Related person</H6></ifcount>}}}
                                {{{<ifcount code="ca_entities" min="2"><H6>Related people</H6></ifcount>}}}
                                {{{<unit relativeTo="ca_entities" delimiter="<br/>"><l>^ca_entities.preferred_labels.displayname</l> (^relationship_typename)</unit>}}} -->
                                
                                {{{<ifcount code="ca_entities" min="1" max="1"><H6>Related person</H6><br /></ifcount>}}}
                                {{{<ifcount code="ca_entities" min="2"><H6>Related people</H6><br /</ifcount>}}}
                                {{{<unit relativeTo="ca_objects_x_entities" delimiter="<br/>"><unit relativeTo="ca_entities"><l>^ca_entities.preferred_labels.displayname</l></unit> (^relationship_typename)</unit>}}}
                                
                                
                                {{{<ifcount code="ca_places" min="1" max="1"><H6>Related place</H6><br /</ifcount>}}}
                                {{{<ifcount code="ca_places" min="2"><H6>Related places</H6><br /</ifcount>}}}
                                {{{<unit relativeTo="ca_objects_x_places" delimiter="<br/>"><unit relativeTo="ca_places"><l>^ca_places.preferred_labels</l></unit> (^relationship_typename)</unit>}}}
                                
                                {{{<ifcount code="ca_list_items" min="1" max="1"><H6>Related Term</H6><br /</ifcount>}}}
                                {{{<ifcount code="ca_list_items" min="2"><H6>Related Terms</H6><br /</ifcount>}}}
                                {{{<unit relativeTo="ca_objects_x_vocabulary_terms" delimiter="<br/>"><unit relativeTo="ca_list_items"><l>^ca_list_items.preferred_labels.name_plural <unit relativeTo="ca_list_items.list_id"><!--  (^ca_lists.preferred_labels) --></unit></l></unit> <!-- (^relationship_typename) --></unit>}}}


                                {{{<ifcount min="1" code="ca_objects.children"><hr /> <h6>Auszüge oder Teilobjekte</h6><ul><unit relativeTo="ca_objects.children" delimiter=" "><li><l>^ca_objects.preferred_labels</l></li></unit></ul></ifcount>}}}
                            </div><!-- end col -->                
                            <div class="col-sm-6 colBorderLeft">
                                {{{map}}}
								<br />
								{{{<ifdef code="ca_objects.coverageNotes">
                            <div class='unit'><h6>Coverage Notes:</h6>
                                <span class="trimText2">^ca_objects.coverageNotes</span>

                            </div>
                        </ifdef>}}}
                            </div>
                        </div><!-- end second order row-->
                            <div class="col-sm-12">
                                {{{<ifdef code="ca_objects.external_link.url_entry" >
                                    <h5>External Links</h5><li>
                                    <unit relativeTo="ca_objects.external_link" delimiter="</li><li>">
                                        <span class="">
                                            <?php echo '<a href="'?>^ca_objects.external_link.url_entry<?php echo '" class="link">'?> ^ca_objects.external_link.url_source</a>
                                            <span class="tooltip" id="urltip">^ca_objects.external_link.url_entry</span><br />
                                        </span>
                                    </unit>
                                </ifdef>}}}
                            </div>
                    
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
        
    </div><!-- end col -->

    <div class='navLeftRight col-xs-1 col-sm-1 col-md-1 col-lg-1'>
        <div class="detailNavBgRight">
            {{{nextLink}}}
        </div><!-- end detailNavBgLeft -->
    </div><!-- end col -->
</div><!-- end row -->

<script type='text/javascript'>
    jQuery(document).ready(function() {
        $('.trimText1').readmore({
          speed: 75,
          maxHeight: 462
        });
    });
    jQuery(document).ready(function() {
        $('.trimText2').readmore({
          speed: 75,
          maxHeight: 180
        });
    });
</script>
