<?php
$t_item = $this->getVar("item");
$va_comments = $this->getVar("comments");
$vn_comments_enabled = 	$this->getVar("commentsEnabled");
$vn_share_enabled = 	$this->getVar("shareEnabled");
$vn_pdf_enabled = 		$this->getVar("pdfEnabled");

# --- get collections configuration
$o_collections_config = caGetCollectionsConfig();
$vb_show_hierarchy_viewer = true;
if($o_collections_config->get("do_not_display_collection_browser")) {
    $vb_show_hierarchy_viewer = false;
}
# --- get the collection hierarchy parent to use for exportin finding aid
$vn_top_level_collection_id = array_shift($t_item->get('ca_collections.hierarchy.collection_id', array("returnWithStructure" => true)));
$vn_test = $t_item->get('ca_collections.hierarchy.parent_id');
//require(__CA_THEME_DIR__."/views/StoryMode/src/php/liquify.php");

?>
<!-- water-ripples effect -->


<div class="row" style="">
	<div class='col-xs-12 navTop'><!--- only shown at small screen size -->
		{{{previousLink}}}{{{resultsLink}}}{{{nextLink}}}
	
	</div><!-- end detailTop -->
	<div class='navLeftRight col-xs-1 col-sm-1 col-md-1 col-lg-1'>
		<div class="detailNavBgLeft">
			{{{previousLink}}}{{{resultsLink}}}
		</div><!-- end detailNavBgLeft -->
	</div>
	<!-- end col -->
	<div class='col-xs-12 col-sm-10 col-md-10 col-lg-10'>
		<div class="container">
			<div class="row " id="headrow">
				<div class='col-md-11 col-lg-11'>
						 <H4>{{{^ca_collections.preferred_labels.name}}}</H4>
						<H6>{{{^ca_collections.type_id}}}{{{<ifdef code="ca_collections.idno">, idno: ^ca_collections.idno</ifdef>}}}</H6>
						{{{<ifdef code="ca_collections.parent_id">
							
							<!-- <H6>Path:	<unit relativeTo="ca_collections.hierarchy.parent_id" delimiter="  &nbsp;>>"><l>^ca_collections.preferred_labels.name </l></unit> -->
							<!-- <unit relativeTo="ca_collections" delimiter=" >> "><l>^ca_collections.hierarchy.preferred_labels.name</l></unit> -->
						<!-- </H6> -->

						<H5><unit relativeTo="ca_collections.hierarchy.parent_id" restrictToTypes="approaches" delimiter=" ➔ ">
				<l>&#9641; ^ca_collections.preferred_labels.name</l></unit>
				<unit relativeTo="ca_collections.hierarchy.parent_id" restrictToTypes="collection_type_subcollection" delimiter=" ; "><l> ➔ &#9714; ^ca_collections.preferred_labels.name</l></unit>
			</H5>
			</ifdef>}}}
				
			{{{<ifdef code="ca_collections.response.contributor">
					<h6>Response-able:</h6></ifdef>}}}
					{{{<ifcount code="ca_collections.response.contributor"><br /></ifcount>}}}
					{{{<unit relativeTo="ca_collections.response" delimiter="<br />">
						<span class="">^ca_collections.response.contributor</span>
						<ifdef code="ca_collections.response.contributorcomment">
							<span class="" id="">Response-able comment: ^ca_collections.response.contributorcomment</span>
						</ifdef>
					</unit>}}}
						<?php
                        if ($vn_pdf_enabled) {
                            print "<div style='margin-top:7px;' class='exportCollection'><span class='glyphicon glyphicon-file'></span> ".caDetailLink($this->request, "Download as PDF", "", "ca_collections", $vn_top_level_collection_id, array('view' => 'pdf', 'export_format' => '_pdf_ca_collections_summary'))."</div>";
                        }
// print_r($vn_test);
?>

										
				</div>
				<!-- <div class='col-xs-1 col-sm-1 col-md-1 col-lg-1'>
					<div id="deliquify" onclick="deliquify()" onmouseover="this.style.cursor = 'pointer';">
						<span>De-Liquify this page!</span>
					</div>
				</div> -->
				<div class="col-md-9 col-lg-6">
				<hr />
			</div>
			</div>
			<div class='row'>
				<div class='col-md-9 col-lg-6'><H6>About:</H6>
				<!-- {{{<unit relativeTo="ca_collections" delimiter="<br/>"><span class="trimText">^ca_collections.description</span></unit>}}} -->
						{{{<unit relativeTo="ca_collections" delimiter="<br/>"><span class="trimText">^ca_collections.description</span></unit>}}}
						{{{<ifdef code="ca_collections.long_description">
							<unit relativeTo="ca_collections" delimiter="<br/>">
							<div class='unit longText'>
							<ifdef code="ca_collections.long_description.long_description_text">
							
								<span class="trimText1">^ca_collections.long_description.long_description_text</span>
							</div></unit>
						</ifdef>}}}
					

					{{{<ifcount code="ca_collections.related" restrictToTypes="approach" restrictToTypes="collection_type_subcollection" min="1" max="1"><br /><H6>Related collection:</H6></ifcount>}}}
					{{{<ifcount code="ca_collections.related" restrictToTypes="approach" restrictToTypes="collection_type_subcollection"  min="2"><br /><H6>Related collections:</H6><br /></ifcount>}}}
					{{{<ifcount code="ca_collections" restrictToTypes="approach" restrictToTypes="collection_type_subcollection" min="1" ><unit relativeTo="ca_collections_x_collections" delimiter="<br/>"><unit relativeTo="ca_collections" delimiter="<br/>"><l>^ca_collections.preferred_labels.name</l></unit> (^relationship_typename)</ifcount></unit>}}}
					
					{{{<ifcount code="ca_entities" min="1" max="1"><br /><H6>Related people:</H6><br /</ifcount>}}}
					{{{<ifcount code="ca_entities" min="2"><br /><H6>Related people:</H6><br /</ifcount>}}}
					{{{<ifcount code="ca_entities" min="1" max=""><unit relativeTo="ca_entities_x_collections"><unit relativeTo="ca_entities" delimiter="<br/>"><l>^ca_entities.preferred_labels.displayname</l></unit> (^relationship_typename)</unit></ifcount>}}}
					
					<!-- {{{<ifcount code="ca_occurrences" min="1" max="1"><br /><H6>Related occurrence:</H6><br /</ifcount>}}}
					{{{<ifcount code="ca_occurrences" min="2"><br /><H6>Related occurrences:</H6><br /</ifcount>}}}
					{{{<unit relativeTo="ca_occurrences_x_collections"><unit relativeTo="ca_occurrences" delimiter="<br/>"><l>^ca_occurrences.preferred_labels.name</l></unit> (^relationship_typename)</unit>}}} -->
					
					{{{<ifcount code="ca_places" min="1" max="1"><br /><H6>Related place:</H6></ifcount>}}}
					{{{<ifcount code="ca_places" min="2"><br /><H6>Related places:</H6><br /></ifcount>}}}
					{{{<ifcount code="ca_places" min="1"><unit relativeTo="ca_places_x_collections"><unit relativeTo="ca_places" delimiter="<br/>"><l>^ca_places.preferred_labels.name</l></unit> (^relationship_typename)</ifcount></unit>}}}					
				</div>
				<!-- redundant -->
				<!-- <div class='col-md-9 col-lg-6  col-lg-6'>
					{{{<ifdef code="ca_collections.long_description">

						<div class='unit'><h6>Longer Description</h6>
							<ifdef code="ca_collections.long_description.long_description_text">
							<span class="trimText">^ca_collections.long_description.long_description_text</span>
							</ifdef>
							<ifdef code="ca_collections.long_description.long_description_pseudolink">
						
							<span class="trimText">^ca_collections.long_description.long_description_pseudolink</span>
				
							</ifdef>
						</div>

					</ifdef>}}}								
				</div> -->
			</div>
			
			<div class='row'>
			<div class="col-md-9 col-lg-6">
				<hr />
			</div>

				<div class=' col-md-12 col-lg-12'>
				
					<?php
        if ($vb_show_hierarchy_viewer) {
            ?>			
						<div id="collectionHierarchy"><?php print caBusyIndicatorIcon($this->request).' '.addslashes(_t('Loading...')); ?></div>
							<script>
								$(document).ready(function(){
									$('#collectionHierarchy').load("<?php print caNavUrl($this->request, '', 'Collections', 'collectionHierarchy', array('collection_id' => $t_item->get('collection_id'))); ?>"); 
								})
							</script>
						
					<?php
        }
?>				
				</div>
			</div><!-- end col -->
	<div class="row">			
		<div class="col-sm-6 colBorderLeft">
					{{{map}}}
		</div>
			{{{<ifdef code="ca_collections.children"><div class='unit'><unit relativeTo="ca_collections.children" delimiter=" "><l>^ca_collections.children.preferred_labels</l></unit></div></ifdef>}}}
	</div>
	<div class="row">	
		<div class='col-md-9 col-lg-6'>

				<?php
                # Comment and Share Tools

                if ($vn_comments_enabled | $vn_share_enabled) {

                    print '<div id="detailTools">';
                    if ($vn_comments_enabled) {
                        ?>				
										<div class="detailTool"><a href='#' onclick='jQuery("#detailComments").slideToggle(); return false;'><span class="glyphicon glyphicon-comment"></span>Comments (<?php print sizeof($va_comments); ?>)</a></div><!-- end detailTool -->
										<div id='detailComments'><?php print $this->getVar("itemComments");?></div><!-- end itemComments -->
							<?php
                    }
                    if ($vn_share_enabled) {
                        print '<div class="detailTool"><span class="glyphicon glyphicon-share-alt"></span>'.$this->getVar("shareLink").'</div><!-- end detailTool -->';
                    }
                    print '</div><!-- end detailTools -->';
                }
?>
		
		<hr />	
		</div><!-- end col -->
	</div>
			
			{{{<ifcount code="ca_objects" min="1" max="1">
				<div class="row">
				<div class="col-md-9 col-lg-6">
				<div class='caption' style="margin-bottom:9px;">Only one related Object: <!--<l>^ca_objects.preferred_labels.name</l>-->
				</div>
				<div class='unit'><unit relativeTo="ca_objects" delimiter=" "><l>^ca_object_representations.media.large</l></unit>
				</div>				</div>
				</div>

			</ifcount>}}}
			{{{<ifcount code="ca_objects" min="2">
				<div class="row">
					
					<div id="browseResultsContainer">
						<?php print caBusyIndicatorIcon($this->request).' '.addslashes(_t('Loading...')); ?>
					</div><!-- end browseResultsContainer -->
				</div><!-- end row -->
				<script type="text/javascript">
					jQuery(document).ready(function() {
						jQuery("#browseResultsContainer").load("<?php print caNavUrl($this->request, '', 'Search', 'objects', array('search' => 'collection_id:^ca_collections.collection_id'), array('dontURLEncodeParameters' => true)); ?>", function() {
							jQuery('#browseResultsContainer').jscroll({
								autoTrigger: true,
								loadingHtml: '<?php print caBusyIndicatorIcon($this->request).' '.addslashes(_t('Loading...')); ?>',
								padding: 20,
								nextSelector: 'a.jscroll-next'
							});
						});
						
						
					});
				</script>
			</ifcount>}}}
			
		</div><!-- end col -->
	</div>
	<div class='navLeftRight col-xs-1 col-sm-1 col-md-1 col-lg-1'>

		<div class="detailNavBgRight">
			{{{nextLink}}}
		</div><!-- end detailNavBgLeft -->
	</div>
</div><!-- end row -->


<script type="text/javascript">
	jQuery(document).ready(function () {
	$('.trimText').readmore({
		speed: 75,
		maxHeight: 206,
	});
});
jQuery(document).ready(function() {
        $('.trimText1').readmore({
          speed: 75,
          maxHeight: 350
        });
    });
    jQuery(document).ready(function() {
        $('.trimText2').readmore({
          speed: 75,
          maxHeight: 180
        });
    });


//water-ripples-effect:
// for (var i = 1; i <= 2; i++) {
// 	document
// 		.getElementById(`displacementFilter${i}`)
// 		.querySelector('feTurbulence')
// 		.setAttribute('seed', Math.random() * 1000);
// }

//deliquify button action
// function deliquify() {
// 	document.getElementById('liquify').remove();
// 	document.getElementById('liquistyle').remove();
// 	document.getElementById('deliquify-canvas').remove();
// 	document.getElementById('deliquify').remove();

// }
// console.log("i run");

// //inject deliquify button
// const elDelCanvas = document.getElementById('deliquify-canvas').cloneNode(true);
// elDelCanvas.id = ''; // Don't forget :)
// console.log(elDelCanvas);
// // modify node contents with DOM manipulation
// document.getElementById('headrow').appendChild(elDelCanvas);
// console.log('liquify runs!');
	</script>
