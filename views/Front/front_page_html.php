<!-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/linux-libertine" type="text/css"/> 
<link rel="stylesheet" media="screen" href="../StoryMode/" type="text/css"/>  -->
<!-- <link rel="stylesheet" media="screen" href="../../assets/other_fonts/LinLibertine/LinLibertine.css" type="text/css"/>  -->

<!-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/lato" type="text/css"/>  -->
<?php
/** ---------------------------------------------------------------------
 * themes/default/Front/front_page_html : Front page of site
 * ----------------------------------------------------------------------
 * CollectiveAccess
 * Open-source collections management software
 * ----------------------------------------------------------------------
 *
 * Software by Whirl-i-Gig (http://www.whirl-i-gig.com)
 * Copyright 2013 Whirl-i-Gig
 *
 * For more information visit http://www.CollectiveAccess.org
 *
 * This program is free software; you may redistribute it and/or modify it under
 * the terms of the provided license as published by Whirl-i-Gig
 *
 * CollectiveAccess is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTIES whatsoever, including any implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This source code is free and modifiable under the terms of
 * GNU General Public License. (http://www.gnu.org/copyleft/gpl.html). See
 * the "license.txt" file for details, or visit the CollectiveAccess web site at
 * http://www.CollectiveAccess.org
 *
 * @package CollectiveAccess
 * @subpackage Core
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License version 3
 *
 * ----------------------------------------------------------------------
 */
        // print $this->render("Front/featured_set_slideshow_html.php");

?>
<?php	//print $this->render("Front/gallery_slideshow_html.php");?>
<?php	//print $this->render("Front/featured_set_grid_html.php");?>
<style>
	/* div{
		border: 1px solid black;
	} */
	.fadeout {
			/*box-shadow: -6px 8px 860px 348px rgba(134, 255, 140, 0.62),
				inset 16px 17px 29px 36px rgba(134, 255, 140, 0.62);*/
			box-shadow: -6px 8px 860px 348px rgba( 17,128,141, 0.62),
				inset 16px 17px 29px 36px rgba( 17,128,141, 0.62);	
			/* border: green 1px solid; */
			background: none;
			font-family:Linux Libertine;
			max-width:960px;
			margin-top:3vw;
			padding:30px;
			
	}
	.fadeout, .front-modal h1, h2, h3{
		font-family:Linux Libertine Display;
		font-size:1.8em;
	}
	.fadeout, .front-modal p, li{
		font-family:Linux Libertine;
		font-size: 1.3em;
		
	}

    }
</style>
<div class=" front-modal hidden" id="dsg">
	<div class="front-modal-inner">
			<?php include_once('dsg.html.php');?></div>
		</div>
	<div class="row">
		<div class=" col-sm-2 col-lg-3 col-xl-4"></div>
		<div class=" col-xs-12 col-sm-8 col-lg-6 col-xl-4 fadeout">
		<div class="row">

			<h3>WILKOMMEN BEI <b>about::power</b>!</h3>

			<p style="">Diese Seite widmet sich dem Schreiben und Nachdenken über
			Machtverhältnisse. Sie funktioniert als Archiv und als
			Erzählplattform. <b>a</b><b>bout::power</b> ist polyglott und
			kollaborativ. <b>a</b><b>bout::power</b> ist experimentell und bleibt
			unfertig. 
			</p>
			<h3 class="western">about::power als Archiv</h3>
			<p><b>about::power</b> als Archiv, ist experimentelle Infrastruktur.
			Es archiviert nicht sterile Objekte, sondern verwobene und opake
			Materialien in und aus der Welt. Infrastruktur und Archivar::innen
			von <b>about::power</b> sind nicht sicher vor Kontamination und
			wildem affiziert-Werden. <b>about</b><b>::power</b> bedient
			grundlegende Erwartungen an ein digitales Archiv an einem Ort, um sie
			an anderer Stelle zu unterlaufen und versucht so gleichzeitig zu beerben, Kritik zu üben und Keime
			für Neues spriessen zu lassen. Archiv ist für <b>about::power</b>
			nicht Gegenstand, sondern Medium.</p>
			<p><b>about::power</b> als experimentelle Wissens-Infrastruktur
			situiert ihre Sammelwut: Alle Materialien sind mindestens einem 
			<a href="https://<?php echo $_SERVER['SERVER_NAME']; ?>/index.php/Collections/index">
			▩ <i><b>Approach</b></i></a>, zugeordnet. Diese <i>Approaches</i> sind
			Sammlungen mit Beipackzettel. Auf dem Zettel steht: Wer hat
			hier gesammelt? Mit was für einer Methode? Warum?</p>
			<h3 class="western">about::power als Erzählplattform</h3>
			<p><b>about::power</b> als Erzählplattform zeigt sich als Teppich
			von Texten die archivierte Materialien miteinander verbinden. Diese
			Texte werden <a href="https://<?php echo $_SERVER['SERVER_NAME']; ?>/index.php/StoryMode/Index">
				<small>△◠○◡◇</small><i><b> Fabulationen</b></i></a> genannt. Sie ermöglichen es,
			quer zu Kategorien und Suchanfragen durch das Archiv zu streifen.
			Jede <i>Fabulation</i> hat eine Ansprechperson oder -gruppe, nach
			einem Wortspiel von Donna Haraway <i>Response-able</i> genannt.
			<i>Fabulationen</i> haben ausserdem eine Angabe zum Textgenre, die
			helfen kann den Text zu lesen. Der <i>Fabulationsmodus</i> bietet so
			ein Ökosystem für das Zusammenleben ganz unterschiedlicher
			Textarten, die sich potenziell auf dieselben Materialien beziehen.
			Das heisst auch, er erlaubt ein Miteinander verschiedener
			Wissensformen und Denkstile, ohne dass diese sich gegenseitig
			überschreiben. Überall dort, wo <i>Fabulationen</i> sich auf
			dieselben Materialien beziehen, zeigt about::power eine grafische
			Verknüpfung. Wird diese aktiviert, so wird die verknüpfte
			<i>Fabulation</i> Seite an Seite mit der Ausgangsfabulation
			dargestellt. 
			</p>
			<h3 class="western">//</h3>
			<p>aboutpower.net wurde zwischen 2019 und 2024 im Rahmen eines Studiums an der Zürcher Hochschule der Künste entwickelt.</p> 
						
		</div>
		</div>

		<div class="col col-sm-2 col-lg-3 col-xl-4 " id="circle-button-col">
			<div class="container" id="circle-button-container">
			<button class="circle-button" id='button-approaches' onclick="window.location.href = 'https://<?php echo $_SERVER['SERVER_NAME']; ?>/index.php/Collections/index';" >Approaches</button>
				<button class="circle-button" id='button-dsg' onclick="openModal('dsg')">Datenschutz</button>
				<button class="circle-button" id='button-fabulationen' " onclick="window.location.href = 'https://<?php echo $_SERVER['SERVER_NAME']; ?>/index.php/StoryMode/Index';">Fabulationen</button>

			</div>


		</div>
	</div>
	{{{page_view_count}}}
<!-- 
		<div class="row">
		<div class="col-sm-4">
<?php
//print $this->render("Front/gallery_set_links_html.php");
?>
		</div> 
	</div>end row -->
	<script type="text/javascript">
		function openModal(id){
	const myModal = document.querySelector(`#${id}`)
	if(myModal.classList.contains('hidden')){
	myModal.classList.remove('hidden')
	}
	else{
		myModal.classList.add('hidden')
	}
}


		
	</script>
