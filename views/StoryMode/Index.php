<!-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/linux-libertine" type="text/css"/> 
<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/lato" type="text/css"/>  -->
<!-- <link rel="stylesheet" media="screen" href="../../assets/other_fonts/LinLibertine/LinLibertine.css" type="text/css"/>  -->
<!-- <div class="php-debug"> -->
<div class="notificationMessage hidden" id="fab-notify"><span>Welcome to fabulation mode!</span>
<div class="pull-right"><a href="#" onclick="jQuery('.notificationMessage').hide(); return false;"><span class="glyphicon glyphicon-remove"></span></a></div></div>
<?php
//more tests
    // Turn on error reporting:
    error_reporting(E_ERROR);
//ini_set('display_errors', 1);



require_once(__CA_MODELS_DIR__."/ca_lists.php");
require_once(__CA_MODELS_DIR__."/ca_list_items.php");
require_once(__CA_MODELS_DIR__."/ca_attributes.php");
require_once(__CA_MODELS_DIR__."/ca_attribute_values.php");
require_once(__CA_MODELS_DIR__."/ca_metadata_elements.php");
require_once(__CA_MODELS_DIR__."/ca_objects_x_collections.php");
require_once(__CA_MODELS_DIR__."/ca_relationship_type_labels.php");
require_once(__CA_MODELS_DIR__."/ca_entities.php");
require_once(__CA_MODELS_DIR__."/ca_object_representations.php");

require_once(__CA_MODELS_DIR__."/ca_collections.php");
require_once(__CA_MODELS_DIR__."/ca_occurrences.php");
require_once(__CA_MODELS_DIR__."/ca_objects.php");
require_once(__CA_THEME_DIR__."/views/StoryMode/src/php/color-tools.php");
require_once(__CA_THEME_DIR__."/conf/storymode.conf.php");

require_once(__CA_THEME_DIR__."/views/StoryMode/src/php/menu-left.php");
//require_once(__CA_THEME_DIR__."/views/StoryMode/src/php/detail-overlay.php");

// Define the path to the configuration file
$config_file_path = __CA_APP_DIR__.'/conf/local/app.conf';

// Initialize an array to store the configuration data
$app_conf = [];

// Open the configuration file for reading
$file_handle = fopen($config_file_path, 'r');
if ($file_handle) {
    // Read the file line by line
    while (($line = fgets($file_handle)) !== false) {
        // Trim whitespace and skip empty lines
        $line = trim($line);
        if (empty($line)) {
            continue;
        }

        // Skip lines that are comments (assuming comments start with #)
        if (strpos($line, '#') === 0) {
            continue;
        }

        // Split the line into key and value
        list($key, $value) = explode('=', $line, 2);

        // Trim whitespace from key and value
        $key = trim($key);
        $value = trim($value);

        // Check if the value is enclosed in square brackets
        if (preg_match('/^\[(.*)\]$/', $value, $matches)) {
            // Convert the comma-separated string inside brackets to an array
            $value = array_map('trim', explode(',', $matches[1]));
        }

        // Store the key-value pair in the config array
        $app_conf[$key] = $value;
    }

    // Close the file handle
    fclose($file_handle);
} else {
    // Handle the error if the file could not be opened
    echo "Error: Unable to open configuration file.";
    exit;
}




// $fabFull_object = new ca_objects(73);
// print_r($fabFull_object);
MetaTagManager::setWindowTitle($this->request->config->get(app_display_name).": StoryMode");
//for debugging database-returns more easily
function print_nice($array1)
{
    echo '<div class="debug-div">';
    echo '<pre>';
    print_r($array1);
    echo '</pre>';
    echo '</div>';

}

// load javascript
$script = file_get_contents(__DIR__ . '/dist/main.js');
AssetLoadManager::addComplementaryScript($script);

//CHECK IF CACHED STORIES SHOULD BE LOADED?
// Define the cache file path

$cache_test = 1;
if(isset($non_caching_servers)) {
    foreach ($non_caching_servers as $dev_url) {
        if($_SERVER['SERVER_NAME'] == $dev_url) {
            $cache_test = 0;
        }
    }
}


$freshData = false;
// Check if the cache file exists and is not older than 24 hours
if ($do_cache_stories && file_exists($cache_file_path) && time() - filemtime($cache_file_path) < 24 * 60 * 60 * $cache_validity_days && $cache_test == 1) {
    // Read the serialized data from the file
    $serialized_data = file_get_contents($cache_file_path);

    // Unserialize the data to restore the original object
    $cached_data = unserialize($serialized_data);
} else {
    // Generate or fetch the $stories object here



    /*
    Eine kleine Gedächtnisstütze für die abfrage direkt über die tabelle:
    1.	alles läuft über die haupttabellen. wirklich.
    2.	mit Abfragen vom Typ:
                $collectionRecords = ca_collections::find([
                'access' => $publicAccessStatusValue,
                'type_id' => $storiesCollectionTypeItemId,
            ], [
                'returnAs' => 'arrays',
                'sort' => 'ca_collections.idno_sort'
            ]);
        erhält man gefilterte Eintre asu der Datenbanktabelle selber. Da ist aber noch kein Fleisch am Knochen. Um irgend etwas schlaues zu erhalten, brauchen wir jedoch die ids der relevanten zeilen der haupttabelle.
    3.	Mit diesen IDs erstellen wir nun Objekte:

        deren Methoden uns die eigentlich interessanten Daten zurück geben. Diese MEthoden sind beschrieben in: ...
    //print_nice(

    */
    // //besser pro Instanz in app/conf bestimmen was als public gilt!

    // // ################ get access statuses list ()
    // //   print_nice($public_access_settings);
    // $accessStatusesList = ca_lists::find([
    //     'list_code' => 'access_statuses',
    // ], [
    //     'returnAs' => 'arrays',
    // ])[0];
    // //print_r($accessStatusesList);
    // $publicAccessStatus = ca_list_items::find([
    //     'list_id' => $accessStatusesList['list_id'],
    //     'idno' => 'public_access',
    // ], [
    //     'returnAs' => 'arrays',
    // ]);
    // //  print_nice($publicAccessStatus);
    // $publicAccessStatusValue = $publicAccessStatus['item_value'];
    // //  print_r($publicAccessStatusValue);


    // ################ get the right collections from type stories
    $collectionTypesList = ca_lists::find([
        'list_code' => 'collection_types',
    ], [
        'returnAs' => 'arrays',
    ])[0];
    $storiesCollectionType = ca_list_items::find([
        'list_id' => $collectionTypesList['list_id'],
        'idno' => 'stories',
    ], [
        'returnAs' => 'arrays',
    ])[0];
    $storiesCollectionTypeItemId = $storiesCollectionType['item_id'];

    $collectionRecords = [];
    $accessible_states = $app_conf['public_access_settings'] ?? [1];
    // print_nice($accessible_states);
    foreach ($accessible_states as $accessible_state) {
        $results = ca_collections::find([
            'access' => $accessible_state,
            'type_id' => $storiesCollectionTypeItemId,
        ], [
            'returnAs' => 'arrays',
            'sort' => 'ca_collections.idno_sort'

        ]);
        $collectionRecords = array_merge($collectionRecords, $results);
    }

    // //besser pro Instanz in app/conf bestimmen was als public gilt!
    // $dev_test = 0;
    // foreach ($allow_restricted_fab_url as $dev_url) {
    //     if($_SERVER['SERVER_NAME'] = $dev_url) {
    //         $dev_test = 1;
    //     }
    // }
    // if($dev_test = 1) {
    //     $collectionRecordsRestricted = ca_collections::find([
    //         'access' => 2,
    //         'type_id' => $storiesCollectionTypeItemId,
    //     ], [
    //         'returnAs' => 'arrays',
    //         'sort' => 'ca_collections.idno_sort'

    //     ]);
    //     $collectionRecords = array_merge($collectionRecords, $collectionRecordsRestricted);
    // }

    // print_nice($collectionRecords);
    ################# get the specific relationship IDs for various relationships
    $fabulationMaterialRelationTypeLabelId = ca_relationship_type_labels::find([
        'typename' => 'is fabulation-material for',
    ])[0];
    $fabulationMaterialRelationLabel = new ca_relationship_type_labels($fabulationMaterialRelationTypeLabelId);
    $fabulationMaterialRelationTypeID = $fabulationMaterialRelationLabel->get('ca_relationship_type_labels.type_id');

    $methodRelationTypeLabelId = ca_relationship_type_labels::find([
        'typename' => 'is method for',
    ])[0];
    $methodRelationLabel = new ca_relationship_type_labels($methodRelationTypeLabelId);
    $methodRelationTypeID = $methodRelationLabel->get('ca_relationship_type_labels.type_id');

    // ################ get the right narration bits (occurences type narration-bit)

    $occurrenceTypesList = ca_lists::find([
        'list_code' => 'occurrence_types',
    ], [
        'returnAs' => 'arrays',
    ])[0];
    $narrationBitOccurrenceType = ca_list_items::find([
        'list_id' => $occurrenceTypesList['list_id'],
        'idno' => 'narration-bit',
    ], [
        'returnAs' => 'arrays',
    ])[0];
    $narrationBitOccurrenceTypeItemId = $narrationBitOccurrenceType['item_id'];

    $stories = [];
    foreach ($collectionRecords as $collectionRecord) {
        $id = $collectionRecord['collection_id'];

        // $story = [
        // 	'id' => $id,
        // 	'name' => $collectionRecord['idno'],
        // 	'label' => $collectionLabel[0]
        // 	'narrationBits' => [],
        // ];
        //############only here we retrieve the collection object with its methods:
        $collection = new ca_collections($id);
        $collectionLabel = $collection->get('ca_collections.preferred_labels.name', [
            'returnAsArray' => true,

        ]);
        //set a color for story
        $stryColorObj = new stilmittel();
        //	$stryColorObj->random_color_bright();
        $stryColorObj->random_color();
        $stryColor = $stryColorObj->color;
        //	print_nice($stryColor);

        //#######################'responsable' element
        #BUG obtains only first entry
        // $storyResponsable = $collection->get('ca_collections.response', [
        // 	'returnAsArray' => true, ]);
        // 	print_nice($storyResponsable);
        $storyResponsableEnt = $collection->get('ca_collections.response.contributor', [
            'returnAsArray' => true,

    ])[0];
        $storyResponsableComment = $collection->get('ca_collections.response.contributorcomment', [
        'returnAsArray' => true,

    ])[0];
        $storyResponsableEntityName = ca_entities::find(['entity_id' => $storyResponsableEnt], [
            'returnAs' => 'arrays',
        ])[0]['idno'];
        $storyResponsable = [
            'contributor' => $storyResponsableEntityName,
            'role' => $storyResponsableComment,
        ];
        ##another attempt - unfinished:

        $storyResponsablesElementID = ca_metadata_elements::find([
        'element_code' => 'response',
        //'idno' => 'narration-bit',
    ], [
        //'returnAs' => 'arrays',
    ])[0];
        $storyResponsablesAttributeValues = ca_attribute_values::find([
            'element_id' => $storyResponsablesElementID,
            //'idno' => 'narration-bit',
        ], [
            //'returnAs' => 'arrays',
        ])[0];

        ####################################
        ##################get fab Material
        #TODO this can be simplified, as only one reference to a material is suggested. for more contectualisation use approaches


        $fabMaterialRelation = ca_objects_x_collections::find([
            'collection_id' => $id,
            'type_id' => $fabulationMaterialRelationTypeID,
        ], [
            'returnAs' => 'arrays',
        ])[0];
        //print_nice($fabMaterialRelation);
        if(!empty($fabMaterialRelation)) {
            $fabMaterialID = $fabMaterialRelation['object_id'];
        } else {
            $fabMaterialID = 0;

        }
        //   print_nice($collectionLabel[0].": fabmaterialid = ".$fabMaterialID);
        if($fabMaterialID !== 0) {


            $fabObjectRecord = ca_objects::find([
                'object_id' => $fabMaterialID,
            ], [
                'returnAs' => 'arrays',
            ])[0];

            $fabFull_object = new ca_objects($fabMaterialID);
            $fabObjectLabel = $fabFull_object->get('ca_objects.preferred_labels.name');
            $fabObjectTypeID = $fabFull_object->get('ca_objects.type_id');
            $fabObjectType = new ca_list_items($fabObjectTypeID);
            $fabObjectTypeIdno = $fabObjectType->get('ca_list_items.idno');
            $fabObjectMedia = $fabFull_object->get('ca_object_representations.media.medium', array('returnAsLink' => false));
            trim($fabObjectMedia, '"');
            ###uncomment if large media rep is needed
            // $fabObjectMediaLarge = $fabFull_object->get('ca_object_representations.media.large'/*, array('returnAsLink' => true)*/);
            // trim($fabObjectMediaLarge, '"');
            $fabMaterialDetailPage = $fabFull_object->get('ca_objects.link');




            $fabMaterial = [
                'id' => $fabMaterialID,
                'name' => $fabObjectRecord['idno'],
                'label' => $fabObjectTitle,
                'typeID' => $fabObjectTypeID,
                'type' => $fabObjectTypeIdno,
                'media' => $fabObjectMedia,
        //		'mediaLarge' => $fabObjectMediaLarge,
                'detailPage' => $fabMaterialDetailPage,

            ];

        } else {
            $fabMaterial = 0;
        }
        // print_nice($fabMaterial);
        // print_nice($fabObjectTypeID);

        ############################################
        ############## Description / Subtitle und genre
        $fabDescription = $collection->get('ca_collections.description');
        $fabGenres = $collection->get('ca_collections.genre', [
            'returnAsArray' => true,

        ]);


        // collecting fetched data in $story
        $story = [
            'id' => $id,
            'idno' => $collectionRecord['idno'],
            'label' => $collectionLabel[0],
            'description' => $fabDescription,
            'fabMaterial' => $fabMaterial,
            'responsable' => $storyResponsable,
            'genres' => $fabGenres,
            'stryColor' => $stryColor,
            'narrationBits' => [],
        ];
        //	print_nice($story);

        $relatedOccurrencesCodes = $collection->get('ca_occurrences.idno', [
            'returnAsArray' => true,
        ]);
        //		print_nice($relatedOccurrencesCodes);
        foreach ($relatedOccurrencesCodes as $occurrenceCode) {

            $occurrenceRecord = ca_occurrences::find([
                'access' => $accessible_state,
                'idno' => $occurrenceCode,
                'type_id' => $narrationBitOccurrenceTypeItemId,
        //			'deleted' => 0, //unnötig
            ], [
                'returnAs' => 'arrays',
                'sort' => $occurrenceRecord['rank'],

            ])[0];
            //		print_nice($narrationBitOccurrenceTypeItemId);
            //print_nice($occurrenceRecord[rank]);

            if ($occurrenceRecord) {
                $occurrenceId = $occurrenceRecord['occurrence_id'];


                $narrationBitInstance = new ca_occurrences($occurrenceId);
                $storyText1 = $narrationBitInstance->get('storytext_above');
                $storyText2 = $narrationBitInstance->get('storytext_aside');
                $storyText3 = $narrationBitInstance->get('storytext_below');
                $storyObjectCaption = $narrationBitInstance->get('story_object_caption');
                $storyBitStatusID = $narrationBitInstance->get('story_bit_status');
                $storyBitStatus = new ca_list_items($storyBitStatusID);
                $storyBitStatusIdno = $storyBitStatus->get('ca_list_items.idno');


                $narrationBit = [
                    'rank'	=> $occurrenceRecord['rank'],

                    'id' => $occurrenceId,
                    'name' => $occurrenceCode,
                    'p1' => $storyText1,
                    'p2' => $storyText2,
                    'p3' => $storyText3,
                    'caption' => $storyObjectCaption,
                    'status' => $storyBitStatusIdno,
                    'objects' => [],
                    'collection-as-material',
                ];
                //	print_nice($narrationBit);
                //getting collections as material
                //##############################OR MAYBE BETTER JUST GETTING
                // $materialCollectionId = $narrationBitInstance->get(
                // 	'ca_collections.collection_id',
                // [
                // 	'returnAsArray' => true,
                // ]);

                //getting objects-as-material
                $relatedObjectIds = $narrationBitInstance->get(
                    'ca_objects.object_id',
                    [
                    'returnAsArray' => true,
                    ]
                );

                foreach ($relatedObjectIds as $objectId) {
                    $objectRecord = ca_objects::find([
                        'object_id' => $objectId,
                    ], [
                        'returnAs' => 'arrays',
                    ])[0];
                    // print_r($objectRecord);
                    // print_r("<br><br>");
                    $full_object = new ca_objects($objectId);
                    // print_r($full_object);
                    // print_r("<br><br>");
                    $objectLabel = $full_object->get('ca_objects.preferred_labels.name');
                    $objectTypeID = $full_object->get('ca_objects.type_id');
                    $objectType = new ca_list_items($objectTypeID);
                    $objectTypeIdno = $objectType->get('ca_list_items.idno');
                    $objectMedia = $full_object->get('ca_object_representations.media.medium'/*, array('returnAsLink' => true)*/);
                    trim($objectMedia, '"');
                    $objectMediaOrig = $full_object->get('ca_object_representations.media.original', array('returnAsLink' => false));
                    // $objectDate = $full_object->get('ca_objects.date', ['returnAsArray' => true,'returnAsLink' => false,]);

                    //echo("hello");
                    ###uncomment if large media rep is needed

                    // $objectMediaLarge = $full_object->get('ca_object_representations.media.large'/*, array('returnAsLink' => true)*/);
                    // trim($objectMediaLarge, '"');
                    $detailPage = ""."Detail/objects/".$objectId;

                    $long_description = $full_object->get('ca_objects.long_description');

                    $description = $full_object->get('ca_objects.description');
                    //http://aboutpower.localhost/index.php/Detail/objects/41
                    // $objectType = .$full_object->get('ca_object_type');
                    // print "The title of the object is ".$full_object->get('ca_objects.preferred_labels.name')."<br/>\n";    // get the preferred name of the object
                    // print "more picture on it: ".$full_object->get('ca_object_representations.media.medium', array('returnAsLink' => true))."<br/>\n";    //
                    // print "more picture on it: ".$full_object->get('ca_objects.type')."<br/>\n";
                    //print $objectRecord->get('ca_object_representations.media.medium', array('returnAsLink' => true));
                    if ($objectRecord) {
                        $narrationBit['objects'][] = [
                            'id' => $objectId,
                            'name' => $objectRecord['idno'],
                            'label' => $objectTitle,
                            'typeID' => $objectTypeID,
                            'type' => $objectTypeIdno,
                            'media' => $objectMedia,
                            'mediaOrig' => $objectMediaOrig,
                            // 'mediaLarge' => $objectMediaLarge,
                            'detailPage' => $detailPage,
                            'longDescription' => $long_description,
                            'description' => $description,
                            //'dates' => $objectDate,

                        ];
                    }
                }

                $story['narrationBits'][] = $narrationBit;

            }
        }
        ///Sort the narration bits
        $ranks = array();
        foreach ($story['narrationBits'] as $my_object) {
            $ranks[] = $my_object->rank; //any object field
            //print_nice($ranks);
        }

        array_multisort($ranks, SORT_ASC, $story['narrationBits']);


        //print_nice($story['narrationBits']);

        //print_nice($story[narrationBits]);




        $stories[] = $story;




    }


    // Assign the $stories object to $cached_stories
    $cached_data[0] = date('y.m.d - H:i');
    $cached_data[1] = $stories;
    // Serialize the $stories object

    $serialized_stories = serialize($cached_data);
    // delete old cache file
    unlink($cache_file_path);
    // Write the serialized data to the cache file
    file_put_contents($cache_file_path, $serialized_stories);
    $freshData = true;
}

// //define landing in fabulation mode
// // do that in a js conf
// if($first_fab_random) {
//     $first_fab = 'random-9j34mfe0';
// } else {
//     $first_fab = $first_fab_idno;
// }


//set a color for the site-design
$siteColorObj = new stilmittel();
$siteColorObj->random_color_bright();
$siteColor = $siteColorObj->color;


?>
<!-- </div>  -->
<!-- 
<div id="loading">
  <img id="loading-image" src="/themes/StoryMode/assets/other_graphics/loading.gif" alt="Loading..." />
</div> -->
<div id="StoryMode" data-first_fab="<?php echo $first_fab; ?>"   data-fresh="<?php echo $freshData; ?>" data-timestamp="<?php echo $cached_data[0]; ?>" data-stories="<?php echo htmlspecialchars(json_encode($cached_data[1])); ?>" data-sitecolor="<?php echo $siteColor;

?>"></div>
