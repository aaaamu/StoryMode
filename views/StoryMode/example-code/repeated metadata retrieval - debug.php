<?php
//############# get the models
    require_once(__CA_MODELS_DIR__."/ca_lists.php");
    require_once(__CA_MODELS_DIR__."/ca_list_items.php");
    require_once(__CA_MODELS_DIR__."/ca_attributes.php");
    require_once(__CA_MODELS_DIR__."/ca_attribute_values.php");
    require_once(__CA_MODELS_DIR__."/ca_metadata_elements.php");
    require_once(__CA_MODELS_DIR__."/ca_objects_x_collections.php");
    require_once(__CA_MODELS_DIR__."/ca_relationship_type_labels.php");
    require_once(__CA_MODELS_DIR__."/ca_entities.php");

    require_once(__CA_MODELS_DIR__."/ca_collections.php");
    require_once(__CA_MODELS_DIR__."/ca_occurrences.php");
    require_once(__CA_MODELS_DIR__."/ca_objects.php");
// ################ get the right collections from type stories
	$collectionTypesList = ca_lists::find([
		'list_code' => 'collection_types',
	], [
		'returnAs' => 'arrays',
	])[0];
	$storiesCollectionType = ca_list_items::find([
		'list_id' => $collectionTypesList['list_id'],
		'idno' => 'stories',
	], [
		'returnAs' => 'arrays',
	])[0];
	$storiesCollectionTypeItemId = $storiesCollectionType['item_id'];

	$collectionRecords = ca_collections::find([
		'access' => $publicAccessStatusValue,
		'type_id' => $storiesCollectionTypeItemId,
	], [
		'returnAs' => 'arrays',
		'sort' => 'ca_collections.idno_sort'

	]);
//#################[]
foreach ($collectionRecords as $collectionRecord) {
    $id = $collectionRecord['collection_id'];
    
    // $story = [
    // 	'id' => $id,
    // 	'name' => $collectionRecord['idno'],
    // 	'label' => $collectionLabel[0]
    // 	'narrationBits' => [],
    // ];
//############only here we retrieve the collection object with its methods:
    $collection = new ca_collections($id);
    $collectionLabel = $collection->get('ca_collections.preferred_labels.name', [
        'returnAsArray' => true,

		]);
// #########[]
    $storyResponsableEnt = $collection->get('ca_collections.response.contributor', [
		'returnAsArray' => true,

   ])[0];
   $storyResponsableComment = $collection->get('ca_collections.response.contributorcomment', [
	'returnAsArray' => true,

    ])[0];
    $storyResponsableEntityName = ca_entities::find(['entity_id' => $storyResponsableEnt],[
        'returnAs' => 'arrays',
    ])[0]['idno'];
    $storyResponsable = [
        'contributor' => $storyResponsableEntityName,
        'role' => $storyResponsableComment,
    ];

//######################[]
}
?>
