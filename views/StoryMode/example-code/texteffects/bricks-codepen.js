// 'use strict';
// import {
//     gsap
// } from "gsap";
// import {
//     TextPlugin
// } from "gsap/TextPlugin";

// gsap.registerPlugin(TextPlugin);
// gsap.registerPlugin(SplitText);

function myAnimation() {
    var tl = gsap.timeline(),
        split = new SplitText("#quote", {
            type: "words,chars"
        }),
        words = split.words; //an array of all the divs that wrap each character

    gsap.set("#quote", {
        perspective: 400
    });

    tl.to(words, {
        duration: 1.5,
        fontWeight: "300", // Use fontWeight instead of --weight
        ease: "none",
        color: gsap.utils.random(["hsl(+=0, 70%, 20%)", "hsl(+=40, 70%, 20%)", "hsl(-40, 70%, 20%)"]), // Fix color notation
        stagger: {
            each: 0.4,
        }
    });
}
