<?php
	$script = file_get_contents(__DIR__ . '/dist/edit.js');
	AssetLoadManager::addComplementaryScript($script);
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->


<!-- <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header pull-right">
            <ul class="nav navbar-nav">
                <li>
                    <button class="btn btn-md btn-success">Login</button>
                </li>
            </ul>
        </div>
    </div>
</nav> -->
<div class="container">
    <div class="row tabs-container">
        <div class="col-sm-offset-1 col-sm-10">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1default" data-toggle="tab">Rules</a></li>
                        <li><a href="#tab2default" data-toggle="tab">Events</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="story-title">Title:</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="story-title" story-title="story-title"
                                               placeholder="Enter story-title">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label col-sm-2" for="story-title">Resonsable:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="story-title" story-title="story-title"
                                               placeholder="Enter story-title">
                                    </div><label class="control-label col-sm-1" for="story-title">Role:</label><div class="col-sm-4">
                                        <input type="text" class="form-control" id="story-title" story-title="story-title"
                                               placeholder="Enter story-title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="genre1">Genre:</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" id="genre1" name="genre1">
                                            <option>Genre1</option>
                                            <option>Genre2</option>
                                            <option>Genre3</option>
                                            <option>Genre4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="type">Description:</label>

                                    <div class="col-sm-10">
										<textarea class="form-control" height="150px" name="descriptionRules">A short Summary or introduction into your Story
										</textarea>
                                    </div>
                                </div>
                                <div class="parent-border col-sm-offset-0 col-sm-12">
                                   
                                    
									
									<div class="row">
										<div class="form-group">
											<div class="col-sm-offset-11">
												<button type="button" id="deleteRow" class="btn btn-success btn-circle btn-lg"><i
														class="glyphicon glyphicon glyphicon-trash"></i></button>
											</div>
                                 	   </div>

										<div class="form-group col-sm-11">
										
											<div class="col-sm-7">
												<span class="">Maximal drei Absätze können zu einer Archivalie geschrieben werden</span>

												<div class="form-floating">
													<textarea class="form-control" placeholder="Optional Paragraph" id="p1" style="height: 60px"></textarea>
													<label for="p1">Absatz 1 (optional)</label>
												</div>
												<div class="form-floating">
													<textarea class="form-control" placeholder="Main Paragraph" id="p2" style="height: 100px"></textarea>
													<label for="p2">Haupt-Absatz</label>
												</div>
												<div class="form-floating">
													<textarea class="form-control" placeholder="Optional Paragraph" id="p3" style="height: 60px"></textarea>
													<label for="p3">Absatz 3 (optional)</label>
												</div>
										
												<div class="mb-3 form-check">
													<input type="text" class="form-text-input" id="example-text">
													<label class="form-check-label" for="example-text">Check me out text</label>
												</div>

											</div>
											<div class="col-xs-12 col-sm-4">
												<div class="mb-3">
													<label for="ca_object-reference" class="form-label">Referenced Material</label>
													<input type="url" class="form-control" id="ca_object_reference">
												</div>
												<div class="mb-3 form-check">
													<input type="checkbox" class="form-check-input" id="exampleCheck1">
													<label class="form-check-label" for="exampleCheck1">Check me out</label>
												</div>
												<div class="mb-3">

													<label class="form-check-label" for="example-text">Feeling: <input class="form-control" name=f type="text" oninput="check(this)"></label>
												</div>
													<script>
													function check(input) {
														console.log(input);
													if (!(input.value == "good" ||
														input.value == "fine" ||
														input.value == "tired")) {
															console.log('"' + input.value + '" is not a feeling.');
														input.setCustomValidity('"' + input.value + '" is not a feeling.');
													} else {
														console.log('input good');
														// input is fine -- reset the error message
														input.setCustomValidity('');
													}
													}
													</script>
											</div>
										</div>
										<div class="form-group">
                                    <div class="col-sm-offset-11">
                                        <button type="button" id="addRow" class="btn btn-success btn-circle btn-lg"><i
                                                class="glyphicon glyphicon-plus"></i></button>
                                    </div>
                                </div>
                                    </div>
                                   
                                
                                </div>
                                <div id="container">
                                </div>
                                
                            </form>
                        </div>
                       
						
						
						<div class="tab-pane fade" id="tab2default">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="name1">Title:</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name1" name="name1"
                                               placeholder="Enter name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="type">Type:</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" id="type" name="type">
                                            <option>Type1</option>
                                            <option>Type2</option>
                                            <option>Type3</option>
                                            <option>Type4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="type">Description:</label>

                                    <div class="col-sm-10">
                  						<textarea class="form-control" rows="4" cols="50" name="descriptionEvents">XYZ
                  						</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="row">
		<div class="col">
			<button class="btn btn-md btn-success">Save</button>
		</div>
	</div>
	
</div>


<!-- 
<h1>Edit Story</h1>
<form>
<div class="row">
<div class="col-xs-12">
<h2>Edit Storytitle</h2></div>

<div class="col-xs-12"></div>
</div>
</div>
<div class="row">
	<div class="col-sm-6">
	<span class="">Maximal drei Absätze können zu einer Archivalie geschrieben werden</span>

	<div class="form-floating">
  <textarea class="form-control" placeholder="Optional Paragraph" id="p1" style="height: 60px"></textarea>
  <label for="p1">Absatz 1 (optional)</label>
</div>
<div class="form-floating">
  <textarea class="form-control" placeholder="Main Paragraph" id="p2" style="height: 100px"></textarea>
  <label for="p2">Haupt-Absatz</label>
</div>
<div class="form-floating">
  <textarea class="form-control" placeholder="Optional Paragraph" id="p3" style="height: 60px"></textarea>
  <label for="p3">Absatz 3 (optional)</label>
</div>
 
  <div class="mb-3 form-check">
    <input type="text" class="form-text-input" id="example-text">
    <label class="form-check-label" for="example-text">Check me out text</label>
  </div>
  <br />
  </div>	
 	<div class="col-xs-12 col-md-3">
		<div class="mb-3">
			<label for="ca_object-reference" class="form-label">Referenced Material</label>
			<input type="url" class="form-control" id="ca_object_reference">
		</div>
		<div class="mb-3 form-check">
			<input type="checkbox" class="form-check-input" id="exampleCheck1">
			<label class="form-check-label" for="exampleCheck1">Check me out</label>
		</div>
		<div class="mb-3">

 			 <label class="form-check-label" for="example-text">Feeling: <input class="form-control" name=f type="text" oninput="check(this)"></label>
		</div>
			<script>
			function check(input) {
				console.log(input);
			if (!(input.value == "good" ||
				input.value == "fine" ||
				input.value == "tired")) {
					console.log('"' + input.value + '" is not a feeling.');
				input.setCustomValidity('"' + input.value + '" is not a feeling.');
			} else {
				console.log('input good');
				// input is fine -- reset the error message
				input.setCustomValidity('');
			}
			}
			</script>
	</div>

</div>
<div class="row">
	<div class="col-xs-12">
<button type="submit" class="btn btn-primary">Submit Fabulation</button>
</div>
</div>
</form> -->
