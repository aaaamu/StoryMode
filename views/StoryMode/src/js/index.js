/*

#########################
Window objects:
objIndex
theObj
*/
'use strict';
/* global document, story, element, console, objIndex, window */
//load modules
import { createLinks } from './createLinks';
import { initializer } from './initializer';
import { readAndRoute } from './routing';
//load classes:
import Navbar from './lib/domTools';
import { displayCacheTime } from './lib/domTools';
//load prototypes (will then be availlable everywhere even if the method is not called here)
import convertToRGB from './lib/colorTools';
import lightenDarkenColor from './lib/colorTools';
import { truncateTextbased } from './lib/domTools';
// import storyModeConf from './conf.json' with { type: 'json' };
// console.log(storyModeConf);

document.addEventListener('DOMContentLoaded', () => {
	window.conf = require('./conf.json');
	console.log(window.conf);
	const element = document.getElementById('StoryMode');
	const siteColor = element.getAttribute('data-siteColor');
	const cacheAge = element.getAttribute('data-timestamp');
	const freshData = element.getAttribute('data-fresh');
	const firstFab = element.getAttribute('data-first_fab');
	console.log(firstFab);
	// Get the root element
	// var rubidu = element.querySelector('.story__item');
	// rubidu.style.setProperty('background-color', siteColor);
	const stories = JSON.parse(element.getAttribute('data-stories'));
	window.objIndex = createLinks(stories);
	console.log(stories);
	console.log(objIndex);
	//console.log(element);
	//This is not used at the moment
	window.breadCrumbs = [];

	const argsInitializer = {
		element: element,
		stories,
	};

	const theObj = new initializer(argsInitializer);

	theObj.initializeAll();

	//routing:
	//	###############################################
	// reads the given url and orders actions accordingly

	readAndRoute(theObj);

	// initializing the different elements of the left menuBar
	const singleNav = new Navbar('single-story');
	const crumbNav = new Navbar('crumbs');
	displayCacheTime(cacheAge, freshData);

	//	const filterReload = new Navbar('filter-reload');

	let timeout = false; // holder for timeout id
	let delay = 450; // delay after event is "complete" to run callback

	// window.resize callback function

	// window.resize event listener
	window.addEventListener('resize', function () {
		// clear the timeout
		clearTimeout(timeout);
		// start timing for event "completion"
		timeout = setTimeout(readAndRoute(theObj), delay);
	});

	// console.log(document.querySelector('.single-story-drawer'));
});

//some global declarations
// String.prototype.convertToRGB = function () {
// 	if (this.length != 6) {
// 		throw 'Only six-digit hex colors are allowed.';
// 	}

// 	var aRgbHex = this.match(/.{1,2}/g);
// 	var aRgb = [
// 		parseInt(aRgbHex[0], 16),
// 		parseInt(aRgbHex[1], 16),
// 		parseInt(aRgbHex[2], 16),
// 	];
// 	return aRgb;
// };
// window.onload = function () {
// 	truncateTextbased(); // Call your function here
// };
