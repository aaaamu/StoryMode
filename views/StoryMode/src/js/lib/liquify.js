jQuery(document).ready(function () {
	$('.trimText').readmore({
		speed: 75,
		maxHeight: 206,
	});
});
//water-ripples-effect
for (var i = 1; i <= 2; i++) {
	document
		.getElementById(`displacementFilter${i}`)
		.querySelector('feTurbulence')
		.setAttribute('seed', Math.random() * 1000);
}
//deliquify button action
function deliquify() {
	const element = document.getElementById('liquify');
	element.remove();
	const stylo = document.getElementById('liquistyle');
	stylo.remove();
	const bottone = document.getElementById('deliquify');
	bottone.remove();
}
//inject deliquify button
const elDelCanvas = document.getElementById('deliquify-canvas').cloneNode(true);
elDelCanvas.id = ''; // Don't forget :)
console.log(elDelCanvas);
// modify node contents with DOM manipulation
document.getElementById('headrow').appendChild(node);
console.log('liquify runs!');
