/**
 * search a story with a chosen key and value pair
 * @param {string} objKey - The search key.
 * @param {string} value - The search key.
 * @returns {object} story, the chosen story
 */
/* global document, console */
import { lightenDarkenColor } from './colorTools';
import { randomAddClass } from './domTools';

/**
 * This will show a notification to the User
 * works ONLY if notfication div is already present in the HTML
 */
export function notifyUser({ notifyPage, notifyMsg, notifyKey }) {
	let elNotify;
	switch (notifyPage) {
		case 'StoryMode':
			elNotify = document.getElementById('fab-notify');

			break;
		default:
			if (!elNotify) {
				elNotify = document.querySelector('.notificationMessage');
			}
			break;
	}
	elNotify.firstChild.textContent = notifyMsg;
	//console.log('check cookies' + cookieTool.checkCookie(notifyKey));
	if (cookieTool.checkCookie(notifyKey) == 0) {
		elNotify.classList.remove('hidden');

		cookieTool.setCookie(notifyKey, 1);
	}
	console.log(document.cookie);
}

export const cookieTool = {
	setCookie: function (cName, cValue, exDays) {
		//cookie registry:
		const regEx = new RegExp(
			'^(?:.*;)?\\s*' + cName + '\\s*=\\s*([^;]+)(?:.*)?$'
		);
		if (document.cookie.match(regEx) == null) {
			let keyCookie = this.getCookie('keyCookie');
			keyCookie += cName;
			document.cookie = 'keyCookie=' + `${keyCookie}, `;
		}

		const d = new Date();
		d.setTime(d.getTime() + exDays * 24 * 60 * 60 * 1000);
		let expires = 'expires=' + d.toUTCString();
		document.cookie = cName + '=' + cValue + ';' + expires + ';path=/';
	},

	getCookie: function (cName) {
		let name = cName + '=';

		let decodedCookie = decodeURIComponent(document.cookie);
		let ca = decodedCookie.split(';');
		for (let i = 0; i < ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	},
	checkCookie: function (cName) {
		const regEx = new RegExp(
			'^(?:.*;)?\\s*' + cName + '\\s*=\\s*([^;]+)(?:.*)?$'
		);
		if (document.cookie.match(regEx) == null) {
			return 0;
		} else {
			let cookieV = this.getCookie(cName);
			if (cookieV == '0') {
				return 0;
			} else {
				return cookieV;
			}
		}
	},
	deleteAllCookies: function () {
		document.cookie.split(';').forEach(function (c) {
			document.cookie = c
				.replace(/^ +/, '')
				.replace(
					/=.*/,
					'=;expires=' + new Date().toUTCString() + ';path=/'
				);
		});
	},
};

export function chooseStory(objKey, value) {
	const element = document.getElementById('StoryMode');
	const stories = JSON.parse(element.getAttribute('data-stories'));
	const story = stories.find((element) => element[objKey] === value);

	return story;
}
function removeTags(str) {
	if (str === null || str === '') return false;
	else str = str.toString();
	return str.replace(/(<([^>]+)>)/gi, '');
}
export function refineOverlay(sColor, elOverlayOuter, fabObjLinkTarget) {
	const elOverlayInner = elOverlayOuter.querySelector('.overlay-inner');
	const elLoading = elOverlayOuter.querySelector('#loading');
	const elOverlayButtons = elOverlayOuter.querySelector('#overlay-buttons');
	const detailLink = document.querySelector('#detail-link');
	detailLink.setAttribute('href', fabObjLinkTarget);

	elOverlayOuter.classList.remove('hidden');
	elLoading.classList.remove('hidden');
	const elIframe = document.querySelector('iframe');

	// set up the event handler on main page
	window.addEventListener('message', (evt) => {
		if (evt.source === iframe.contentWindow) {
			console.log(evt.data);
		}
	});
	elIframe.onload = function (evt) {
		console.log('onload');
		if (elIframe.contentDocument.body) {
			elIframe.contentDocument.body
				.querySelector('nav')
				.classList.add('hidden');
			elIframe.contentDocument.body
				.querySelector('.detailNavBgLeft')
				.classList.add('hidden');
			elIframe.contentDocument.body
				.querySelector('.detailNavBgRight')
				.classList.add('hidden');
			// elIframe.contentDocument.body.querySelector('.container').style
			// 	.margin-top='-10px';
			elIframe.contentDocument.querySelector('body').style.paddingTop =
				'1%';
			document.getElementById(
				'close-overlay'
			).firstChild.style.backgroundColor = sColor;
		}
		elLoading.classList.add('hidden');
		elOverlayInner.classList.remove('hidden');
		elOverlayButtons.classList.remove('hidden');
	};
}

export function refineObjRep(elObj, elObjRep, objects, elStry) {
	//console.log(objects);
	//check if therre is a related object
	if (objects.length > 0) {
		//if there would be several related objects, that would be a mistake, log an error:
		if (objects.length > 1) {
			// console.log(
			// 	'one narration bit in' +
			// 		elStry.id +
			// 		'is linked to multiple objects. That should not be the case. will continue with first linked object'
			// );
		}
		function decideObjShape() {
			if (!elObjRep.classList.contains('missing-object')) {
				//this sets the item randomly round or square - later to be replaced by a content-related function
				randomAddClass(elObjRep, 'round');
				// randomAddClass(elObjRep, 'blackout');
				// randomAddClass(elObjRep, 'whiteout');
				randomAddClass(elObjRep, 'fadeout');
			}
		}
		decideObjShape();
		//object/element inherits database id of related object
		//console.log(objects[0].type);
		//use description in absence of media:

		function useDesc(longpref) {
			if (longpref === 1) {
				if (objects[0].longDescription) {
					elObjRep.innerHTML = `<span>${objects[0].longDescription}</span>`;
				} else if (objects[0].description) {
					elObjRep.innerHTML = `<span>${objects[0].description}</span>`;
				}
			} else {
				if (objects[0].description) {
					elObjRep.innerHTML = `<span>${objects[0].description}</span>`;
				} else if (objects[0].longDescription) {
					elObjRep.innerHTML = `<span>${objects[0].longDescription}</span>`;
				}
			}
			elObjRep.style.backgroundColor = lightenDarkenColor(
				elStry.dataset.linecolor,
				110
			);
			elObjRep.style.padding = '1em';
			elObjRep.firstChild.classList.add('show-desc');
			elObjRep.classList.remove('round');
		}
		switch (objects[0].type) {
			case 'quote':
				if (objects[0].media) {
					elObjRep.innerHTML = objects[0].media;
					//console.log(objects[0].media);
				} else {
					const elBlockQuote = document.createElement('blockquote');

					elBlockQuote.setAttribute('cite', '');
					elObjRep.append(elBlockQuote);
					//	console.log(objects);

					if (objects[0].description) {
						//console.log('description used');

						elBlockQuote.innerHTML = objects[0].description;
					} else if (objects[0].longDescription) {
						//console.log('long description found');
						elBlockQuote.innerHTML = removeTags(
							objects[0].longDescription
						);
					}
					elObjRep.style.backgroundColor = lightenDarkenColor(
						elStry.dataset.linecolor,
						110
					);
					elObjRep.style.padding = '1em';
					elObjRep.firstChild.classList.add('show-desc');
					elObjRep.classList.remove('round');
				}
				// elObjRep.style.backgroundColor = lightenDarkenColor(
				// 	elStry.dataset.linecolor,
				// 	140
				// );
				//elObjRep.style.backgroundColor = 'white';
				break;
			case 'text':
			case 'dictionary_entry':
			case 'testimony':
			case 'email':
				if (objects[0].media) {
					elObjRep.innerHTML = objects[0].media;
					//console.log(objects[0].media);
				} else {
					useDesc(1);
				}
				break;
			case 'radio_broadcast':
			case 'audio_recording':
			case 'interview':
			case 'podcast':
			case 'sound':
			case 'time_based':
			case 'video_recording':
			case 'film':
			case 'moving_image':
			case 'presentation':
			case 'tv_broadcast':
				//console.log('timebased!');
				if (objects[0].media) {
					function isVideoSource(sourceUrl) {
						// Extract the file extension from the URL
						const fileExtension = sourceUrl
							.split('.')
							.pop()
							.toLowerCase();

						// Define an array of video file extensions
						const videoExtensions = [
							'mp4',
							'webm',
							'ogg',
							'avi',
							'mov',
							'flv',
							'mkv',
							'm4v',
						];

						// Check if the file extension is in the list of video extensions
						return videoExtensions.includes(fileExtension);
					}
					const response = objects[0].mediaOrig;

					// Extract the video source from the response
					const startSrcIndex =
						response.indexOf('src="') + 'src="'.length;
					const endSrcIndex = response.indexOf('"', startSrcIndex);
					const mediaSrc = response.substring(
						startSrcIndex,
						endSrcIndex
					);
					if (isVideoSource(mediaSrc)) {
						// Create a native HTML video player element
						const videoElement = document.createElement('video');
						videoElement.setAttribute('muted', 'muted');
						videoElement.setAttribute('playsinline', '');
						//				videoElement.setAttribute('autoplay', '');
						videoElement.setAttribute('loop', '');

						videoElement.setAttribute('preload', 'auto'); // Preload video for better performance
						videoElement.classList.add('video-material');
						videoElement.innerHTML = `<source src="${mediaSrc}" type="video/mp4">`;
						// videoElement.addEventListener('click', (e) => {
						// 	e.stopPropagation(); // Prevent the click event from bubbling up
						// });
						// elObjRep.addEventListener('click', (e) => {
						// 	// Check if the click target is the video element itself (not the controls)
						// 	if (!e.target.closest('video')) {
						// 		const link =
						// 			elObjRep.querySelector('.video-link');
						// 		if (link) {
						// 			window.location.href = link.href; // Navigate to the link URL
						// 		}
						// 	}
						// });
						// Add the video player element to the container
						elObjRep.appendChild(videoElement);
					} else {
						const audioElement = document.createElement('audio');
						audioElement.setAttribute('controls', ''); // Add controls attribute to enable player controls
						audioElement.setAttribute('preload', 'auto'); // Preload audio for better performance
						audioElement.innerHTML = `<source src="${mediaSrc}" type="audio/mp4">`;

						// Add the video player element to the container
						elObjRep.appendChild(audioElement);
					}

					// Update the element with cleaned response
					// elObjRep.firstElementChild.removeAttribute('width');
					// elObjRep.firstElementChild.removeAttribute('height');
					// const playButton = elObjRep.querySelector(
					// 	'.vjs-big-play-button'
					// );
					// playButton.width = '5em';
					// playButton.height = '5em';
				} else {
					useDesc(0);
				}
				break;

			default:
				//check if database object has media
				if (objects[0].media) {
					elObjRep.innerHTML = objects[0].media;
					//console.log(objects[0].media);
				} else {
					useDesc(0);
				}
		}

		//if there is a firstChild it should be the detail link

		// elObjRepImg.style.width = '400px';
		// elObjRepImg.style.height = '400px';

		// itemImg.setAttribute('height', 'auto');

		//elObjRep.append(objects[0].mediaLarge);
		// const elObjRepMed = document.createElement('img');
		// elObjRepMed.append(objects[0].mediaLarge);
		// elObjRep.append(elObjRepMed);
	} else {
		// // If you want to tolerate storybits without  objects
		// elObjRep.classList.add('hidden');
		// //if you want an error message if there is NO related object use instead:
		// const p = document.createElement('p');
		// p.textContent =
		// 	'This bit of narration is not linked to an archived material. This is probably an Error \u000D ............................. \u000D In case you are an editor of this story, please consider thefollowing: If you want to accentuate a significant Gap in the Archive, use an Object of Type Gap. If you absolutely must publissh a story while you do not have the linked materials ready yet, please use the material of the type placeholder.';
		// elObjRep.append(p);
		// elObjRep.classList.add('missing-object');
	}

	return { elObjRep, elObj };
}
