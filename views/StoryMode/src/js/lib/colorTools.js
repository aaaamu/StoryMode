/**
 * Get a random rgb color.
 * @returns {string} A random rgb color.
 */
export function randomColor() {
	return (
		'rgb(' +
		Math.trunc(Math.random() * 255) +
		', ' +
		Math.trunc(Math.random() * 255) +
		', ' +
		Math.trunc(Math.random() * 255) +
		')'
	);
}
export default String.prototype.convertToRGB = function () {
	if (this.length != 6) {
		throw 'Only six-digit hex colors are allowed.';
	}

	var aRgbHex = this.match(/.{1,2}/g);
	var aRgb = [
		parseInt(aRgbHex[0], 16),
		parseInt(aRgbHex[1], 16),
		parseInt(aRgbHex[2], 16),
	];
	return aRgb;
};

export function lightenDarkenColor(col, amt) {
	//this was written by Pimp Trizkit: https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
	var usePound = false;
	if (col[0] == '#') {
		col = col.slice(1);
		usePound = true;
	}

	var num = parseInt(col, 16);

	var r = (num >> 16) + amt;

	if (r > 255) r = 255;
	else if (r < 0) r = 0;

	var b = ((num >> 8) & 0x00ff) + amt;

	if (b > 255) b = 255;
	else if (b < 0) b = 0;

	var g = (num & 0x0000ff) + amt;

	if (g > 255) g = 255;
	else if (g < 0) g = 0;

	return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
}
