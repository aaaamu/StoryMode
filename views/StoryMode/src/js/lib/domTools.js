/**
 * Remove all child DOM nodes from a given element.
 * @param {HTMLElement} parent - The parent DOM element.
 */
/*global setTimeout, document, e, observer, MutationObserver, console, requestAnimationFrame, performance*/
export function removeAllChildNodes(parent) {
	while (parent.firstChild) {
		parent.removeChild(parent.firstChild);
	}
}
export function randomAddClass(element, cssClass) {
	const bool = Math.trunc(Math.random() * 2);
	bool ? element.classList.add(cssClass) : element.classList.remove(cssClass);
}
export function animate({ timing, draw, duration, onFinish }) {
	let start = performance.now();

	requestAnimationFrame(function animate(time) {
		// timeFraction goes from 0 to 1
		let timeFraction = (time - start) / duration;
		if (timeFraction > 1) timeFraction = 1;

		// calculate the current animation state
		let progress = timing(timeFraction);

		draw(progress); // draw it

		if (timeFraction < 1) {
			requestAnimationFrame(animate);
		}
		if (timeFraction >= 1) {
			console.log('----------------------finished');
			onFinish();
		}
	});
}
export function truncateTextbased() {
	const truncatedTextElements = document.querySelectorAll('.show-desc');

	truncatedTextElements.forEach((truncatedText) => {
		const computedStyle = window.getComputedStyle(truncatedText);
		const maxHeight = parseInt(
			computedStyle.getPropertyValue('max-height'),
			10
		);

		// Temporarily set position to 'absolute' to ignore height constraints by ancestors
		const originalPosition = truncatedText.style.position;
		truncatedText.style.position = 'absolute';

		// Set the element's height to auto temporarily to measure the full content height
		truncatedText.style.height = 'auto';
		const isOverflowing = truncatedText.scrollHeight > maxHeight;

		// If there's overflow, truncate the text
		if (isOverflowing) {
			// Truncate the text as desired...
			// For simplicity, let's just set the text to a shorter version with '...'
			const originalText = truncatedText.textContent;
			truncatedText.textContent =
				originalText.slice(0, 100) + '... (click to read more)';
		}

		// Restore the element's original position and height
		truncatedText.style.position = originalPosition;
		truncatedText.style.height = ''; // Remove inline height style
	});
}
export function displayCacheTime(cachingTimestamp, freshData) {
	const elCacheAge = document.getElementById('cache-age-title');
	elCacheAge.textContent = freshData
		? 'Un-Cached Data'
		: 'Cached Data. Last retrieval: ' + cachingTimestamp;
	if (freshData) {
		elCacheAge.parentNode.parentNode.setAttribute(
			'style',
			'background-color:#19b9c1;;'
		);
	}
}
export default class Navbar {
	constructor(handle) {
		this.drawer = document.querySelector('.' + handle + '-drawer');

		this.target = document.querySelector('.' + handle + '__btn');
		this.isOpen = false;
		this.navList = this.drawer.querySelector('.' + handle + '__list');
		this.closeBtn = this.drawer.querySelector('.' + handle + '__close');
		this.menuEntry = this.drawer.querySelectorAll(
			'.' + handle + '__menuentry'
		);
		this.setEventListeners();
		// Options for the observer (which mutations to observe)
		this.obsConfig = { attributes: false, childList: true, subtree: true };
		// Create an observer instance linked to the observerCallback function
		this.observer = new MutationObserver(this.observeNewEntriesCallback);
		// Start observing the target node for configured mutations
		this.observer.observe(this.navList, this.obsConfig);
		// bind causes a fixed `this` context to be assigned to an object
		//	this.close = this.close.bind(this);
	}

	// Callback function to execute when mutations are observed
	observeNewEntriesCallback(mutationsList, observer) {
		// Use traditional 'for loops' for IE 11
		for (const mutation of mutationsList) {
			if (mutation.type === 'childList') {
				console.log('A child node has been added or removed.');
				const newBorn = document.querySelector('.newborn');
				newBorn.addEventListener('click', () => {
					//this.close();
				});
				newBorn.classList.remove('newborn');
			}
		}
	}

	setEventListeners() {
		this.target.addEventListener('click', (e) => {
			if (!this.isOpen) {
				return this.open();
			}

			return this.close();
		});
		this.closeBtn.addEventListener('click', () => {
			this.close();
		});
		//this will only be executed on site load later entries in breadcrumbs are not recognized
		for (const userChoice of this.menuEntry) {
			userChoice.addEventListener('click', () => {
				this.close();
			});
		}
	}

	open() {
		this.isOpen = !this.isOpen;
		this.drawer.classList.add('open-drawer');
		setTimeout(() => {
			this.navList.classList.add('slide-in');
		}, 30);
	}
	close() {
		this.isOpen = !this.isOpen;
		this.navList.classList.remove('slide-in');
		setTimeout(() => {
			this.drawer.classList.remove('open-drawer');
		}, 200);
	}
}
export const container = [
	`story-container1`,
	`story-container2`,
	`story-container3`,
];
String();
