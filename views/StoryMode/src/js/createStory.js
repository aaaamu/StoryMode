import { dynNav } from './dynNav';
/* global document, story, element, console, objIndex, window */

/**
 * Create a new story representation object.
 * @param {Object} argsStory - The story creation arguments.
 * @param {string} argsStory.storyIdno - The story's idno.
 *
 * @returns The story representation object.
 */
export function createStory({ storyIdno, createLinkPreviewElement }) {
	//	console.log(container);
	let elStory = document.querySelector('#story-' + storyIdno);
	const stryColor = elStory.getAttribute('data-linecolor');

	// get the story's rectangle
	let rectStory = elStory.getBoundingClientRect();

	let elsStoryItemsObjRep = elStory.querySelectorAll('.story__item__obj-rep');
	let elsStoryItemsObj = elStory.querySelectorAll('.story__item__obj');

	let lineCanvas = elStory.querySelector('.line-canvas1');
	//console.log(lineCanvas);

	// retrieve the drawing context from the canvas
	let lineContext = lineCanvas.getContext('2d');
	//console.log(lineContext);

	return {
		getFreshDOMData() {
			//	console.log(container);
			elStory = document.querySelector('#story-' + storyIdno);

			// get the story's rectangle
			rectStory = elStory.getBoundingClientRect();

			elsStoryItemsObjRep = elStory.querySelectorAll(
				'.story__item__obj-rep'
			);
			elsStoryItemsObj = elStory.querySelectorAll('.story__item__obj');

			lineCanvas = elStory.querySelector('.line-canvas1');
			//console.log(lineCanvas);

			// retrieve the drawing context from the canvas
			lineContext = lineCanvas.getContext('2d');
			//console.log(lineContext);
		},
		clearCanvas() {
			lineContext.clearRect(0, 0, lineCanvas.width, lineCanvas.height);
		},
		setCanvasSize() {
			// set canvas height to be the same as the story's
			lineCanvas.height = elStory.clientHeight;
			lineCanvas.width = elStory.clientWidth;
		},
		// decideObjShape() {
		// 	elsStoryItemsObjRep.forEach((elStoryItemObjRep) => {
		// 		if (!elStoryItemObjRep.classList.contains('missing-object')) {
		// 			//this sets the item randomly round or square - later to be replaced by a content-related function
		// 			randomAddClass(elStoryItemObjRep, 'round');
		// 			// randomAddClass(elStoryItemObjRep, 'blackout');
		// 			// randomAddClass(elStoryItemObjRep, 'whiteout');
		// 			randomAddClass(elStoryItemObjRep, 'fadeout');
		// 		}
		// 	});
		// },
		drawLine() {
			this.clearCanvas();
			this.getFreshDOMData();

			//this.decideObjShape();
			//set the canvas size
			this.setCanvasSize();

			// iterate over the StoryItems
			let elStoryItemObjRepPrevious;
			elsStoryItemsObjRep.forEach((elStoryItemObjRep) => {
				// const stryItemHeight = elStoryItemObjRep.clientHeight;
				// //console.log(elStoryItemObjRep);
				if (elStoryItemObjRepPrevious) {
					this.drawLineSegment({
						elStoryItemObjRepA: elStoryItemObjRepPrevious,
						elStoryItemObjRepB: elStoryItemObjRep,
						rectStory,
					});
				}
				elStoryItemObjRepPrevious = elStoryItemObjRep;
			});

			//coloring the obj-rep-container has been disregarded

			//this.setObjBgColor(stryColor);
			//add the dots
			//this.drawLinks(storyIdno);
		},
		drawLineSegment({ elStoryItemObjRepA, elStoryItemObjRepB, rectStory }) {
			lineContext.lineWidth = 3;
			//            lineContext.strokeStyle = 'rgba(255,100,100,0.85)';
			lineContext.strokeStyle = stryColor;
			const rectStoryItemObjRepA =
				elStoryItemObjRepA.getBoundingClientRect();
			const rectStoryItemObjRepB =
				elStoryItemObjRepB.getBoundingClientRect();
			const rectStoryObj = 1;
			//		//console.log({ elStoryItemObjRepA, elStoryItemObjRepB, rectStory });
			// create position dictionaries for both items
			const posStoryItemA = {
				x:
					rectStoryItemObjRepA.x -
					rectStory.x +
					rectStoryItemObjRepA.width / 2,
				y:
					rectStoryItemObjRepA.y -
					rectStory.y +
					rectStoryItemObjRepA.height * 0.5,
			};

			const posStoryItemB = {
				x:
					rectStoryItemObjRepB.x -
					rectStory.x +
					rectStoryItemObjRepB.width / 2,
				y:
					rectStoryItemObjRepB.y -
					rectStory.y +
					rectStoryItemObjRepB.height * 0.5,
			};

			// draw the line
			// const yCorr = 80;
			// const xCorr = 50;
			lineContext.beginPath(posStoryItemA.x, posStoryItemA.y);
			lineContext.moveTo(posStoryItemA.x, posStoryItemA.y);
			//bezier
			//		console.log(posStoryItemA.x);
			lineContext.bezierCurveTo(
				posStoryItemA.x,
				posStoryItemA.y + 170 * (Math.random() / 1.5 + 1),
				posStoryItemB.x + 2 * (Math.random() / 4 + 1),
				posStoryItemB.y - 80 * (Math.random() / 1.5 + 1),
				posStoryItemB.x,
				posStoryItemB.y
			);

			//			//console.log('still running');
			// lineContext.stroke();

			// lineContext.beginPath();
			// lineContext.arc(100, 100, 80, 0, 2 * Math.PI);

			// lineContext.bezierCurveTo(
			// 	posStoryItemA.x,
			// 	posStoryItemA.y + 170 * (Math.random() / 1.5 + 1),
			// 	posStoryItemB.x + 2 * (Math.random() / 4 + 1),
			// 	posStoryItemB.y - 80 * (Math.random() / 1.5 + 1),
			// 	posStoryItemB.x,
			// 	posStoryItemB.y
			// );

			// draw a circle around line-stations  resp object positions:
			// lineContext.stroke();
			// lineContext.beginPath();
			// lineContext.arc(
			// 	posStoryItemB.x,
			// 	posStoryItemB.y,
			// 	40,
			// 	0,
			// 	2 * Math.PI
			// );
			lineContext.stroke();

			//	console.log();
		},
		drawLinks: function () {
			////console.log(elsStoryItemsObj);
			const linkData = objIndex.multipleObj;
			//	console.log(elsStoryItemsObj);

			for (const testObj of elsStoryItemsObj) {
				const objId = String(testObj.id).slice(4);
				//	console.log(objId);
				if (linkData[objId]) {
					const linkedObjData = { ...linkData[objId] };
					delete linkedObjData[storyIdno];

					for (const singleLinkHandle of Object.keys(linkedObjData)) {
						const singleLinkData = linkedObjData[singleLinkHandle];
						console.log(singleLinkData);

						const arg = {
							linkedObj: testObj,
							singleLinkData,
						};
						drawDots(arg);
					}
				}
			}

			function drawDots({ linkedObj, singleLinkData }) {
				// Define a variable to cache the link preview element for this specific call
				let cachedLinkPreview = null;
				//console.log(singleLinkData);
				//console.log('singlelinkdata');
				// Create dotSVG element...
				//SVG variante
				const dotSVG = document.createElementNS(
					'http://www.w3.org/2000/svg',
					'svg'
				);
				dotSVG.setAttribute('width', '20px');
				dotSVG.setAttribute('height', '20px');
				dotSVG.classList.add('dot-svg');

				const svgDot = document.createElementNS(
					'http://www.w3.org/2000/svg',
					'circle'
				);

				svgDot.cx.baseVal.value = 10;
				svgDot.cy.baseVal.value = 10;
				svgDot.r.baseVal.value = 8;
				svgDot.setAttribute('stroke', singleLinkData.stryColor);
				svgDot.setAttribute('fill', singleLinkData.stryColor);
				//	console.log(singleLinkData);
				//###########################################################

				svgDot.addEventListener('click', function linkEvent() {
					//the svgDot is now a button, triggering the method dynNav.setBase Method with the following arguments-object:
					const userAction = new dynNav({
						storyIdnoRef: singleLinkData.stryIdno,
						refItemId: `item-${singleLinkData.narrBitId}`,
						storyIdnoBase: storyIdno,
						baseItemId: linkedObj.parentElement.id,
						actionOrigin: 'script',
					});
					userAction.setBase();
				});

				let elLinkPreviewNow;

				dotSVG.addEventListener('mouseover', function () {
					// Check if the link preview is already cached
					const cachedLinkPreview =
						dotSVG.getAttribute('data-link-preview');

					// If cached, convert it to a DOM element
					if (cachedLinkPreview) {
						const parser = new DOMParser();
						elLinkPreviewNow = parser.parseFromString(
							cachedLinkPreview,
							'text/html'
						).body.firstChild;
					} else {
						// Otherwise, create a new link preview element
						elLinkPreviewNow = createLinkPreviewElement(
							singleLinkData.stryIdno
						);

						// Ensure that createLinkPreviewElement() returns a valid DOM element
						if (!(elLinkPreviewNow instanceof HTMLElement)) {
							console.error(
								'createLinkPreviewElement() did not return a valid DOM element.'
							);
							return;
						}

						// Cache the link preview element
						dotSVG.setAttribute(
							'data-link-preview',
							elLinkPreviewNow.outerHTML
						);
					}

					// Get the position of dotSVG
					const dotRect = dotSVG.getBoundingClientRect();
					const offsetX = dotRect.width / 2; // Offset from left edge of dotSVG
					const offsetY = dotRect.height / 2; // Offset from top edge of dotSVG

					// Calculate available space on the left and right sides of dotSVG
					const spaceLeft = dotRect.left - window.scrollX;
					const spaceRight =
						window.innerWidth -
						(dotRect.left + dotRect.width) +
						window.scrollX;
					elLinkPreviewNow.style.left =
						dotRect.left + dotRect.width + 'px';

					const spaceAbove = dotRect.top;
					const spaceBelow = window.innerHeight - dotRect.bottom;

					const positionAbove = spaceAbove > spaceBelow;

					elLinkPreviewNow.id = 'elLinkPreviewNow';
					document.body.appendChild(elLinkPreviewNow); // Append to body
					const previewRect =
						elLinkPreviewNow.getBoundingClientRect();
					if (spaceRight > spaceLeft) {
						elLinkPreviewNow.style.left =
							dotRect.left + dotRect.width + 0 + 'px'; // Position to the right of dotSVG
					} else {
						document.getElementById('elLinkPreviewNow').style.left =
							previewRect.x - previewRect.width - 20 + 'px';
					}

					const verticalPosition = positionAbove
						? dotRect.top -
						  elLinkPreviewNow.offsetHeight +
						  window.scrollY -
						  10
						: dotRect.bottom + window.scrollY - 10;
					document.getElementById('elLinkPreviewNow').style.top =
						verticalPosition + 'px';
				});

				//	Add mouseout event listener to hide link preview

				dotSVG.addEventListener('mouseout', function () {
					const elLinkPreviewNow =
						document.querySelector('.link-preview');
					if (elLinkPreviewNow) {
						elLinkPreviewNow.remove();
					}
				});

				// dotSVG.addEventListener('mouseout', function () {
				// 	// Hide the link preview element
				// 	elLinkPreviewNow.style.display = 'none';
				// });

				//################################################################
				//svgDot.setAttribute('onclick', 'linkEvent()');

				svgDot.setAttribute('stroke-opacity', '0.8');
				svgDot.setAttribute('fill-opacity', '0.3');
				svgDot.setAttribute('stroke-width', '3px');

				svgDot.setAttribute('stroke-dasharray', '');
				svgDot.classList.add('svg-dot');
				dotSVG.appendChild(svgDot);
				const linkedObjRep = linkedObj.querySelector(
					' .story__item__obj-rep-container'
				);

				linkedObjRep.appendChild(dotSVG);

				//#######unfinished work on link-preview:
				// dotSVG.addEventListener('focusin', (event) => {
				// 	console.log(
				// 		'triggered hover event' + singleLinkData.stryIdno
				// 	);
				// 	const elPreview = document.querySelector(
				// 		'#' + singleLinkData.stryIdno + '_link-preview'
				// 	);
				// 	const rectLinkedObjRep = linkedObjRep.getBoundingClientRect();
				// 	if (rectLinkedObjRep.x < window.innerWidth) {
				// 		elPreview.style['left'] =
				// 			rectLinkedObjRep.x +
				// 			rectLinkedObjRep.width +
				// 			window.pageXOffset +
				// 			50 +
				// 			'px';
				// 	} else {
				// 		elPreview.style['left'] =
				// 			rectLinkedObjRep.x -
				// 			elPreview.width -
				// 			50 +
				// 			window.pageXOffset +
				// 			'px';
				// 	}
				// 	elPreview.style['top'] =
				// 		rectLinkedObjRep.y -
				// 		rectLinkedObjRep.height / 2 +
				// 		window.pageYOffset +
				// 		'px';
				// 	//elPreview.style['top'] = '500px';
				// 	console.log(rectLinkedObjRep.y);
				// 	//elPreview.style.top = rectLinkedObj
				// 	elPreview.classList.remove('hidden');
				// 	dotSVG.setAttribute('width', '40px');
				// 	dotSVG.setAttribute('height', '40px');
				// 	svgDot.cx.baseVal.value = 20;
				// 	svgDot.cy.baseVal.value = 20;
				// 	svgDot.r.baseVal.value = 18;
				// });
				//console.log(linkedObjRep);
			}
		},
		setObjBgColor(stryColor) {
			let t = stryColor.slice(1).convertToRGB();
			const objBgColor = String(
				'rgba(' + t[0] + ', ' + t[1] + ', ' + t[2] + ', 0.175)'
			);
			const objs = elStory.querySelectorAll('.story__item__obj');
			for (const obj of objs) {
				obj.style['background-color'] = objBgColor;
			}
		},
	};
}
