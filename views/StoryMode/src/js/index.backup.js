/*If npm is not working, run:
nvm install 'lts/*' --reinstall-packages-from=default --latest-npm 

*/
'use strict';

import * as $ from 'jquery';
import jqueryCsv from 'jquery-csv';
import { doc } from 'prettier';
import { createStory } from './createStory';
import { menuNav } from './menuNav';
import { createLinks } from './createLinks';

document.addEventListener('DOMContentLoaded', () => {
	const element = document.getElementById('StoryMode');
	const stories = JSON.parse(element.getAttribute('data-stories'));
	const objIndex = createLinks(stories);
	console.log(stories);
	console.log(objIndex);

	let displayNr = 1;
	const menuContainer = document.getElementById('menu-container');

	//for every story create a container
	// for (let i = 0; i < stories.length; i++) {
	// 	let story = stories[i];
	//TEMPORARY
	const elStoryContainer2 = document.createElement('div');
	elStoryContainer2.classList.add('story-container1');
	element.append(elStoryContainer2);

	for (let story of stories) {
		const elStryContainer1 = document.createElement('div');
		elStryContainer1.classList.add('story-container1');
		elStryContainer1.setAttribute('id', story.idno);
		//elStryContainer1.setAttribute('data-linecolor', story.stryColor);

		// Construct title element
		const titleContainer = document.createElement('div');
		titleContainer.classList.add('title-container');
		titleContainer.setAttribute('id', `${story.idno}_title`);
		const storyTitleEl = document.createElement('h2');
		storyTitleEl.textContent = story.label;
		titleContainer.append(storyTitleEl);

		const lineCanvas = document.createElement('canvas');
		lineCanvas.classList.add('line-canvas');

		const linkCanvas = document.createElement('canvas');
		linkCanvas.classList.add('link-canvas');

		const stry = document.createElement('div');
		stry.classList.add('story');
		stry.id = story.idno;

		// let h2 = document.createElement('h2');
		// //build the hierarchy of the created html elements
		// h2.append(story.idno);
		// elStryContainer1.append(h2);
		//build the hierarchy of the created html elements

		const narrationBits = story.narrationBits;
		//console.log(narrationBits.length);
		let orientationClass = 'obj-left';
		// nun sollen die einzelnen html elemente erstellt werden für jedes storybit
		for (const narrationBit of narrationBits) {
			const stryItem = document.createElement('div');
			stryItem.classList.add('story__item');
			stryItem.classList.add(orientationClass);
			stryItem.id = narrationBit.id;

			const flipCardInner = document.createElement('div');
			flipCardInner.classList.add('flip-card-inner');

			const flipCard = document.createElement('div');
			flipCard.classList.add('story__item__flip-card');

			const flopside = document.createElement('div');
			flopside.classList.add('flopside');

			const flipside = document.createElement('div');
			flipside.classList.add('flipside');
			//flipside.classList.add(orientationClass);
			const flipsideTitle = document.createElement('h3');
			flipsideTitle.classList.add('flipside-title');
			flipsideTitle.innerHTML =
				'<h3>Details, facts, etc. A possibility to inspect the object</h3>';

			const stryItemObj = document.createElement('div');
			stryItemObj.classList.add('story__item__obj');
			stryItemObj.id = narrationBit.objects[0].id;

			const stryItemObjRepContainer = document.createElement('div');
			stryItemObjRepContainer.classList.add(
				'story__item__obj-rep-container'
			);

			const stryItemObjRep = document.createElement('div');
			stryItemObjRep.classList.add('story__item__obj-rep');
			stryItemObjRep.innerHTML = narrationBit.objects[0].media;
			const itemImg = stryItemObjRep.getElementsByTagName('img');
			for (const oneImg of itemImg) {
				if (oneImg) {
					oneImg.setAttribute('width', '100%');
					oneImg.setAttribute('height', 'auto');
					//	console.log(oneImg);
				}
			}

			// itemImg.setAttribute('height', 'auto');

			//stryItemObjRep.append(narrationBit.objects[0].mediaLarge);
			// const stryItemObjRepMed = document.createElement('img');
			// stryItemObjRepMed.append(narrationBit.objects[0].mediaLarge);
			// stryItemObjRep.append(stryItemObjRepMed);

			const stryItemObjCapt = document.createElement('div');
			stryItemObjCapt.classList.add('story__item__caption');
			stryItemObjCapt.innerHTML = `<p>${narrationBit.caption}</p>`;

			const stryItemTxt = document.createElement('div');
			stryItemTxt.classList.add('story__item__texte');

			const stryItemTxtP1 = document.createElement('div');
			stryItemTxtP1.classList.add('story__item__texte__text1');
			stryItemTxtP1.innerHTML = `<p>${narrationBit.p1}</p>`;

			const stryItemTxtP2 = document.createElement('div');
			stryItemTxtP2.classList.add('story__item__texte__text2');
			stryItemTxtP2.innerHTML = `<p>${narrationBit.p2}</p>`;

			const stryItemTxtP3 = document.createElement('div');
			stryItemTxtP3.classList.add('story__item__texte__text3');
			stryItemTxtP3.innerHTML = `<p>${narrationBit.p3}</p>`;

			stryItemTxt.append(stryItemTxtP1);
			stryItemTxt.append(stryItemTxtP2);
			stryItemTxt.append(stryItemTxtP3);

			stryItemObj.append(stryItemObjRepContainer);

			stryItemObjRepContainer.append(stryItemObjRep);
			stryItemObj.append(stryItemObjCapt);
			flipside.append(flipsideTitle);
			//	flopside.append(stryItemObj);
			flopside.append(stryItemTxt);
			stryItem.setAttribute('id', narrationBit.id);

			flipCardInner.append(flopside);
			flipCardInner.append(flipside);

			flipCard.append(flipCardInner);
			stryItem.append(stryItemObj);

			stryItem.append(flipCard);

			stry.append(stryItem);
			orientationClass =
				orientationClass === 'obj-left' ? 'obj-right' : 'obj-left';
		}
		elStryContainer1.append(titleContainer);
		elStryContainer1.append(lineCanvas);
		elStryContainer1.append(linkCanvas);

		elStryContainer1.append(stry);
		element.append(elStryContainer1);

		/*
		//correct height of each stryItem in the DOM according to contents
		const elsStoryItems = document.querySelectorAll('.story__item');
		for (const elStoryItem of elsStoryItems) {
			const textHeight = elStoryItem.querySelector('.flopside')
				.clientHeight;
			//console.log(textHeight);
			const objHeight = elStoryItem.querySelector('.story__item__obj')
				.clientHeight;
			//console.log(objHeight);
			// let itemHeight = 7;
			let itemHeight = textHeight >= objHeight ? textHeight : objHeight;
			console.log(itemHeight);
			//	itemHeight = `'${itemHeight}px'`;
			//	console.log(itemHeight);
			//elStoryItem.style.height = temP;
			elStoryItem.style.setProperty(
				'height',
				`${itemHeight}px`,
				'important'
			);

			console.log(elStoryItem.style.height);
		}
		//console.log(elsStoryItems);
*/
		// create the object that is expected by the function createStory as arguments
		const args = {};
		args.storyIdno = story.idno;
		args.objIndex = objIndex;
		args.stryColor = story.stryColor;
		//	console.log(args);
		//create (draw the story)
		story = { ...createStory(args), ...story };
		story.decideObjShape();
		// storyObj.setCanvasSize();
		// //storyObj.decideObjShape();
		//		console.log(story);
		story.drawLine();
		// // //storyObj.drawLineSegment();
		menuNav.createMenuEntry({
			storyTitle: story.label,
			storyIdno: story.idno,
		});
		elStryContainer1.classList.add('hidden');
		elStryContainer1.setAttribute('data-display-nr', displayNr);
		displayNr++;
	}
	//display a random story at the beginning
	const randomStoryNr = Math.trunc(Math.random() * stories.length) + 1;
	const randomStory = document.querySelector(
		`[data-display-nr="${randomStoryNr}"]`
	);
	// display specific story on landing ninstead
	//const randomStory = document.querySelector(`[data-display-nr="2"]`);
	randomStory.classList.remove('hidden');
});
