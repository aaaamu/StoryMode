/**
 * Create a new object that indexes all the archive-objects in the selected stories and their linkages.
 * @param {Object} stories - The stories that should be berücksichtigt
 * @returns the objIndex Object
 *
 *
 *
 */
/* global  console, */

export function createLinks(stories) {
	//CHECK DATA-STORIES FOR OBJECTS THAT HAVE MULTIPLE OCCURENCES BY BUILDUNG THAT OBJECT:
	const objIndex = {
		objPerStory: {},
		fabMaterial: {},
		allObj: [],
		multipleObj: {},
	};
	//needed to construct multipleObj
	const multiTester = {};
	for (let i = 0; i < stories.length; i++) {
		let story = stories[i];
		//	console.log(story);

		if (story.fabMaterial !== null) {
			const fabMatId = story.fabMaterial.id;
			//		console.log(story.idno, fabMatId);
			if (!objIndex.fabMaterial[fabMatId]) {
				objIndex.fabMaterial[fabMatId] = [];
			}
			objIndex.fabMaterial[fabMatId].push(story.idno);
			//objIndex.fabMaterial = {...objIndex.fabMaterial, ...fabMatId}
		}
		objIndex.objPerStory[story.idno] = [];
		const narrationBits = story.narrationBits;
		// nun sollen die einzelnen html elemente erstellt werden für jedes storybit
		for (let n = 0; n < narrationBits.length; n++) {
			const narrationBit = narrationBits[n];
			//console.log(narrationBit);

			if (narrationBit.objects.length > 0) {
				const bitObjId = narrationBit.objects[0].id;
				//add to allObj
				objIndex.allObj.push(bitObjId);
				//add to objPerStory
				objIndex.objPerStory[story.idno].push(bitObjId);
				//add to multipleObj

				if (!multiTester[bitObjId]) {
					multiTester[bitObjId] = {};
				}

				multiTester[bitObjId][story.idno] = {};
				multiTester[bitObjId][story.idno].narrBitId = narrationBit.id;
				multiTester[bitObjId][story.idno].stryColor = story.stryColor;
				multiTester[bitObjId][story.idno].stryIdno = story.idno;
				multiTester[bitObjId][story.idno].stryId = story.id;
			} else {
				//		console.log(				'warning, missing object-relation for story Bit ' +					narrationBit.id				);
			}
		}
	}
	//now clean up data
	for (const objId of Object.keys(multiTester)) {
		//clean up allObj (remove duplicates)
		objIndex.allObj = Array.from(new Set(objIndex.allObj));
		// create and clean link data
		let count = Object.keys(multiTester[objId]).length;

		if (count > 1) {
			objIndex.multipleObj[objId] = multiTester[objId];
		}
	}
	return {
		...objIndex,
	};
}
