// document.addEventListener('DOMContentLoaded', function () {
// 	inSiteJS();
// });
function openModal(id) {
	const myModal = document.querySelector(`#${id}`);
	if (myModal.classList.contains('hidden')) {
		myModal.classList.remove('hidden');
	} else {
		myModal.classList.add('hidden');
	}
}

function inSiteJS(validHits) {
	console.log('valid hits:');
	console.log(validHits);
	const elCollectionList = document.querySelector('#collectionsList');
	//const numHits = elCollectionList.getAttribute('data-numhits');
	const numHits = validHits;
	function animate({ timing, draw, duration, onFinish }) {
		let start = performance.now();

		requestAnimationFrame(function animate(time) {
			// timeFraction goes from 0 to 1
			let timeFraction = (time - start) / duration;
			if (timeFraction > 1) timeFraction = 1;

			// calculate the current animation state
			let progress = timing(timeFraction);

			draw(progress); // draw it

			if (timeFraction < 1) {
				requestAnimationFrame(animate);
			}
			if (timeFraction >= 1) {
				console.log('Animation ----------------------finished');
				onFinish();
			}
		});
	}

	/**
	 * Generates an array of unique random numbers within a specified range.
	 *
	 * @param {number} min - The minimum value in the range (inclusive).
	 * @param {number} max - The maximum value in the range (inclusive).
	 * @param {number} count - The number of unique random numbers to generate.
	 * @returns {number[]} An array containing the unique random numbers.
	 * @throws Will throw an error if the count is greater than the range of numbers.
	 */
	function getRandomNumbers(min, max, count) {
		// Ensure the count does not exceed the range of possible unique numbers
		if (count > max - min + 1) {
			throw new Error(
				'Count must be less than or equal to the range of numbers.'
			);
		}

		const numbers = []; // Initialize an empty array to hold the random numbers

		// Continue generating random numbers until the desired count is reached
		while (numbers.length < count) {
			// Generate a random number within the specified range
			const randomNumber =
				Math.floor(Math.random() * (max - min + 1)) + min;

			// Check if the generated number is already in the array
			if (!numbers.includes(randomNumber)) {
				// If the number is unique, add it to the array
				numbers.push(randomNumber);
			}
		}

		// Return the array of unique random numbers
		return numbers;
	}

	/**
	 * Shuffle elements of a view and apply random indentation to selected canvas elements.
	 */
	const numberOfSpecial = getRandomNumbers(1, numHits / 2, 1); // Get a random number of special items
	const specialCanvasDiv = getRandomNumbers(1, numHits - 1, numberOfSpecial); // Get random indices for special items
	console.log('specialCanvasDiv');
	console.log(specialCanvasDiv);

	/**
	 * Generate a random choice from 0 to max-1.
	 * @param {number} max - The upper bound for the random number.
	 * @returns {number} A random integer between 0 and max-1.
	 */
	const randChoice = (max) => {
		return Math.floor(Math.random() * max);
	};

	// Choose a random color from the colorChoices array
	const colorChoice = randChoice(3);
	const colorChoices = ['red', 'black', '#11808d'];

	/**
	 * Apply indentation effect on a canvas element.
	 * @param {number} selectedIndex - The index of the element to apply the effect on.
	 */
	function indentation(selectedIndex) {
		console.log(selectedIndex);

		// Select the canvas element with the specified data-tile-index
		const canvas = document.querySelector(
			`[data-tile-index="${selectedIndex}"] > * > canvas`
		);

		// Get the drawing context of the canvas
		const ctx = canvas.getContext('2d');

		// Set the clipping region to be much larger than the canvas to avoid clipping
		ctx.beginPath();
		ctx.rect(-5000, -5000, canvas.width + 10000, canvas.height + 10000); // Extend clipping region beyond canvas
		ctx.clip();

		// Set the stroke style to the chosen color and line width
		ctx.strokeStyle = colorChoices[colorChoice];
		ctx.lineWidth = 2;

		const cW = canvas.width;
		const cH = canvas.height;

		// Define random line patterns for each side of the canvas
		const randLines = {
			topLine: [
				() => ctx.lineTo(cW, 0), // Top point
				() =>
					ctx.quadraticCurveTo(
						cW *
							(Math.random() < 0.2
								? Math.random() * 0.2
								: Math.random() < 0.8
								? Math.random() * 0.6 + 0.2
								: Math.random() * 0.2 + 0.8),
						cH * Math.random() * 0.1,
						cW,
						0
					), // Quadratic curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / -Math.random(),
						cW * -Math.random(),
						cH * 0.7 * Math.random(),
						cW,
						0
					), // Bezier curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / Math.random(),
						cW / -Math.random(),
						(cH / 0.3) * Math.random(),
						cW,
						0
					), // Bezier curve
			],
			rightLine: [
				() => ctx.lineTo(cW, cH), // Right point
				() =>
					ctx.quadraticCurveTo(
						cW - cW * Math.random() * 0.012,
						cH * Math.random() * 1.1,
						cW,
						cH
					), // Quadratic curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / -Math.random(),
						cW * -Math.random(),
						cH * 0.7 * Math.random(),
						cW,
						cH
					), // Bezier curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / Math.random(),
						cW / -Math.random(),
						(cH / 0.3) * Math.random(),
						cW,
						cH
					), // Bezier curve
			],
			bottomLine: [
				() => ctx.lineTo(0, cH), // Bottom point
				() =>
					ctx.quadraticCurveTo(
						cW *
							(Math.random() < 0.2
								? Math.random() * 0.2
								: Math.random() < 0.8
								? Math.random() * 0.6 + 0.2
								: Math.random() * 0.2 + 0.8),
						cH - cH * (Math.random() * 0.1),
						0,
						cH
					), // Quadratic curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH * Math.random(),
						cW * -Math.random(),
						cH * 0.7 * Math.random(),
						0,
						cH
					), // Bezier curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / Math.random(),
						cW * Math.random(),
						(cH / 0.3) * Math.random(),
						0,
						cH
					), // Bezier curve
			],
			leftLine: [
				() => ctx.lineTo(0, 0), // Left point
				() =>
					ctx.quadraticCurveTo(
						cW * Math.random() * 0.015,
						cH * Math.random() * Math.random(),
						0,
						0
					), // Quadratic curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH * Math.random(),
						cW * -Math.random(),
						cH * 0.7 * Math.random(),
						0,
						0
					), // Bezier curve
				() =>
					ctx.bezierCurveTo(
						cW * Math.random(),
						cH / Math.random(),
						cW * Math.random(),
						(cH / 0.3) * Math.random(),
						0,
						0
					), // Bezier curve
			],
		};

		// Begin a new path and move to the starting point
		ctx.beginPath();
		ctx.moveTo(0, 0);

		// Draw lines based on random choices from the randLines object
		randLines.topLine[randChoice(2)](); // Top line
		randLines.rightLine[randChoice(2)](); // Right line
		randLines.bottomLine[randChoice(2)](); // Bottom line
		randLines.leftLine[randChoice(2)](); // Left line

		// Stroke the path to render the lines
		ctx.stroke();
	}

	// Apply the indentation effect to each selected canvas element
	specialCanvasDiv.forEach((element) => {
		indentation(element);
	});

	function shuffleDown(min, max, speed, intervall) {
		min = min === 1 ? 2 : min;
		if (max === 0) {
			return;
		} else {
			let nrMobile = getRandomNumbers(min, max, 1);
			let mobileItems = getRandomNumbers(0, numHits - 1, nrMobile);
			let arrayShuffle = getRandomNumbers(0, nrMobile - 1, nrMobile);

			let duration = speed * 1000; // Convert speed to milliseconds

			animate({
				timing: function (timeFraction) {
					return timeFraction;
				},
				draw: function (progress) {
					for (let i = 0; i < nrMobile; i++) {
						let chosenTile = document.querySelector(
							`[data-tile-index="${
								mobileItems[arrayShuffle[i]]
							}"]`
						);
						chosenTile.style.transform = `translateY(${i * 100}%)`; // Adjust as needed
					}
				},
				duration: duration,
				onFinish: function () {
					console.log('Animation finished');
				},
			});
		}
	}

	function shuffleExplode(min, max, speed, intervall) {
		min = min === 1 ? 2 : min;
		if (max === 0) {
			return;
		} else {
			let nrMobile = getRandomNumbers(min, max, 1);
			let mobileItems = getRandomNumbers(0, numHits - 1, nrMobile);
			console.log(mobileItems);
			let tilePositions = [];
			mobileItems.forEach((element) => {
				const elTile = document.querySelector(
					`[data-tile-index="${element}"]`
				);
				const tilePosition = {};
				tilePosition.x = elTile.getBoundingClientRect().x;
				tilePosition.y = elTile.getBoundingClientRect().y;
				tilePositions.push(tilePosition);
				//console.log(tilePosition);
			});
			console.log(tilePositions);
			let arrayShuffle = getRandomNumbers(0, nrMobile - 1, nrMobile);
			console.log(arrayShuffle);

			// for (let i = 0; i < nrMobile; i++) {
			// 	let chosenTile = document.querySelector(
			// 		`[data-tile-index="${mobileItems[arrayShuffle[i]]}"]`
			// 	);
			// 	chosenTile.style.order = i; // Adjust index by 1 (1-based index)
			// }

			// console.log(mobileItems);
			let duration = speed * 1000; // Convert speed to milliseconds

			animate({
				timing: function (timeFraction) {
					return timeFraction;
				},
				draw: function (progress) {
					for (let i = 0; i < nrMobile; i++) {
						let chosenTile = document.querySelector(
							`[data-tile-index="${
								mobileItems[arrayShuffle[i]]
							}"]`
						);
						let newX = tilePositions[i].x;
						let newY = tilePositions[i].y;
						chosenTile.style.transform = `translate(${newX}px, ${newY}px)`;
					}
				},
				duration: duration,
				onFinish: function () {
					console.log('Animation finished');
				},
			});
		}
	}

	/**
	 * Shuffles elements of a view by rearranging their order.
	 *
	 * @param {number} min - The minimum value for the number of elements to shuffle.
	 * @param {number} max - The maximum value for the number of elements to shuffle.
	 * @param {number} speed - (Not used in the function, can be removed if not needed elsewhere).
	 * @param {number} interval - (Not used in the function, can be removed if not needed elsewhere).
	 */
	function shuffleInitially(min, max, speed, interval) {
		// Ensure that the minimum value is at least 2
		min = min === 1 ? 2 : min;

		// If max is 0, there's nothing to shuffle
		if (max === 0) {
			return;
		} else {
			// Generate a random number within the range [min, max]
			let nrMobile = getRandomNumbers(min, max, 1)[0];

			// Generate an array of random indices within the range [0, numHits - 1]
			let mobileItems = getRandomNumbers(0, numHits - 1, nrMobile);
			console.log('mobile items');
			console.log(mobileItems);

			let tilePositions = []; // Initialize an array to store the positions of the tiles

			// Iterate over each randomly selected item
			mobileItems.forEach((element) => {
				// Select the element with the corresponding data-tile-index
				const elTile = document.querySelector(
					`[data-tile-index="${element}"]`
				);

				// Get the position of the element and store it in tilePositions
				const tilePosition = {
					x: elTile.getBoundingClientRect().x,
					y: elTile.getBoundingClientRect().y,
				};
				tilePositions.push(tilePosition);
			});
			console.log('tile-positions', tilePositions);

			// Generate a shuffled array of indices within the range [0, nrMobile - 1]
			let arrayShuffle = getRandomNumbers(0, nrMobile - 1, nrMobile);

			// Iterate over the number of mobile items
			for (let i = 0; i < nrMobile; i++) {
				// Select the element with the shuffled data-tile-index
				let chosenTile = document.querySelector(
					`[data-tile-index="${mobileItems[arrayShuffle[i]]}"]`
				);

				// Set the order of the chosen tile to the current index
				chosenTile.style.order = i;
			}
		}
	}

	shuffleInitially(numHits, numHits, 3, 30);

	//shuffleChildren(numHits / 2, numHits, 30, 30);
	function delayExecution(callback, seconds, ...args) {
		setTimeout(() => callback(...args), seconds * 1000); // Convert seconds to milliseconds
	}
	const messUpProb = getRandomNumbers(1, 2, 1);
	if (messUpProb[0] === 1) {
		const messUpTimer = getRandomNumbers(20, 60, 1);
		delayExecution(shuffleExplode, messUpTimer[0], 2, numHits, 30, 30);
		console.log('messup-timer', messUpTimer);
	}

	//swapping descendents of  .collectionsList
	function shuffleDescendants() {
		function animateSwap(parentNode, index1, index2) {
			const tiles = Array.from(
				parentNode.querySelectorAll('.collectionTile')
			);
			if (index1 !== index2) {
				const temp = tiles[index1].cloneNode(true);
				tiles[index1].style.transition = 'transform 0.5s ease';
				tiles[index1].style.transform = `translate(${
					tiles[index2].offsetLeft - tiles[index1].offsetLeft
				}px, ${tiles[index2].offsetTop - tiles[index1].offsetTop}px)`;
				tiles[index2].style.transition = 'transform 0.5s ease';
				tiles[index2].style.transform = `translate(${
					tiles[index1].offsetLeft - tiles[index2].offsetLeft
				}px, ${tiles[index1].offsetTop - tiles[index2].offsetTop}px)`;

				// Swap the elements in the DOM after the animation
				setTimeout(() => {
					tiles[index1].parentNode.replaceChild(temp, tiles[index2]);
					tiles[index1].parentNode.insertBefore(
						tiles[index2],
						tiles[index1]
					);
					tiles[index1].style.transition = '';
					tiles[index1].style.transform = '';
					tiles[index2].style.transition = '';
					tiles[index2].style.transform = '';
				}, 200);
			}
		}

		function shuffleElements(parentNode) {
			const tiles = Array.from(
				parentNode.querySelectorAll('.collectionTile')
			);
			const positions = Array.from({ length: tiles.length }, (_, i) => i); // Array representing initial positions

			for (let i = positions.length - 1; i > 0; i--) {
				const j = Math.floor(Math.random() * (i + 1));
				[positions[i], positions[j]] = [positions[j], positions[i]]; // Swap positions randomly
			}

			for (let i = 0; i < tiles.length; i++) {
				animateSwap(parentNode, i, positions[i]);
			}
		}

		const parentNode = document.querySelector('.collectionsList');
		shuffleElements(parentNode);
	}

	// document.addEventListener('DOMContentLoaded', function() {
	//     shuffleDescendants();
	// });

	//swapping children of same parent:
	function shuffleChildren() {
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		function swapElements(parentNode, index1, index2) {
			const tiles = parentNode.querySelectorAll('.collectionTile');
			const temp = document.createElement('div');
			parentNode.insertBefore(temp, tiles[index1]);
			parentNode.insertBefore(tiles[index2], tiles[index1]);
			parentNode.insertBefore(tiles[index1], temp);
			parentNode.removeChild(temp);
		}

		function shuffleElements() {
			const tiles = document.querySelectorAll('.collectionTile');
			const numberOfTiles = tiles.length;
			const numberOfSwaps = getRandomInt(2, numberOfTiles);

			const shuffledIndices = [];
			for (let i = 0; i < numberOfSwaps; i++) {
				let index = getRandomInt(0, numberOfTiles - 1);
				shuffledIndices.push(index);
			}

			const newIndices = shuffledIndices
				.slice()
				.sort(() => Math.random() - 0.5);

			for (let i = 0; i < numberOfSwaps; i++) {
				const index1 = shuffledIndices[i];
				const index2 = newIndices[i];
				swapElements(tiles[0].parentNode, index1, index2);
			}
		}

		function animateShuffle() {
			setInterval(shuffleElements, getRandomInt(3000, 4000));
		}

		animateShuffle();
	}
	//shuffleChildren();

	// 	jQuery(document).ready(function () {
	// 	$('.trimText').readmore({
	// 		speed: 75,
	// 		maxHeight: 206,
	// 	});
	// });
	//water-ripples-effect
	// for (var i = 1; i <= 2; i++) {
	// 	document
	// 		.getElementById(`displacementFilter${i}`)
	// 		.querySelector('feTurbulence')
	// 		.setAttribute('seed', Math.random() * 1000);
	// }
	// //deliquify button action
	// function deliquify() {
	// 	document.getElementById('liquify').remove();
	// 	document.getElementById('liquistyle').remove();
	// 	document.getElementById('deliquify-canvas').remove();
	// 	document.getElementById('deliquify').remove();

	// }
	// //inject deliquify button
	// const elDelCanvas = document.getElementById('deliquify-canvas').cloneNode(true);
	// elDelCanvas.id = ''; // Don't forget :)
	// console.log(elDelCanvas);
	// // modify node contents with DOM manipulation
	// document.getElementById('headrow').appendChild(elDelCanvas);
	// console.log('liquify runs!');
}
