/*global window, parts*/
import { dynNav } from './dynNav';

export function readAndRoute(theObj) {
	const routingConf = window.conf.routing;
	console.log(routingConf);
	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(
			/[?&]+([^=&]+)=([^&]*)/gi,
			function (m, key, value) {
				vars[key] = value;
			}
		);
		return vars;
	}
	if (window.location.href.indexOf('ir') > -1) {
		const urlVars = getUrlVars();

		const userAction = new dynNav({
			storyIdnoBase: decodeURI(urlVars.sb),
			storyIdnoRef: decodeURI(urlVars.sr),
			baseItemId: decodeURI(urlVars.ib),
			refItemId: decodeURI(urlVars.ir),
			actionOrigin: 'fullUrl',
		});
		userAction.setBase();
	} else {
		if (window.location.href.indexOf('single') > -1) {
			const urlVars = getUrlVars();
			//focus is expected to have as avalue the id of an object referenced in the story
			if (window.location.href.indexOf('focus') > -1) {
				urlVars.focus = decodeURI(urlVars.focus);
			}
			urlVars.single = decodeURI(urlVars.single);
			theObj.displaySingle({
				storyIdno: urlVars.single,
				storyFocus: urlVars.focus,
			});
		} else {
			if (routingConf.firstFabRandom === true) {
				theObj.displayRandomSingle();
			} else {
				console.log(routingConf.firstFabIdno);

				theObj.displaySingle({
					storyIdno: routingConf.firstFabIdno,
				});
			}
		}
	}
}
