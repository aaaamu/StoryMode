import { createStory } from './createStory';
import { menuNav } from './dynNav';
import { refineObjRep } from './lib/dataTools';
import { refineOverlay } from './lib/dataTools';
import { notifyUser } from './lib/dataTools';
import { truncateTextbased } from './lib/domTools';

/* global document, story, element, console */

/**
 * Create a new story representation object.
 * @param {Object} argsInitializer - The story creation arguments.
 * @param {HTMLElement} argsInitializer.element - the DOM node, our stories will be placed in
 * @param {Object} argsInitializer.stories - the data from the archive database
 * //@param {Object} argsInitializer.stories.objIndex - some abstracted data needed for link creation
 *
 * @returns the object of objects ;)
 */
export function initializer({ element, stories }) {
	//this will determine each storybits orientation - if the obj is displayed left or right
	let orientationClass = 'obj-left';
	//this will decide the placement of the story as single view, left or right (or maybe in the future also middle pane)
	let displayNr = 1;

	return {
		createTitleElement(story, overlayOuter) {
			// Construct title element
			const elTitleContainer = document.createElement('div');
			elTitleContainer.classList.add('title-container');
			elTitleContainer.setAttribute('id', `${story.idno}_title`);
			const elTitleTextContainer = document.createElement('div');
			elTitleTextContainer.classList.add('title-text-container');
			const elStoryTitle = document.createElement('h2');
			elStoryTitle.textContent = story.label;
			const elStorySubTitle = document.createElement('h5');
			const elStoryResponsable = document.createElement('p');
			const elStoryGenre = document.createElement('p');
			if (story.fabMaterial) {
				const elFabObjRepContainer = document.createElement('div');
				elFabObjRepContainer.classList.add(
					'fab_material_obj-rep_container'
				);

				const elFabObjRep = document.createElement('div');
				elFabObjRep.classList.add('fab_material_obj-rep');
				elFabObjRep.style.borderColor = story.stryColor;

				elFabObjRep.id = `obj-${story.fabMaterial.id}`;
				const elFabObjLink = document.createElement('a');
				const fabObjLinkSrc = `/index.php/Detail/objects/${story.fabMaterial.id}`;
				elFabObjLink.setAttribute(
					'href',
					//because ffor use as a html element id we could not use only numbers, [objID] but obj-[objID] we have to use slice to remove the first 4 letters
					//	`/index.php/Detail/objects/${elStryItemObj.id.slice(4)}`
					fabObjLinkSrc
				);
				elFabObjLink.setAttribute('target', 'overlay');
				elFabObjLink.addEventListener('click', function () {
					refineOverlay(story.stryColor, overlayOuter, fabObjLinkSrc);
				});

				elFabObjRep.innerHTML = story.fabMaterial.media;

				console.log('FABULATIONMATERIALID ' + story.fabMaterial.id);

				elFabObjLink.append(elFabObjRep);
				elFabObjRepContainer.append(elFabObjLink);
				elTitleContainer.append(elFabObjRepContainer);
			}
			if (story.description) {
				elStorySubTitle.innerHTML = story.description;
			}
			if (story.responsable.contributor) {
				elStoryResponsable.textContent = `Responseable: ${story.responsable.contributor}`;
			}
			if (story.genres) {
				let genreList = null;

				for (const storygenre of story.genres) {
					if (genreList !== null) {
						genreList = genreList + `, ${storygenre}`;
					} else {
						genreList = storygenre;
					}
				}
				// elStoryGenre.textContent = `Genre: ${story.genres}`;
				elStoryGenre.textContent = `Genre: ${genreList}`;
			}

			elTitleTextContainer.append(elStoryTitle);
			elTitleTextContainer.append(elStorySubTitle);
			elTitleTextContainer.append(elStoryGenre);
			elTitleTextContainer.append(elStoryResponsable);
			elTitleContainer.append(elTitleTextContainer);
			return elTitleContainer;
		},
		createLinkPreviewElement(stryIdno) {
			const story = stories.find((item) => item.idno === stryIdno);

			console.log(story);

			const elLnPrev = document.createElement('div');
			elLnPrev.classList.add('link-preview');
			elLnPrev.setAttribute('id', `${story.idno}_link-preview`);
			elLnPrev.style.borderColor = story.stryColor;
			const elLnPrevInner = document.createElement('div');
			elLnPrevInner.classList.add('link-preview-inner');
			const elLnPrevTextContainer = document.createElement('div');
			elLnPrevTextContainer.classList.add('linkPreview-text-container');
			const elStoryLnPrev = document.createElement('h4');
			elStoryLnPrev.textContent = story.label;
			const elStorySubtitle = document.createElement('h8');
			const elStoryResponsable = document.createElement('p');
			const elStoryGenre = document.createElement('p');
			if (story.fabMaterial) {
				console.log(story.fabMaterial);
				const elFabObjRepContainer = document.createElement('div');
				elFabObjRepContainer.classList.add(
					'story__item__obj-rep-container'
				);
				const elFabObjRep = document.createElement('div');
				elFabObjRep.classList.add('ln-fab-material-div');
				elFabObjRep.style.borderColor = story.stryColor;
				elFabObjRep.innerHTML = story.fabMaterial.media;

				// //the link has to be specified
				// elFabObjRep.firstChild.setAttribute(
				// 	'href',
				// 	//because from php we get not [objID] but obj-[objID] we have to use slice to remove the first 4 letters
				// 	`/index.php/Detail/objects/${story.fabMaterial.id}`
				// );
				// elFabObjRep.firstChild.setAttribute('target', 'overlay');
				// elFabObjRep.addEventListener('click', function () {
				// 	overlayOuter.classList.remove('hidden');
				// });

				elLnPrevInner.append(elFabObjRep);
			}
			if (story.description) {
				elStorySubtitle.textContent = story.description;
			}
			if (story.responsable.contributor) {
				elStoryResponsable.textContent = `Responseable: ${story.responsable.contributor}`;
			}
			if (story.genres) {
				elStoryGenre.textContent = `Genre: ${story.genres}`;
			}
			elLnPrevTextContainer.append(elStoryLnPrev);
			elLnPrevTextContainer.append(elStorySubtitle);
			elLnPrevTextContainer.append(elStoryGenre);
			elLnPrevTextContainer.append(elStoryResponsable);
			elLnPrevInner.append(elLnPrevTextContainer);
			elLnPrev.append(elLnPrevInner);
			let t = story.stryColor.slice(1).convertToRGB();
			const previewBgColor = String(
				'rgba(' + t[0] + ', ' + t[1] + ', ' + t[2] + ', 0.175)'
			);
			elLnPrevInner.style['background-color'] = previewBgColor;
			//elLnPrev.style['display'] = 'none';

			return elLnPrev;
		},
		//erstellt DOM elemente die dazu dienen die detail ansicht für ein objekt als overlay zu betrachten
		createDetailOverlayElement(element) {
			const elOverlayOuter = document.createElement('div');
			elOverlayOuter.classList.add('overlay-outer');
			elOverlayOuter.classList.add('hidden');
			elOverlayOuter.setAttribute('id', 'exposeMask');
			const elOverlayInner = document.createElement('div');
			elOverlayInner.classList.add('overlay-inner');
			elOverlayInner.classList.add('hidden');

			const elOverlay = document.createElement('iframe');
			elOverlay.setAttribute('id', 'overlay');
			elOverlay.setAttribute('name', 'overlay');
			//create overlay buttons
			const elOverlayButtons = document.createElement('div');
			elOverlayButtons.setAttribute('id', 'overlay-buttons');
			elOverlayButtons.classList.add('hidden');

			//create close button
			const elCloseOverlay = document.createElement('div');
			elCloseOverlay.setAttribute('id', 'close-overlay');
			// const elOverlayOuter = document.getElementById('exposeMask');
			// const elOverlay = document.getElementById('overlay');
			// const elCloseOverlay = document.getElementById('close-overlay');
			elCloseOverlay.addEventListener('click', function () {
				elOverlayOuter.classList.add('hidden');
				elOverlay.setAttribute('src', 'about:blank');
			});
			const elCloseOverlImg = document.createElement('img');
			elCloseOverlImg.setAttribute(
				'src',
				'/themes/StoryMode/assets/other_graphics/close.png'
			);
			//create toTab button
			const elToTabOverlay = document.createElement('div');
			elToTabOverlay.setAttribute('id', 'toTab-overlay');
			const elDetailLink = document.createElement('a');
			elDetailLink.setAttribute('target', '_blank');
			//this is some code that can be adapted to open the new tab in the background
			// elDetailLink.setAttribute('onclick', 'return popUnder(this)');
			// function popUnder(node) {
			// 	var newWindow = window.open(
			// 		'about:blank',
			// 		node.target,
			// 		'width=500,height=500'
			// 	);
			// 	newWindow.blur();
			// 	window.focus();
			// 	newWindow.location.href = node.href;
			// 	return false;
			// }
			elDetailLink.setAttribute('id', 'detail-link');
			//uncomment this oif you want the overlay to close when it is opened in a new tab
			// elToTabOverlay.addEventListener('click', function () {
			// 	elOverlayOuter.classList.add('hidden');
			// 	elOverlay.setAttribute('src', 'about:blank');
			// });
			const elToTabOverlImg = document.createElement('img');
			elToTabOverlImg.setAttribute(
				'src',
				'/themes/StoryMode/assets/other_graphics/toTab.png'
			);
			// create a loading message
			const elLoading = document.createElement('div');
			elLoading.setAttribute('id', 'loading');
			//elLoading.classList.add('hidden');
			const elLoadingImg = document.createElement('img');
			elLoadingImg.setAttribute(
				'src',
				'/themes/StoryMode/assets/other_graphics/loading.gif'
			);
			elLoadingImg.setAttribute('id', 'loading-image');
			const elLoadingMsg = document.createElement('span');
			elLoadingMsg.innerHTML =
				'Please wait while the requested materials are obtained from the archive.';
			elLoading.append(elLoadingImg);
			elLoading.append(elLoadingMsg);

			elCloseOverlay.append(elCloseOverlImg);
			elToTabOverlay.append(elDetailLink);
			elDetailLink.append(elToTabOverlImg);
			elOverlayButtons.append(elCloseOverlay, elToTabOverlay);
			elOverlayInner.append(elOverlay);
			elOverlayOuter.append(elLoading);
			elOverlayOuter.append(elOverlayInner, elOverlayButtons);
			const elBody = document.getElementsByTagName('body');

			elBody[0].append(elOverlayOuter);
			return elOverlayOuter;
		},
		createNarrationBitElements(narrationBits, elStry, overlayOuter) {
			console.log(elStry);
			for (const narrationBit of narrationBits) {
				const elStryItem = document.createElement('div');
				elStryItem.classList.add('story__item');
				elStryItem.classList.add(orientationClass);
				elStryItem.id = `item-${narrationBit.id}`;
				//elStryItem.setAttribute('id', narrationBit.id);

				const flipCardInner = document.createElement('div');
				flipCardInner.classList.add('flip-card-inner');

				const flipCard = document.createElement('div');
				flipCard.classList.add('story__item__flip-card');

				const flopside = document.createElement('div');
				flopside.classList.add('flopside');

				const flipside = document.createElement('div');
				flipside.classList.add('flipside');

				const flipsideTitle = document.createElement('h3');
				flipsideTitle.classList.add('flipside-title');
				flipsideTitle.innerHTML =
					'<h3>Details, facts, etc. A possibility to inspect the object</h3>';

				const elStryItemObj = document.createElement('div');
				elStryItemObj.classList.add('story__item__obj');

				const elStryItemObjRepContainer = document.createElement('div');
				elStryItemObjRepContainer.classList.add(
					'story__item__obj-rep-container'
				);
				const elStryItemObjRep = document.createElement('div');
				elStryItemObjRep.classList.add('story__item__obj-rep');
				//ab hier auslagern
				refineObjRep(
					elStryItemObj,
					elStryItemObjRep,
					narrationBit.objects,
					elStry
				);
				const elStryItemObjLink = document.createElement('a');
				if (narrationBit.objects[0]) {
					elStryItemObj.id = `obj-${narrationBit.objects[0].id}`;

					const objLinkSrc = `/index.php/Detail/objects/${narrationBit.objects[0].id}`;
					elStryItemObjLink.setAttribute(
						'href',
						//because ffor use as a html element id we could not use only numbers, [objID] but obj-[objID] we have to use slice to remove the first 4 letters
						//	`/index.php/Detail/objects/${elStryItemObj.id.slice(4)}`
						objLinkSrc
					);
					elStryItemObjLink.classList.add('detail-link');
					elStryItemObjLink.setAttribute('target', 'overlay');
					elStryItemObjLink.addEventListener('click', function () {
						refineOverlay(
							elStry.getAttribute('data-linecolor'),
							overlayOuter,
							objLinkSrc
						);
					});
				}
				// 	// elStryItemObjRepImg.style.width = '400px';
				// 	// elStryItemObjRepImg.style.height = '400px';

				// 	// itemImg.setAttribute('height', 'auto');

				// 	//elStryItemObjRep.append(narrationBit.objects[0].mediaLarge);
				// 	// const elStryItemObjRepMed = document.createElement('img');
				// 	// elStryItemObjRepMed.append(narrationBit.objects[0].mediaLarge);
				// 	// elStryItemObjRep.append(elStryItemObjRepMed);
				// } else {
				// 	const p = document.createElement('p');
				// 	p.textContent =
				// 		'This bit of narration is not linked to an archived material. This is probably an Error ............................................................................................................ In case you are an editor of this story, please consider thefollowing: If you want to accentuate a significant Gap in the Archive, use an Object of Type Gap. If you absolutely must publissh a story while you do not have the linked materials ready yet, please use the material of the type placholder.';
				// 	elStryItemObjRep.append(p);

				// 	elStryItemObjRep.classList.add('missing-object');
				// }
				const elStryItemObjCapt = document.createElement('div');
				elStryItemObjCapt.classList.add('story__item__caption');
				if (narrationBit.caption !== null) {
					elStryItemObjCapt.innerHTML = `<p>${narrationBit.caption}</p>`;
				}
				const elStryItemTxt = document.createElement('div');
				elStryItemTxt.classList.add('story__item__texte');

				const elStryItemTxtP1 = document.createElement('div');
				elStryItemTxtP1.classList.add('story__item__texte__text1');
				if (narrationBit.p1) {
					elStryItemTxtP1.innerHTML = `<p>${narrationBit.p1}</p>`;
				}

				const elStryItemTxtP2 = document.createElement('div');
				elStryItemTxtP2.classList.add('story__item__texte__text2');
				if (narrationBit.p2) {
					elStryItemTxtP2.innerHTML = `<p>${narrationBit.p2}</p>`;
				}

				const elStryItemTxtP3 = document.createElement('div');
				elStryItemTxtP3.classList.add('story__item__texte__text3');
				if (narrationBit.p3) {
					elStryItemTxtP3.innerHTML = `<p>${narrationBit.p3}</p>`;
				}

				elStryItemTxt.append(elStryItemTxtP1);
				elStryItemTxt.append(elStryItemTxtP2);
				elStryItemTxt.append(elStryItemTxtP3);

				elStryItemObj.append(elStryItemObjRepContainer);
				elStryItemObjRepContainer.append(elStryItemObjLink);

				elStryItemObjLink.append(elStryItemObjRep);
				elStryItemObj.append(elStryItemObjCapt);
				flipside.append(flipsideTitle);
				//	flopside.append(elStryItemObj);
				flopside.append(elStryItemTxt);
				flipCardInner.append(flopside);
				flipCardInner.append(flipside);
				flipCard.append(flipCardInner);
				elStryItem.append(elStryItemObj);
				elStryItem.append(flipCard);
				elStry.append(elStryItem);
				orientationClass = this.setItemOrientation('change');
			}
			return elStry;
		},
		setItemOrientation(key) {
			switch (key) {
				case 'r':
					orientationClass = 'obj-right';
					break;
				case 'l':
					orientationClass = 'obj-left';
					break;

				case 'change':
					orientationClass =
						orientationClass === 'obj-left'
							? 'obj-right'
							: 'obj-left';
					break;
				default:
					orientationClass =
						orientationClass === 'obj-left'
							? 'obj-right'
							: 'obj-left';
					break;
			}
			return orientationClass;
		},
		setItemImgSize() {
			const itemImg = this.elStryItemObjRep.getElementsByTagName('img');
			for (const oneImg of itemImg) {
				if (oneImg) {
					oneImg.setAttribute('width', '100%');
					oneImg.setAttribute('height', 'auto');
					//	console.log(oneImg);
				}
			}
		},
		initStory(story) {
			//create arguments for storydrawing
			const argsStory = {
				storyIdno: story.idno,
				createLinkPreviewElement:
					this.createLinkPreviewElement.bind(this), // Bind the method to the current object
			};
			const createThatStory = new createStory(argsStory);
			//create new story-object
			createThatStory.drawLinks(story.idno);
			const extStory = { ...createThatStory, ...story };
			return extStory;
		},
		displaySingle({ storyIdno, storyFocus }) {
			//this is basically a pass through for index
			menuNav.showSingleStory({ storyIdno, storyFocus });
		},
		displayRandomSingle() {
			//display a random story at the beginning
			const randomStoryNr = Math.trunc(Math.random() * stories.length);
			const randomStoryIdno = stories[randomStoryNr].idno;
			menuNav.showSingleStory({ storyIdno: randomStoryIdno });
			//notify
			notifyUser({
				notifyPage: 'StoryMode',
				notifyMsg:
					'Welcome to fabulation mode. You are seeing a randomly picked fabulation. To navigate fabulation mode, use the menu on the left, or the material::connections indicated by little circles next to the archived materials!',
				notifyKey: 'StoryModeFirst',
			});
		},
		makeTimebasedAutoplay() {
			// Function to check if an element is in the middle third of the screen
			function isInMiddleThird(element) {
				const rect = element.getBoundingClientRect();
				const windowHeight = window.innerHeight;
				const elementTop = rect.top;
				const elementBottom = rect.bottom;

				// Calculate the top and bottom boundaries of the middle third
				const middleThirdTop = windowHeight / 3;
				const middleThirdBottom = (windowHeight * 2) / 3;

				// Check if the element is in the middle third
				return (
					elementTop <= middleThirdBottom &&
					elementBottom >= middleThirdTop
				);
			}

			// Function to stop the video autoplay when it's not in the middle third of the screen
			function stopVideoAutoplay(videoElement) {
				if (videoElement) {
					//const sourceTemp = videoElement.innerHTML;
					//videoElement.innerHTML = '';
					//videoElement.removeAttribute('autoplay');
					videoElement.pause();
					//videoElement.innerHTML = sourceTemp;
				}
			}

			// Add event listener to track scroll position
			window.addEventListener('scroll', function () {
				// Get all video elements
				const videoElements =
					document.querySelectorAll('.video-material');

				// Loop through each video element
				videoElements.forEach(function (videoElement) {
					// Check if the video element is in the middle third of the screen
					if (videoElement && isInMiddleThird(videoElement)) {
						// Set autoplay attribute to start playing the video automatically
						//videoElement.setAttribute('autoplay', '');
						//videoElement.setAttribute('muted', '');
						videoElement.play();

						console.log('autoplay started');
					} else {
						// If the video is not in the middle third, stop autoplay
						stopVideoAutoplay(videoElement);
						console.log('autoplay stopped');
					}
				});
			});
		},
		initializeAll() {
			const overlayOuter = this.createDetailOverlayElement(element);

			this.stories = {};
			//this.previewData = {};
			for (let story of stories) {
				orientationClass = 'obj-left';

				const elStryContainerPRE = document.createElement('div');
				elStryContainerPRE.classList.add('story-container');

				const elLineCanvas1 = document.createElement('canvas');
				elLineCanvas1.classList.add('line-canvas1');
				elLineCanvas1.classList.add('line-canvas');
				// const elLinkCanvas1 = document.createElement('canvas');
				// elLinkCanvas1.classList.add('link-canvas1');

				//console.log(narrationBits.length);
				elStryContainerPRE.setAttribute(
					'id',
					`cont-story-${story.idno}`
				);
				const elTitleContainer = this.createTitleElement(
					story,
					overlayOuter
				);
				//const elLnPrev = this.createLinkPreviewElement(story);
				//this.previewData[story.idno] = elLnPrev;
				const elStry = document.createElement('div');
				elStry.classList.add('story');
				elStry.id = `story-${story.idno}`;
				//		console.log(elStry);
				elStry.setAttribute('data-linecolor', story.stryColor);

				const narrationBits = story.narrationBits;

				this.createNarrationBitElements(
					narrationBits,
					elStry,
					overlayOuter
				);

				elStry.append(elLineCanvas1);
				elStryContainerPRE.append(elTitleContainer);
				elStryContainerPRE.append(elStry);
				//console.log(elStry);
				elStryContainerPRE.classList.add('hidden');
				elStryContainerPRE.setAttribute('data-display-nr', displayNr);
				element.append(elStryContainerPRE);
				//element.append(elLnPrev);

				const extStory = this.initStory(story);
				extStory.displayNr = displayNr;

				menuNav.createMenuEntry({
					storyTitle: story.label,
					storyIdno: story.idno,
				});

				displayNr++;
				this.stories[story.idno] = extStory;
				this.stories[story.idno].elStry = elStryContainerPRE;
				truncateTextbased();
				this.makeTimebasedAutoplay();
				//				console.log(this.stories);
			}
			//console.log(this.previewData);
			//console.log('hello previewdata');

			//should only be called if no url-parameters are set
			//this.displayRandomSingle();

			//return
		},
	};
}
