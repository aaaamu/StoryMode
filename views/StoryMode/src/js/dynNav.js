import { container } from './lib/domTools';
import { createStory } from './createStory';
import * as $ from 'jquery';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/effect';
import { animate } from './lib/domTools';
import Navbar from './lib/domTools';
import { cookieTool } from './lib/dataTools';
import { chooseStory } from './lib/dataTools';
/* global document, story, element, console, objIndex, window */

/**
 * provides three essential functions for navigation. 1 is the dynamic app like dynNav,
 * one is the static menuNav, that can be used for single story display and can be called with lesss informations.
 * the last one serves to undo modifications by dyn nav and has to beexecuted at the beginning of every navigation call
 * @param {Object} args - The story creation arguments.
 * @param {string} args.storyIdno - The story's idno.
 * @param {HTMLElement} args.elStory -
 *
 * @returns The story representation object.
 */
// base means the story or item presenting the link. ref means the referenced item and story
const baseUrl = '/index.php/StoryMode/Index';

export function dynNav({
	storyIdnoBase,
	storyIdnoRef,
	baseItemId,
	refItemId,
	actionOrigin,
}) {
	const elStoryRef = document.querySelector(
		'.story-container#cont-story-' + storyIdnoRef
	);
	const elStoryBase = document.querySelector(
		'.story-container#cont-story-' + storyIdnoBase
	);
	const elBaseItem = document.querySelector(
		'div#' + baseItemId + '.story__item'
	);
	const elBaseItemObj = elBaseItem.firstChild;
	//	console.log(refItemId);
	const elRefItem = document.querySelector(
		'div#' + refItemId + '.story__item'
	);
	const elRefItemObj = elRefItem.firstChild;

	return {
		setBase() {
			let lastLinkActionData = {
				lastElStoryBase: window.lastElStoryBase,
				lastElStoryRef: window.lastElStoryRef,
				lastElBaseItem: window.lastElBaseItem,
				lastElRefItem: window.lastElRefItem,
			};

			// this will reset ajustments in story positioning made bz yAjustStories()
			undoLastModifications(lastLinkActionData);
			//TODO is it necessary to unset display classes for momentary loaded stories, or is this done anyway on every reload?
			//		console.log(actionOrigin);
			//first level: test where the navigation action comes from, script or url on load
			if (actionOrigin === 'breadCrumb') {
				// console.log('breadCrumb action triggered  in dynnav');
				const elStoryOldSingle = document.querySelector('.single');
				const elStoryOldLeft = document.querySelector(
					'.' + container[0]
				);
				const elStoryOldRight = document.querySelector(
					'.' + container[1]
				);
				if (elStoryOldSingle) {
					elStoryOldSingle.className = 'story-container';
					elStoryOldSingle.classList.add('hidden');
				}
				if (elStoryOldLeft) {
					elStoryOldLeft.className = 'story-container';
					elStoryOldLeft.classList.add('hidden');
				}
				if (elStoryOldRight) {
					elStoryOldRight.className = 'story-container';
					elStoryOldRight.classList.add('hidden');
				}
				elStoryBase.classList.add(container[0]);
				//	elStoryBase.classList.remove('single');
				elStoryBase.classList.remove('hidden');
				elStoryRef.classList.add(container[1]);
				//	elStoryRef.classList.remove('single');
				elStoryRef.classList.remove('hidden');
				// console.log(elStoryBase, elStoryRef, container[0]);
			} else {
				if (actionOrigin === 'fullUrl') {
					//that means a reload has just happened
					// console.log('double link found in dynnav');
					// clean slate on display classes:
					const temp =
						document.getElementsByClassName('story-container');
					// console.log(temp);
					for (const oneContainer of temp) {
						oneContainer.classList.add('hidden');
						oneContainer.classList.remove('story-container1');
						oneContainer.classList.remove('story-container2');
						oneContainer.classList.remove('single');
					}

					elStoryBase.classList.add(container[0]);
					//	elStoryBase.classList.remove('single');
					elStoryBase.classList.remove('hidden');
					elStoryRef.classList.add(container[1]);
					//	elStoryRef.classList.remove('single');
					elStoryRef.classList.remove('hidden');
					// console.log(elStoryBase, elStoryRef, container[0]);
					createStory({
						storyIdno: storyIdnoBase,
					}).drawLine();
					createStory({
						storyIdno: storyIdnoRef,
					}).drawLine();
					// console.log('this still happens');
				} else {
					if (actionOrigin === 'script') {
						//SINGLE
						//if base story is displayed in a single panel currently
						if (elStoryBase.classList.contains('single')) {
							elStoryBase.classList.remove('single');
							elStoryBase.classList.add(container[0]);
						} else {
							//BASE LEFT
							//if base story is displayed in left container currently
							if (elStoryBase.classList.contains(container[0])) {
								//TODO if the same button is pressed again, this is interpreted as toggeling the display of the second story. nbut not if it is another element that connects the two:
								if (
									elStoryRef.classList.contains(container[1])
								) {
									elStoryRef.className = 'story-container';
									elStoryRef.classList.add('hidden');

									elStoryBase.classList.add('single');
									elStoryBase.classList.remove(container[0]);

									createStory({
										storyIdno: storyIdnoBase,
									}).drawLine();
								}
								const elStoryOld = document.querySelector(
									'.' + container[1]
								);

								//	elStoryBase.classList.add(container[0]);
								elStoryOld.classList.add('hidden');
								elStoryOld.classList.remove(container[1]);
							}
							//BASE RIGHT
							//if base story is displayed in the right panel currently
							if (elStoryBase.classList.contains(container[1])) {
								const elStoryOld = document.querySelector(
									'.' + container[0]
								);

								// animate({
								// 	timing: function linear(timeFraction) {
								// 		return timeFraction;
								// 	},
								// 	draw: function (progress) {
								// 		elStoryBase.style.left =
								// 			(1 - progress) *
								// 			elStoryBase.getBoundingClientRect.x;
								// 	},
								// 	duration: 500,
								// 	onFinish: function () {
								// 		createStory({
								// 			storyIdno: storyIdnoRef,
								// 		}).drawLine();
								// 	},
								// });
								//SWAPPING
								//if i am hitting a link in the right panel referring the story in the left panel
								if (elStoryRef === elStoryOld) {
									elStoryOld.classList.add('hidden');
									elStoryOld.classList.remove(container[0]);
									// elStoryBase.classList.remove(container[1]);
									// elStoryBase.classList.add(container[0]);
									$('.story-container2').switchClass(
										container[1],
										container[0],
										1000,
										'easeInOutQuad'
									);
								} else {
									elStoryOld.classList.add('hidden');
									elStoryOld.classList.remove(container[0]);
									// elStoryBase.classList.remove(container[1]);
									// elStoryBase.classList.add(container[0]);
									$('.story-container2').switchClass(
										container[1],
										container[0],
										1000,
										'easeInOutQuad'
									);
								}
							}
						}
					}
				}
			}

			//now after the base story is checked and ajusted accordingly, set the classes of the referenced story
			//TODO check if this is fully redundant
			elStoryRef.classList.add(container[1]);
			elStoryRef.classList.remove(container[0], 'hidden');

			//only in the end ajust the position of the stories

			this.removeRefItemLinks();
			this.yAjustStories();
			this.ajustItemObjOrientation();
			this.moveObj();
			this.setNewUrl();
			this.actualizeBreadCrumbs();

			window.lastElStoryBase = elStoryBase;
			window.lastElStoryRef = elStoryRef;
			window.lastElBaseItem = elBaseItem;
			window.lastElRefItem = elRefItem;

			//BUG why the z-index levels do not work?
			//BUG in one case the ycorrecting is not working - why?

			//TODO handle captions in shared object

			// elRefItem.style.zIndex = '0';
			// elRefItemObj.style.zIndex = '0';
			// elBaseItem.style.zIndex = '900';
			// elBaseItemObj.style.zIndex = '900';
			// const superLinks = elBaseItemObj.querySelectorAll('.dot-svg');
			// for (const sLink of superLinks) {
			// 	sLink.style.zIndex = 1100;
			// }
		},
		argsToUri() {
			const argsObj = {
				sb: storyIdnoBase,
				sr: storyIdnoRef,
				ib: baseItemId,
				ir: refItemId,
			};
			const argsUri = encodeURI(
				'?sb=' +
					storyIdnoBase +
					'&sr=' +
					storyIdnoRef +
					'&ib=' +
					baseItemId +
					'&ir=' +
					refItemId
			);
			//const argsUri = encodeURI(JSON.stringify(argsObj));
			return argsUri;
		},
		setNewUrl() {
			let stateObj = { id: '100' };
			window.history.replaceState(
				stateObj,
				'Page 3',
				baseUrl + this.argsToUri()
			);
		},
		actualizeBreadCrumbs() {
			//proably obsolete now:
			window.breadCrumbs.push(storyIdnoRef);
			//	// console.log(window.breadCrumbs);
			const elBreadCrumbs = document.querySelector('.crumbs__list-inner');
			//	// console.log(elBreadCrumbs);
			const elCrumb = document.createElement('li');
			elCrumb.classList.add('crumbs__item');
			const elLink = document.createElement('a');
			elLink.classList.add('crumbs__link');
			elLink.classList.add('newborn');

			const linkData = {
				storyIdnoRef,
				refItemId,
				storyIdnoBase,
				baseItemId,
				actionOrigin: 'breadCrumb',
			};
			const staticLinkData = { ...linkData };
			elLink.addEventListener('click', function () {
				const userAction = new dynNav(staticLinkData);
				userAction.setBase();
			});
			//the newborn class serves as a marker for the Navbar function that will listen vfor changes and add functionalities to newly created elements.
			// elLink.addEventListener('click', function () {
			// 	const userAction = new dynNav({
			// 		storyIdnoRef,
			// 		refItemId,
			// 		storyIdnoBase,
			// 		baseItemId,
			// 		actionOrigin: 'breadCrumb',
			// 	});
			// 	userAction.setBase();
			// });

			// Navbar.lateAddEventListener(elLink);
			elLink.textContent =
				chooseStory('idno', storyIdnoBase).label +
				' -> ' +
				chooseStory('idno', storyIdnoRef).label;
			elCrumb.append(elLink);

			elBreadCrumbs.append(elCrumb);
		},
		yAjustStories() {
			//aligne the stories
			function setTopPart() {
				elStoryRef.style.top = '0px';
				elStoryBase.style.top = '0px';
				//TODO get delta height of titlecontainers and include it in the movement (or not and solve it differently)
				const rectBaseItem = elBaseItem.getBoundingClientRect();
				const rectRefItem = elRefItem.getBoundingClientRect();
				const rectStoryBase = elStoryBase.getBoundingClientRect();
				const rectStoryRef = elStoryRef.getBoundingClientRect();
				const rectStoryRefTitle =
					elStoryRef.firstChild.getBoundingClientRect();
				const rectStoryBaseTitle =
					elStoryBase.firstChild.getBoundingClientRect();
				//  console.log(
				// 	'attention:' + rectBaseItem.y,
				// 	rectRefItem.y,
				// 	rectStoryBaseTitle.height,
				// 	rectStoryRefTitle.height
				// );
				if (rectBaseItem.y > rectRefItem.y) {
					elStoryRef.style.top =
						rectBaseItem.y - rectRefItem.y + 'px';
				} else {
					elStoryBase.style.top =
						rectRefItem.y - rectBaseItem.y + 'px';
				}
			}
			//SCROLLING PART
			function scrollPart() {
				const rectBaseItem = elBaseItem.getBoundingClientRect();
				//EITHER
				//	elBaseItem.scrollIntoView({ behavior: 'smooth' });
				//OR
				window.scrollTo({
					left: rectBaseItem.left + window.pageXOffset,
					top: rectBaseItem.top + window.pageYOffset - 50,
					behavior: 'smooth',
				});
			}
			function hideRefItemObj() {
				/*()
				elRefItemObj =
					elRefItem.getElementsByClassName('story__item__obj');
				*/
				elRefItem.getElementsByClassName = 'story__item__obj';
				// console.log('hello' + elRefItemObj);
			}

			setTopPart();
			scrollPart();
		},

		ajustItemObjOrientation() {
			if (elBaseItem.classList.contains('obj-left')) {
				elBaseItem.classList.remove('obj-left');
				elBaseItem.classList.add('obj-right', 'changed');
			}
			if (elRefItem.classList.contains('obj-right')) {
				elRefItem.classList.remove('obj-right');
				elRefItem.classList.add('obj-left', 'changed');
			}
		},
		moveObj() {
			const rectBaseItemObj = elBaseItemObj.getBoundingClientRect();
			const rectRefItemObj = elRefItemObj.getBoundingClientRect();
			const halfObjWidth = rectBaseItemObj.width / 2 + 23;

			elRefItemObj.style.zIndex = '0';
			animate({
				timing: function linear(timeFraction) {
					return timeFraction;
				},
				draw: function (progress) {
					elBaseItemObj.style.marginRight =
						progress * -halfObjWidth + 'px';
				},
				duration: 900,
				onFinish: function () {
					createStory({
						storyIdno: storyIdnoBase,
					}).drawLine();
				},
			});
			elBaseItemObj.style.zIndex = '400';
			animate({
				timing: function linear(timeFraction) {
					return timeFraction;
				},
				draw: function (progress) {
					elRefItemObj.style.marginLeft =
						progress * -halfObjWidth + 'px';
				},
				duration: 900,
				onFinish: function () {
					createStory({
						storyIdno: storyIdnoRef,
					}).drawLine();
				},
			});
		},

		removeRefItemLinks() {
			const surplusLinks = elRefItemObj.querySelectorAll('.dot-svg');
			for (const sLink of surplusLinks) {
				sLink.classList.add('hidden');
			}
			const superLinks = elBaseItemObj.querySelectorAll('.dot-svg');
			for (const sLink of superLinks) {
				sLink.style.zIndex = 1100;
			}
		},

		// Method to display a single story and hide all others
	};
}
export function undoLastModifications({
	lastElStoryBase,
	lastElStoryRef,
	lastElBaseItem,
	lastElRefItem,
}) {
	if (lastElStoryBase) {
		const lastElBaseItemObj = lastElBaseItem.firstChild;
		const lastElRefItemObj = lastElRefItem.firstChild;

		lastElStoryBase.style.top = '0px';
		lastElStoryRef.style.top = '0px';
		lastElRefItem.classList.remove('hidden');
		lastElBaseItemObj.style.marginRight = '0px';
		lastElBaseItemObj.style.zIndex = 300;

		lastElRefItemObj.style.marginLeft = '0px';
		lastElRefItemObj.style.zIndex = 300;
		//restore lnk visibility on previously referenced item
		const surplusLinks = lastElRefItemObj.querySelectorAll('.dot-svg');
		for (const sLink of surplusLinks) {
			sLink.classList.remove('hidden');
		}

		if (lastElBaseItem.classList.contains('changed')) {
			lastElBaseItem.classList.toggle('obj-right', 'obj-left');
			lastElBaseItem.classList.remove('changed');
			// console.log('now changed orientation of last base item');
		}
		if (lastElRefItem.classList.contains('changed')) {
			lastElRefItem.classList.toggle('obj-right', 'obj-left');
			lastElRefItem.classList.remove('changed');
			// console.log('now changed orientation of last ref item');
		}
	}
}
export const menuNav = {
	// Method to display a single story and hide all others

	showSingleStory({ storyTitle, storyIdno, storyFocus }) {
		let lastLinkActionData = {
			lastElStoryBase: window.lastElStoryBase,
			lastElStoryRef: window.lastElStoryRef,
			lastElBaseItem: window.lastElBaseItem,
			lastElRefItem: window.lastElRefItem,
		};

		// this will reset ajustments in story positioning made bz yAjustStories()

		undoLastModifications(lastLinkActionData);
		const elSinlgeStory = document.querySelector(
			`.story-container#cont-story-${storyIdno}`
		);
		if (!elSinlgeStory) {
			const notifiCation = document.getElementById('fab-notify');
			notifiCation.innerHTML =
				'<h3>Ooops, the content that was requested is not availlable. oje oje oje.</h3>';
			notifiCation.classList.remove('hidden');
		} else {
			// clean slate on display classes:
			const temp = document.getElementsByClassName('story-container');
			// console.log(temp);
			for (const oneContainer of temp) {
				oneContainer.classList.add('hidden');
				oneContainer.classList.remove('story-container1');
				oneContainer.classList.remove('story-container2');
				oneContainer.classList.remove('single');
			}
			elSinlgeStory.style.top = '0px';
			elSinlgeStory.classList.remove('hidden');
			elSinlgeStory.classList.add('single');
			//console.log(initObj);
			createStory({ storyIdno }).drawLine();
			//		this.writeStoryTitle(storyTitle);
			//storyIdno.storyObj.writeStoryTitle();
			// stories[storyIdno].drawDots();

			//check if a focussed item is set
			function scrollPart() {
				const elBaseItemObj = document.getElementById(
					'obj-' + storyFocus
				);
				const rectBaseItemObj = elBaseItemObj.getBoundingClientRect();
				//EITHER
				//	elBaseItem.scrollIntoView({ behavior: 'smooth' });
				//OR
				window.scrollTo({
					left: rectBaseItemObj.left + window.pageXOffset,
					top: rectBaseItemObj.top + window.pageYOffset,
					behavior: 'smooth',
				});
			}
			if (storyFocus) {
				//	const baseItemId = storyFocus
				//	const elBaseItemObj = elBaseItem.firstChild;

				scrollPart();
			}
			this.setNewUrl(storyIdno);
			this.actualizeBreadCrumbs(storyIdno);
		}
	},
	argsToUri(storyIdno) {
		const argsUri = encodeURI('?single=' + storyIdno);
		//const argsUri = encodeURI(JSON.stringify(argsObj));
		return argsUri;
	},
	setNewUrl(storyIdno) {
		let stateObj = { id: '100' };
		window.history.replaceState(
			stateObj,
			'Page 3',
			baseUrl + this.argsToUri(storyIdno)
		);
	},
	actualizeBreadCrumbs(storyIdno) {
		window.breadCrumbs.push(storyIdno);
		//	console.log(window.breadCrumbs);
		const elBreadCrumbs = document.querySelector('.crumbs__list-inner');
		//	console.log(elBreadCrumbs);
		const elCrumb = document.createElement('li');
		elCrumb.classList.add('crumbs__item');
		const elLink = document.createElement('a');
		elLink.classList.add('crumbs__menuentry');
		elLink.textContent =
			' --- ' + chooseStory('idno', storyIdno).label + ' --- ';

		//	const staticLinkData = storyIdno.value;
		elLink.addEventListener('click', function () {
			menuNav.showSingleStory({ storyIdno: storyIdno });
		});
		// elLink.addEventListener('click', function () {
		// 	const userAction = new dynNav({
		// 		storyIdnoRef,
		// 		refItemId,
		// 		storyIdnoBase,
		// 		baseItemId,
		// 		actionOrigin: 'breadCrumb',
		// 	});
		// 	userAction.setBase();
		// });

		elCrumb.append(elLink);
		elBreadCrumbs.append(elCrumb);
		//plus write a cookie
	},

	//Method to create a menu entry for each availlable story
	createMenuEntry({ storyTitle, storyIdno }) {
		const menuContainer = document.getElementById('menu-container');
		const menuEntry = document.createElement('button');
		let elStory = document.querySelector('#story-' + storyIdno);
		const stryColor = elStory.getAttribute('data-linecolor');
		let t = stryColor.slice(1).convertToRGB();
		const storyColorLight = String(
			'rgba(' + t[0] + ', ' + t[1] + ', ' + t[2] + ', 0.275)'
		);
		//	console.log(storyColorLight);
		menuEntry.setAttribute('style', 'background-color:' + storyColorLight);
		menuEntry.classList.add('single-story__menuentry');
		menuEntry.textContent = storyTitle;
		menuEntry.addEventListener('click', function () {
			menuNav.showSingleStory({ storyTitle, storyIdno });
		});
		// console.log(menuEntry);
		menuContainer.appendChild(menuEntry);
	},
};
