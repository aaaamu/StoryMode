
<div class="menu-left"
	<!--drawer-navbar from https://engineertodeveloper.com/create-a-responsive-drawer-navbar-using-javascript/ -->
	<!-- <nav class="filter-reload">
	<button class="filter-reload__btn">
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
        </svg>
		<div class="crumbs__title">
		Breadcrumbs
	</div>
    </button>
	<div class="filter-reload-drawer" ></div>
        <div class="filter-reload__list"><button class="filter-reload__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
            </svg>
        </button><h4>Breadcrumbs</h4>
	
    </div>
</nav> -->
<nav class="crumbs">
	<button class="crumbs__btn">
      
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="32"
   height="32"
   viewBox="0 0 42.849789 42.985092"
   version="1.1"
   id="svg8"
   inkscape:version="0.92.3 (2405546, 2018-03-11)"
   sodipodi:docname="drawing.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#ffffff"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="0.98994949"
     inkscape:cx="215.21277"
     inkscape:cy="-56.643337"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:snap-to-guides="false"
     inkscape:snap-grids="false"
     inkscape:snap-page="true"
     inkscape:snap-text-baseline="false"
     inkscape:snap-center="true"
     inkscape:snap-nodes="false"
     inkscape:window-width="1280"
     inkscape:window-height="963"
     inkscape:window-x="0"
     inkscape:window-y="32"
     inkscape:window-maximized="1" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0.06534988,-0.08729478)">
    <path
       style="fill:#ffffff;stroke-width:0.35277775"
       d="m 25.294867,34.881606 -1.173412,-0.07634 0.04533,-1.291449 c 0.05036,-1.434676 0.748759,-1.886012 2.457897,-1.588409 0.970812,0.169045 0.971566,0.170735 0.643181,1.439302 -0.399019,1.541423 -0.4924,1.613214 -1.972999,1.516892 z M 35.561969,23.654489 c -1.05522,-0.251175 -1.522213,-0.900398 -1.274791,-1.772246 0.15556,-0.548152 0.359777,-0.64694 1.124299,-0.543863 1.05787,0.142623 1.643108,0.802851 1.481595,1.67145 -0.146863,0.789815 -0.336153,0.881489 -1.331103,0.644659 z m 3.98795,-4.511265 c -0.97688,-0.09159 -1.06351,-0.180331 -1.304681,-1.336631 -0.369315,-1.770677 -0.03142,-2.760279 0.957111,-2.803112 0.42925,-0.0186 1.11009,-0.372513 1.51297,-0.786461 1.27847,-1.313588 2.35822,0.01243 1.99936,2.455328 -0.22572,1.536501 -1.60927,2.616702 -3.16476,2.470876 z m -23.94274,16.036862 c -0.874076,-0.681058 -0.891722,-1.996286 -0.03468,-2.584576 0.670847,-0.46048 0.79147,-0.404317 1.392849,0.648514 0.565898,0.99072 0.55818,1.239967 -0.05496,1.77416 -0.523516,0.456104 -0.87033,0.499199 -1.303216,0.161902 z M 1.2096131,43.072002 c -1.19902798,-0.01134 -1.42477898,-0.33617 -1.19487298,-1.719253 0.152738,-0.918825 0.345553,-1.142485 1.18136098,-1.370316 1.386709,-0.377999 2.308913,0.411224 1.938491,1.658962 -0.353593,1.191049 -0.69164,1.442277 -1.924981,1.43061 z M 24.258298,27.13461 c -1.069732,-0.254628 -1.218363,-0.500376 -1.095694,-1.811618 0.09003,-0.962343 0.196492,-1.075193 1.145391,-1.214104 0.575376,-0.08424 1.350087,-0.01674 1.721578,0.149989 0.549906,0.246802 0.612125,0.500544 0.334782,1.365278 -0.428878,1.337181 -1.028207,1.767016 -2.10606,1.510451 z m 4.237092,-5.047857 c -1.121592,-0.266973 -1.289544,-0.575299 -1.010145,-1.854407 0.201074,-0.920516 0.367124,-1.090703 1.168461,-1.197553 0.51374,-0.0685 1.240655,0.01403 1.615361,0.183418 0.553027,0.249991 0.624834,0.502783 0.381433,1.342767 -0.375628,1.296286 -1.065956,1.785026 -2.155113,1.52577 z M 7.8844941,31.955502 c -1.069731,-0.254628 -1.218364,-0.500377 -1.095693,-1.811618 0.09003,-0.962343 0.19649,-1.075193 1.145391,-1.214106 0.575374,-0.08423 1.350086,-0.01673 1.721577,0.149992 0.5499069,0.246801 0.6121269,0.500543 0.334781,1.365276 -0.428877,1.337183 -1.028205,1.767017 -2.106053,1.510459 z M 34.848545,12.991009 c -1.812421,-0.09774 -1.659432,-2.13212 0.178442,-2.372857 0.471271,-0.06173 1.343126,-0.237909 1.937461,-0.391507 l 1.080619,-0.2792693 -0.872884,1.4291773 c -1.054365,1.726315 -0.996394,1.686035 -2.323641,1.614452 z M 20.504989,21.1209 c -0.874076,-0.681059 -0.891723,-1.996286 -0.03468,-2.584577 0.670847,-0.460481 0.791468,-0.404316 1.392849,0.648513 0.565899,0.99072 0.55818,1.239968 -0.05496,1.774162 -0.523516,0.456104 -0.87033,0.499198 -1.303215,0.161902 z m -6.888843,2.762069 c -0.455157,-0.315366 -1.006001,-0.37919 -1.333347,-0.154494 -0.613157,0.420881 -1.421669,0.03182 -1.615538,-0.777397 -0.143727,-0.599925 0.925811,-2.368377 1.706817,-2.822177 0.928427,-0.539454 2.581871,-0.379897 3.312048,0.319618 1.477724,1.415671 -0.425676,4.573739 -2.06998,3.43445 z M 38.885779,5.4579727 c -0.817619,-0.155958 -0.957964,-0.314722 -1.172363,-1.326334 -0.325029,-1.533575 0.14018,-2.751241 1.091313,-2.856443 0.39916,-0.04415 0.97977,-0.36794298 1.29023,-0.71953998 0.68793,-0.779087 1.81345,-0.551703 2.10978,0.426225 0.11432,0.37726298 0.1257,1.24517298 0.0253,1.92867898 -0.22572,1.536558 -1.91153,2.820686 -3.34425,2.547413 z M 24.366876,13.831145 c -0.524474,0.0073 -1.125206,-0.236669 -1.334961,-0.542248 -0.517967,-0.754598 0.35165,-1.980714 1.321793,-1.863651 0.405093,0.04888 0.989368,-0.08173 1.298389,-0.290242 0.308923,-0.208654 0.837052,-0.389465 1.173404,-0.40212 0.491279,-0.01848 0.497705,0.144118 0.03267,0.826802 -0.318384,0.467396 -0.794613,1.166676 -1.058291,1.553958 -0.332887,0.488931 -0.770871,0.708228 -1.433002,0.717501 z"
       id="path3778"
       inkscape:connector-curvature="0" />
  </g>
</svg>
		<div class="crumbs__title">
		Breadcrumbs
	</div>
    </button>
	<div class="crumbs-drawer" id="crumbs">
        <div class="crumbs__list"><button class="crumbs__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
            </svg>
        </button><h4>Breadcrumbs</h4>
		<ul class="crumbs__list-inner"></ul></div>
    </div>
</nav>

<nav class="single-story-nav">
	<button class="single-story__btn">
	<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="36"
   height="36"
   viewBox="0 0 42.849789 42.985092"
   version="1.1"
   id="svg8"
   inkscape:version="0.92.3 (2405546, 2018-03-11)"
   sodipodi:docname="single story.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.979899"
     inkscape:cx="-12.760446"
     inkscape:cy="44.093478"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:snap-to-guides="false"
     inkscape:snap-grids="false"
     inkscape:snap-page="true"
     inkscape:snap-text-baseline="false"
     inkscape:snap-center="true"
     inkscape:snap-nodes="false"
     inkscape:window-width="1280"
     inkscape:window-height="963"
     inkscape:window-x="0"
     inkscape:window-y="32"
     inkscape:window-maximized="1" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0.06534988,-0.08729478)">
    <ellipse
       id="path3827"
       cx="6.8836575"
       cy="4.3851204"
       rx="2.1381562"
       ry="2.2049735"
       style="fill:#ffffff;stroke-width:1.06500006;stroke-miterlimit:4;stroke-dasharray:none" />
    <ellipse
       id="path3827-3"
       cx="30.002472"
       cy="34.653397"
       rx="2.1381562"
       ry="2.2049735"
       style="fill:#ffffff;stroke-width:1.06500006;stroke-miterlimit:4;stroke-dasharray:none" />
    <rect
       style="fill:#ffffff;stroke-width:1.06500006;stroke-miterlimit:4;stroke-dasharray:none"
       id="rect3848"
       width="4.2763119"
       height="4.2763133"
       x="5.8145795"
       y="23.294439" />
    <rect
       style="fill:#ffffff;stroke-width:1.06500006;stroke-miterlimit:4;stroke-dasharray:none"
       id="rect3850"
       width="4.2763114"
       height="4.0090427"
       x="27.730682"
       y="11.935484" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="M 6.6163883,4.9864771 C 18.297939,5.0556569 39.732808,12.020857 23.855272,16.479066 24.256176,17.013606 3.7233447,21.019581 6.3491186,25.8335 3.3162229,29.766455 29.378842,28.862555 30.002472,34.386124 30.585331,38.172821 13.921753,41.023318 7.8190993,44.141463"
       id="path3864"
       inkscape:connector-curvature="0"
       sodipodi:nodetypes="ccccc" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="M 6.2154839,9.7973287 19.445325,9.9309637 19.31169,9.6636937"
       id="path4671"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 6.3090327,14.837797 13.2298413,0.133635 -0.133635,-0.26727"
       id="path4671-7"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 6.3090328,12.191962 13.2298412,0.133635 -0.133635,-0.26727"
       id="path4671-23"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 20.37141,21.772067 13.229842,0.133635 -0.133635,-0.26727"
       id="path4671-75"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 20.464959,26.812536 13.229842,0.133635 -0.133635,-0.26727"
       id="path4671-7-9"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 20.464959,24.166701 13.229842,0.133635 -0.133635,-0.26727"
       id="path4671-23-2"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 2.0634476,33.131022 13.2298414,0.133635 -0.133635,-0.26727"
       id="path4671-2"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 2.1569964,38.171491 13.2298416,0.133635 -0.133635,-0.26727"
       id="path4671-7-8"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#ffffff;stroke-width:1.06500006;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 2.1569965,35.525656 13.2298415,0.133635 -0.133635,-0.26727"
       id="path4671-23-9"
       inkscape:connector-curvature="0" />
  </g>
</svg>
		<div class="crumbs__title">
		Single Stories
	</div>
    </button>

	<div class="single-story-drawer" >
        
		<div id="menu-container" class="single-story__list"><button class="single-story__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
            </svg>
        </button><h4>Single Story Navigation</h4>
    </div>
</nav>
<nav class="crumbs">
	<button class="crumbs__btn">
      

		<div class="crumbs__title" id="cache-age-title">
	</div>
    </button>
	
</nav>
	
</div>

