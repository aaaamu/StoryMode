<!-- PLEASE BE AWARE THAT THE JAVASCRIPT FOR LIQUIFY IS STILL PLACED IN THE FILES FROM WHERE THIS ONE IS CALLED! -->

<svg id="liquify" viewBox="0 0 100vw 10000px">
	<filter id="displacementFilter1">
    <feTurbulence
      type="turbulence"
      baseFrequency="0.006"
      numOctaves="1"
			stitchTiles="noStitch"
      result="turbulence" />
		
		<feColorMatrix in="turbulence" type="hueRotate" values="0" result="animate">
			<animate attributeName="values" from="0" to="360" dur="3s" repeatCount="indefinite"/>
		</feColorMatrix>
		
		<feColorMatrix
      in="animate"
      type="matrix"
      values="0 0 1 0 1
              0 1 0 0 1
              1 0 0 0 1
              .5 .5 .5 1 0"
			result="cloud"/>
		
		<feFlood flood-color="rgba( 17,128,141, 0.62)" result="color-flood"/>
		
		<feComposite in="color-flood" in2="cloud" operator="in" result="OUTLINE"></feComposite>
  </filter>

	<filter id="displacementFilter2">
    <feTurbulence
      type="turbulence"
      baseFrequency="0.004"
      numOctaves="1"
			seed="2"
			stitchTiles="noStitch"
      result="turbulence" />
		
		<feColorMatrix in="turbulence" type="hueRotate" values="0" result="animate">
			<animate attributeName="values" from="0" to="360" dur="5s" repeatCount="indefinite"/>
		</feColorMatrix>
		
		<feColorMatrix
      in="animate"
      type="matrix"
      values="0 0 0 0 1
              0 1 0 0 1
              0 0 0 0 1
              .5 .5 .5 1 0"
			result="cloud"/>
		
		<feFlood flood-color="rgba( 17,128,141, 0.22)" result="color-flood"/>
		
		<feComposite in="color-flood" in2="cloud" operator="in" result="OUTLINE"></feComposite>
  </filter>
	
	<rect x=0 y=0 width=100% height=100% fill=#ade8f4 />
	
	<rect x=0 y=0 width=100% height=100% style="filter: url(#displacementFilter1)" />
	
	<rect x=0 y=0 width=100% height=100% style="filter: url(#displacementFilter2)" />
	
</svg>
<style id="liquistyle">
 .bResultListItem, .navbar, .navbar-default, .yamm, .navLeftRight, .detailNavBgRight, .detailNavBgLeft{
		background-color: rgba(255,255,255,0.4)!important;
	}
	.bResultListItemExpandedInfo, .active a, .btn-search{
		background-color: rgba(255,255,255,0.2)!important;
	}

	
	#deliquify{
		
	background-color: rgba( 17,128,141, 0.8);
	border-style: solid;
	border-width: 1px;
	border-color: rgba( 17,128,141, 0.32);
	// border-top-color: lighten($cacolor, 20%);
	//border-bottom-color: rgba( 17,128,141, 0.62);
	padding: 0.6rem;
	position: relative;
	align-items: center;
	padding: 0.4rem;
	padding-top:12px;
	color: white;
	width: 65px;
	z-index: 10399;
	//margin-left: 0;
	left: 45%;
	height: 65px;
	box-shadow:  8px 8px rgba(255,255,255,0.2);
	font-size: 12px;

	}
	#liquify{
		z-index:0;
		width:100vw;
		height:100%;
		position:fixed;
		top:0;
		margin-left:-15px;
		//margin-top:-20px;

	}
	.on-demand{
		display:none;
		
	}
	#headrow{
			min-height:90px;
		}
</style>

<div class="on-demand">
		<div class='col-xs-1 col-sm-1 col-md-1 col-lg-1' id="deliquify-canvas">
					<div id="deliquify" onclick="deliquify()" onmouseover="this.style.cursor = 'pointer';">
					De-Liquify this page!
					</div>
		</div>
</div>








<script type='text/javascript'>


	
</script>
