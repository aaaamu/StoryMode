<?php
$script = file_get_contents(__CA_THEME_DIR__."/views/StoryMode/src/js/collections-list.js");
AssetLoadManager::addComplementaryScript($script);
$o_collections_config = $this->getVar("collections_config");
$qr_collections = $this->getVar("collection_results");
//require(__CA_THEME_DIR__."/views/StoryMode/src/php/liquify.php");

?>

	<div class="row" id="headrow">
		<div class='col-md-12 col-lg-10 col-lg-offset-1 collectionsListHeader'>
			<h1><?php print $this->getVar("section_name"); ?></h1><span></span>
			<p><?php print $o_collections_config->get("collections_intro_text"); ?></p>
		</div>
	</div>
	<div class="row">
        <?php
        $vn_i = 0;
$numHits = $qr_collections->numHits();
if ($qr_collections && $qr_collections->numHits()) {
    echo "<div class='col-md-12 col-lg-10 col-lg-offset-1 collectionsList'id='collectionsList' data-numhits='$numHits'>";
    $response_ables = [];

    while ($qr_collections->nextHit()) {
        // $labelDiv = "<div class='collectionTile divWithIndent'data-tile-index='$vn_i'>
        // <canvas class='tileCanvas'></canvas>
        // <div class='title'>" . $qr_collections->get("ca_collections.preferred_labels") . "</div></div>";


        // echo caDetailLink($this->request, $labelDiv, "", "ca_collections", $qr_collections->get("ca_collections.collection_id"));
        // if (($o_collections_config->get("description_template")) && ($vs_scope = $qr_collections->getWithTemplate($o_collections_config->get("description_template")))) {
        //     echo "<div>" . $vs_scope . "</div>";
        // }
        // preparing data for response_able filter
        $resp = $qr_collections->get("ca_collections.response.contributor", array('returnAsLink' => false));
        $response_ables[] =  $resp;



        $parentCollection = $qr_collections->get("ca_collections.parent_id");
        // check if approach should be listed even if it is a child approach:
        if ($parentCollection == null or $o_collections_config->get("show_child_approaches") == true) {
            $titleDiv = "<div class='title'>" . $qr_collections->get("ca_collections.preferred_labels")  . "</div><canvas class='tileCanvas'></canvas>";
            //echo"{{{<ifcount code='ca_collections.parent_id' max='0'>";
            echo "<div class='collectionTile divWithIndent' data-tile-index='$vn_i' data-resp='$resp'>
						" . caDetailLink($this->request, $titleDiv, "", "ca_collections", $qr_collections->get("ca_collections.collection_id")) ;
            if (($o_collections_config->get("description_template")) && ($vs_scope = $qr_collections->getWithTemplate($o_collections_config->get("description_template")))) {
                echo "<div>" . $vs_scope . "</div>";
            }
            echo "</div>";
            //echo "</ifcount>}}}";
            $vn_i++;
        }



    }


    $validHits = $vn_i;

    echo "</div> ";

} else {
    echo _t("<div class='col-md-12 col-lg-12 collectionsList'>'No collections available' </div>");
}

?>
</div><!-- end row -->
<style>
.collectionsList {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly; /* Adjust as needed */
	
}
.collectionsListHeader{
	display: flex;
    flex-wrap: wrap;
	justify-content: space-between; /* Adjust as needed */
	padding-left:26px;
	/* min-height:40px; */
	margin-bottom:15px;
}	
@media (max-width: 519px) {
	.collectionsListHeader{
		margin-top:15px;
		margin-bottom:20px;
		justify-content: center !important; /* Adjust as needed */
}}
.collectionsListHeader p{
	text-align:center;
}


.collectionTile {
	display: flex;
    /* justify-content: center; Horizontal zentrieren */
    align-items: center; /* Vertikal zentrieren */
    width: calc(33.33% - 10px); /* Adjust the width as needed */
	min-width: 240px !important;
	max-width: 380px;
	margin-left:5px;
    margin-bottom: 20px; /* Adjust the margin between rows */
    position: relative;
	height: 76.8px;
	transition: transform 3s ease-in-out; /* Apply transition to the transform property */
	overflow: visible;
}

.collectionTile canvas {
    width: 100%;
    height: 100%;
    display: block;
	position:absolute;
	left:0;
	top:0;
	z-index: index 100;
}

.collectionTile .title {
 	/* position: absolute; */
    /* top: 50%; */
    /* left: 50%; */
    /* transform: translate(-50%, -50%); */
    text-align: left;
    /* margin: 10px;  */
    font-size: 14px;
    /* white-space: nowrap; */
	padding-bottom:0px;
	z-index:1000;
	width: 100%;
	height: 100%;
}
.collectionTile a {
	text-decoration: none;
}
</style>
	<script type="text/javascript">
		
		document.addEventListener('DOMContentLoaded', function () {
	inSiteJS(<?php echo json_encode($validHits); ?>);
});

	</script>
