<?php
/* ----------------------------------------------------------------------
 * views/pageFormat/pageFooter.php :
 * ----------------------------------------------------------------------
 * CollectiveAccess
 * Open-source collections management software
 * ----------------------------------------------------------------------
 *
 * Software by Whirl-i-Gig (http://www.whirl-i-gig.com)
 * Copyright 2015-2018 Whirl-i-Gig
 *
 * For more information visit http://www.CollectiveAccess.org
 *
 * This program is free software; you may redistribute it and/or modify it under
 * the terms of the provided license as published by Whirl-i-Gig
 *
 * CollectiveAccess is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTIES whatsoever, including any implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This source code is free and modifiable under the terms of
 * GNU General Public License. (http://www.gnu.org/copyleft/gpl.html). See
 * the "license.txt" file for details, or visit the CollectiveAccess web site at
 * http://www.CollectiveAccess.org
 *
 * ----------------------------------------------------------------------
 */
require_once(__CA_THEME_DIR__."/conf/storymode.conf.php");

?>
		<div style="clear:both; height:1px;"><!-- empty --></div>
		</div><!-- end pageArea --></div><!-- end col --></div><!-- end row --></div><!-- end container -->
		<footer id="footer">
			<!-- <ul class="list-inline pull-right social">
				<li><i class="fa fa-twitter"></i></li>
				<li><i class="fa fa-facebook-square"></i></li>
				<li><i class="fa fa-youtube-play"></i></li>
			</ul> -->
			<div>
		</footer><!-- end footer -->
<?php
    //
    // Output HTML for debug bar
    //
    if(Debug::isEnabled()) {
        print Debug::$bar->getJavascriptRenderer()->render();
    }
?>
	
		<?php print TooltipManager::getLoadHTML(); ?>
		<div id="caMediaPanel"> 
			<div id="caMediaPanelContentArea">
			
			</div>
		</div>
		<script type="text/javascript">
			/*
				Set up the "caMediaPanel" panel that will be triggered by links in object detail
				Note that the actual <div>'s implementing the panel are located here in views/pageFormat/pageFooter.php
			*/
			var caMediaPanel;
			jQuery(document).ready(function() {
				if (caUI.initPanel) {
					caMediaPanel = caUI.initPanel({ 
						panelID: 'caMediaPanel',										/* DOM ID of the <div> enclosing the panel */
						panelContentID: 'caMediaPanelContentArea',		/* DOM ID of the content area <div> in the panel */
						exposeBackgroundColor: '#FFFFFF',						/* color (in hex notation) of background masking out page content; include the leading '#' in the color spec */
						exposeBackgroundOpacity: 0.7,							/* opacity of background color masking out page content; 1.0 is opaque */
						panelTransitionSpeed: 400, 									/* time it takes the panel to fade in/out in milliseconds */
						allowMobileSafariZooming: true,
						mobileSafariViewportTagID: '_msafari_viewport',
						closeButtonSelector: '.close'					/* anything with the CSS classname "close" will trigger the panel to close */
					});
				}
			});
			/*(function(e,d,b){var a=0;var f=null;var c={x:0,y:0};e("[data-toggle]").closest("li").on("mouseenter",function(g){if(f){f.removeClass("open")}d.clearTimeout(a);f=e(this);a=d.setTimeout(function(){f.addClass("open")},b)}).on("mousemove",function(g){if(Math.abs(c.x-g.ScreenX)>4||Math.abs(c.y-g.ScreenY)>4){c.x=g.ScreenX;c.y=g.ScreenY;return}if(f.hasClass("open")){return}d.clearTimeout(a);a=d.setTimeout(function(){f.addClass("open")},b)}).on("mouseleave",function(g){d.clearTimeout(a);f=e(this);a=d.setTimeout(function(){f.removeClass("open")},b)})})(jQuery,window,200);*/
		</script>

<?php
// redirect subdomains:
// Check if the subdomain is present in the request URL
$subdomain = explode('.', $_SERVER['HTTP_HOST'])[0];

if ($subdomain === 'glossar') {
    // Redirect to the specified URL for the glossar subdomain
    header("Location: https://aboutpower.net/index.php/Detail/collections/116");
    exit; // Make sure to exit after redirection
}


// simple visitor counter:
session_start();
$year_month = date(y.m);
$year_month_day = date(y.m.d);
$date_time = $date = date('m/d/Y H:i:s', time());
$counter_name_monthly = $visit_count_path.$year_month."-monthly.txt";
$counter_name_daily = $visit_count_path.$year_month_day."-daily.txt";
$ip_log_name = $visit_count_path.$year_month."-ip-log.csv";


// Check if a text file exists.
// If not create one and initialize it to zero.
if (!file_exists($counter_name_monthly)) {
    $f_monthly = fopen($counter_name_monthly, "w");
    fwrite($f_monthly, "1");
    fclose($f_monthly);
}
if (!file_exists($counter_name_daily)) {
    $f_daily = fopen($counter_name_daily, "w");
    fwrite($f_daily, "1");
    fclose($f_daily);
}
// Read the current value of our counter file
$f_monthly = fopen($counter_name_monthly, "r");
$counter_val_monthly = fread($f_monthly, filesize($counter_name_monthly));
fclose($f_monthly);
$f_daily = fopen($counter_name_daily, "r");
$counter_val_daily = fread($f_daily, filesize($counter_name_daily));
fclose($f_daily);

//adding IP logging
$visitor_ip = $_SERVER["REMOTE_ADDR"];
if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
    $visitor_ip .= '('.$_SERVER["HTTP_X_FORWARDED_FOR"].')';
}
if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
    $visitor_ip .= '('.$_SERVER["HTTP_CLIENT_IP"].')';
}
$visitor_hostname = gethostbyaddr($visitor_ip);
$ip_dat = @json_decode(file_get_contents( 
    "http://www.geoplugin.net/json.gp?ip=" . $ip)); 
	
	$country_name = $ip_dat->geoplugin_countryName; 
	$city_name = $ip_dat->geoplugin_city; 
	$continent_name = $ip_dat->geoplugin_continentName; 

// Has visitor been counted in this session?
// If not, increase counter value by one
if(!isset($_SESSION['hasVisited'])) {
    $_SESSION['hasVisited'] = "yes";
    $counter_val_monthly++;
    $f_monthly = fopen($counter_name_monthly, "w");
    fwrite($f_monthly, $counter_val_monthly);
    fclose($f_monthly);

    $counter_val_daily++;
    $f_daily = fopen($counter_name_daily, "w");
    fwrite($f_daily, $counter_val_daily);
    fclose($f_daily);

// iplogging:
	
	$ip_log = fopen($ip_log_name, 'a');
	fwrite($ip_log, $visitor_ip . ';' . $visitor_hostname . ';' . $date_time . ';' . $city_name . ';' . $country_name . ';' . $continent_name . PHP_EOL);
	fclose($ip_log);
	//echo "IP logged successfully.";
}

//echo "This Month, you are visitor number $counter_val_monthly to this site";

?>
	</body>
</html>
