<?php
	MetaTagManager::setWindowTitle($this->request->config->get("app_display_name").": StaticOther");
	require_once(__CA_LIB_DIR__."/BaseFindController.php");
	require_once(__CA_LIB_DIR__."/Datamodel.php");
	require_once(__CA_MODELS_DIR__."/ca_relationship_types.php");
 	require_once(__CA_APP_DIR__.'/helpers/browseHelpers.php');
 	require_once(__CA_APP_DIR__.'/helpers/accessHelpers.php');

?>

	<div class="row">
		<div class="col-sm-12">
			<H1><?php print _t("StaticOther"); ?></H1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<h2>yet another static page</h2>
			<h2>Trying out Model Access</h2>
<?php
// instantiate a model 
 $t_object = new ca_objects(10);   // load ca_object record with object_id = 10
 print "The title of the object is ".$t_object->get('ca_objects.preferred_labels.name')."<br/>\n";    // get the preferred name of the object
 print "more picture on it: ".$t_object->get('ca_object_representations.media.medium', array('returnAsLink' => true))."<br/>\n";    // 
 print "more picture on it: ".$t_object->get('ca_objects.type')."<br/>\n";    //
 $o_collections = $t_object->get('ca_collections', array('returnAsArray' => true));
 print ("more collections on it: ".$o_collections."<br/>\n");    // 
 var_dump($o_collections);
 print "<br/>\n get related collections attributes?: ".$t_object->get('ca_collections.type' )."<br/>\n";    //
 $t_o_collections = new ca_collections(5);   // load ca_object record with object_id = 10 
 print "The title of the collection is ".$t_object->get('ca_collections.type_id.name')."<br/>\n";    // get the preferred name of the object

?>
<h2>Trying out Collection Search</h2>
<?php
?>
<h2>Trying out Object Search</h2><?php




 // do a search and print out the titles of all found objects
 $o_search = new ObjectSearch();
 $qr_results = $o_search->search("*");    // ... or whatever text you like
 
 $count = 1;
 while($qr_results->nextHit()) {
     print "Hit ".$count.": ".$qr_results->get('ca_objects.preferred_labels.name').$qr_results->get('ca_object_representations.media.medium')."<br/>\n";
     $count++;
 } 

 
    // dump preferred labels in all languages
/*    while($qr_results->nextHit()) {
	print_r($qr_results->get('ca_objects.preferred_labels.name', array('returnAllLocales' => true, 'returnAsArray' => true)));
}

// dump preferred labels in just the current language
while($qr_results->nextHit()) {
	print_r($qr_results->get('ca_objects.preferred_labels.name', array('returnAllLocales' => false, 'returnAsArray' => true)));
} */


/*

 $o_search = new ObjectSearch();
 $qr_results = $o_search->search("Lager");    // ... or whatever text you like
 
 $count = 1;
 while($qr_results->nextHit()) {
     print "Hit ".$count.": ".$qr_results->get('ca_object_representations.media.medium', array('returnAsLink' => true))."<br/>\n";
     $count++;
 }
*/ 
 ?>
 


			
		</div>
		<div class="col-sm-3 col-sm-offset-1">
			<address>Archives<br>			100 Second Avenue, 2nd floor<br>			New York, NY 10010</address>
		
			<address>Jennifer Smith, Archivist<br>			<span class="info">Phone</span> — 212 222.2222<br>			<span class="info">Fax</span> — 212 222.2223<br>			<span class="info">Email</span> — <a href="#">email@archive.edu</a></address>
		</div>
	</div>
