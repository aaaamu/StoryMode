# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Types of changes:
Added for new features.
Changed for changes in existing functionality.
Deprecated for soon-to-be removed features.
Removed for now removed features.
Fixed for any bug fixes.
Security in case of vulnerabilities.

UNRELEASED ########################################################################################
v.1.0.2
added support for text-based materials in storymode
TODO - add config file for deciding what object types are handled what way

-   parse fabulation materials and link accordingly, or add genuine support for linking collections
-   add visualisations for timebased media
-   add a view for focusses
-   add focusses as a filter option

v.0.5.0
Changed the project root to grandparent directory, so now the whole theme is included. this makes sense in general for future handling and is necessary as from now on, changes in other views have to be made in order to integrate storymode in a sensible way.

v.0.4.0
added detail overlay
fixed many bugs (but not all) in dual pane navigation
added a variety of the storycolor as obj background
added shared obj view and other dynamic adjustments that happen during dualpane navigation
removed php footer in storymode (hidden)
removed menuNav and included it in dynNav for simplicity and access to tidy-up function

v. 0.3.0
fixed flip-Card functionality (mostly css)
added dual-pane functionality (css, html, js)
changed index.js to do so and
added initializer.js as a module that now assembles the DOM for each story and initially draws the canvas-elements, etc and sets visibility
added createLinks.js module, that returns "objIndex", containing all the data about linked stories
changed createStory.js to add a method that uses objIndex to draw linked svg circles on the obj representations that represent links to other stories
added dynNav.js that provides dynamic navigation, when the linking svgs ↑ are clicked

for outlook and toDos check gitlab issues with tag 0.3.0

v. 0.2.0
basic database query functional.
display in single story mode functional
flip card feature still not working

RELEASED VERSIONS #################################################################################
