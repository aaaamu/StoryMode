# Story mode

## Development

1. Install npm dependencies

```bash
npm install
```

2. Start the compilation:

| Command         |                                                 Description                                                 |
| :-------------- | :---------------------------------------------------------------------------------------------------------: |
| `npm run dev`   |                                    Compiles the code in development mode                                    |
| `npm run build` |                                    Compiles the code in production mode                                     |
| `npm start`     | Same as `npm run dev` but in watch mode (watches the source files and recompiles whenever they are changed) |

3. Open the `index.html` in your browser

## Workflow

UPDATE 220424

given up submodules. StoryMode is an independent git repo. aboutpower is another and is reduced to config files and other stuff that should not be overwritten in an update of the CA Software.

both have to be pushed and pulled independently or with a helper script.

so the workflow now is: work in storymode project
test on local server immediatly

## Fonts and graphics

local fonts, namely linux libertine, are included over the assets/pawtucket/css folder
graphics are included over assets/other-graphics

local fonts could also be installed via fontsource with:
npm install @fontsource/andada-pro --save-dev
but linux libertine seems not to be availlable there
