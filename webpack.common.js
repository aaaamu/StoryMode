const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
	entry: './views/StoryMode/src/bundle.js',
	output: {
		filename: 'main.js',
		//		path: path.resolve(__dirname, 'views/StoryMode/dist'),
		//path: path.resolve(__dirname + '/views/StoryMode/dist'),
		path: path.join(
			'/home/em/www/gitlab.com/aboutpower/themes/StoryMode/views/StoryMode/dist'
		),
		clean: true,
	},
	// entry: './views/StoryMode/src/edit-bundle.js',
	// output: {
	// 	filename: 'edit-main.js',
	// 	path: path.resolve(__dirname, 'views/StoryMode/dist'),
	// },

	module: {
		rules: [
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.scss$/i,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader',
					// 'postcss-loader',
				],
			},
			{
				test: /\.(woff|woff2|ttf|eot)$/,
				use: 'file-loader?name=fonts/[name].[ext]!static',
			},
			{
				test: /\.png$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							mimetype: 'image/png',
						},
					},
				],
			},
		],
	},
	plugins: [
		// new HtmlWebpackPlugin({
		// 	title: 'Production',
		// }),

		new CleanWebpackPlugin(),

		new ESLintPlugin(),
		new StylelintPlugin({
			files: '**/*.(s(c|a)ss)',
		}),
	],
};
